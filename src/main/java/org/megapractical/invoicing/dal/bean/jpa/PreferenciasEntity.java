/*
 * Created on 31 ene 2018 ( Time 13:03:09 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;


import javax.persistence.*;

/**
 * Persistent class for entity stored in table "preferencias"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="preferencias", schema="contribuyente" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="PreferenciasEntity.countAll", query="SELECT COUNT(x) FROM PreferenciasEntity x" )
} )
public class PreferenciasEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_PreferenciasEntity")
    @SequenceGenerator(name = "gen_PreferenciasEntity", sequenceName = "contribuyente.preferencias_id_seq", allocationSize = 1)
    @Column(name="id", nullable=false)
    private Long       id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="emisor_xml_pdf")
    private Boolean    emisorXmlPdf ;

    @Column(name="emisor_xml")
    private Boolean    emisorXml    ;

    @Column(name="emisor_pdf")
    private Boolean    emisorPdf    ;

    @Column(name="receptor_xml_pdf")
    private Boolean    receptorXmlPdf ;

    @Column(name="registrar_concepto")
    private Boolean    registrarConcepto ;

    @Column(name="receptor_xml")
    private Boolean    receptorXml  ;

    @Column(name="receptor_pdf")
    private Boolean    receptorPdf  ;

    @Column(name="contacto_xml_pdf")
    private Boolean    contactoXmlPdf ;

    @Column(name="contacto_xml")
    private Boolean    contactoXml  ;

    @Column(name="contacto_pdf")
    private Boolean    contactoPdf  ;

    @Column(name="registrar_cliente")
    private Boolean    registrarCliente ;

    @Column(name="actualizar_cliente")
    private Boolean    actualizarCliente ;

    @Column(name="actualizar_concepto")
    private Boolean    actualizarConcepto ;

    @Column(name="registrar_regimen_fiscal")
    private Boolean    registrarRegimenFiscal ;

    @Column(name="actualizar_regimen_fiscal")
    private Boolean    actualizarRegimenFiscal ;

    @Column(name="reutilizar_folio_cancelado")
    private Boolean    reutilizarFolioCancelado ;

    @Column(name="incrementar_folio_final")
    private Boolean    incrementarFolioFinal ;

    @Column(name="registrar_actualizar_lugar_expedicion")
    private Boolean    registrarActualizarLugarExpedicion ;

    @Column(name="prefactura_pdf")
    private Boolean    prefacturaPdf ;

	// "idContribuyente" (column "id_contribuyente") is not defined by itself because used as FK in a link 
	// "idMoneda" (column "id_moneda") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="id_contribuyente", referencedColumnName="id")
    private ContribuyenteEntity contribuyente;

    @ManyToOne
    @JoinColumn(name="id_moneda", referencedColumnName="id")
    private MonedaEntity moneda      ;


    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public PreferenciasEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Long id ) {
        this.id = id ;
    }
    public Long getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : emisor_xml_pdf ( bool ) 
    public void setEmisorXmlPdf( Boolean emisorXmlPdf ) {
        this.emisorXmlPdf = emisorXmlPdf;
    }
    public Boolean getEmisorXmlPdf() {
        return this.emisorXmlPdf;
    }

    //--- DATABASE MAPPING : emisor_xml ( bool ) 
    public void setEmisorXml( Boolean emisorXml ) {
        this.emisorXml = emisorXml;
    }
    public Boolean getEmisorXml() {
        return this.emisorXml;
    }

    //--- DATABASE MAPPING : emisor_pdf ( bool ) 
    public void setEmisorPdf( Boolean emisorPdf ) {
        this.emisorPdf = emisorPdf;
    }
    public Boolean getEmisorPdf() {
        return this.emisorPdf;
    }

    //--- DATABASE MAPPING : receptor_xml_pdf ( bool ) 
    public void setReceptorXmlPdf( Boolean receptorXmlPdf ) {
        this.receptorXmlPdf = receptorXmlPdf;
    }
    public Boolean getReceptorXmlPdf() {
        return this.receptorXmlPdf;
    }

    //--- DATABASE MAPPING : registrar_concepto ( bool ) 
    public void setRegistrarConcepto( Boolean registrarConcepto ) {
        this.registrarConcepto = registrarConcepto;
    }
    public Boolean getRegistrarConcepto() {
        return this.registrarConcepto;
    }

    //--- DATABASE MAPPING : receptor_xml ( bool ) 
    public void setReceptorXml( Boolean receptorXml ) {
        this.receptorXml = receptorXml;
    }
    public Boolean getReceptorXml() {
        return this.receptorXml;
    }

    //--- DATABASE MAPPING : receptor_pdf ( bool ) 
    public void setReceptorPdf( Boolean receptorPdf ) {
        this.receptorPdf = receptorPdf;
    }
    public Boolean getReceptorPdf() {
        return this.receptorPdf;
    }

    //--- DATABASE MAPPING : contacto_xml_pdf ( bool ) 
    public void setContactoXmlPdf( Boolean contactoXmlPdf ) {
        this.contactoXmlPdf = contactoXmlPdf;
    }
    public Boolean getContactoXmlPdf() {
        return this.contactoXmlPdf;
    }

    //--- DATABASE MAPPING : contacto_xml ( bool ) 
    public void setContactoXml( Boolean contactoXml ) {
        this.contactoXml = contactoXml;
    }
    public Boolean getContactoXml() {
        return this.contactoXml;
    }

    //--- DATABASE MAPPING : contacto_pdf ( bool ) 
    public void setContactoPdf( Boolean contactoPdf ) {
        this.contactoPdf = contactoPdf;
    }
    public Boolean getContactoPdf() {
        return this.contactoPdf;
    }

    //--- DATABASE MAPPING : registrar_cliente ( bool ) 
    public void setRegistrarCliente( Boolean registrarCliente ) {
        this.registrarCliente = registrarCliente;
    }
    public Boolean getRegistrarCliente() {
        return this.registrarCliente;
    }

    //--- DATABASE MAPPING : actualizar_cliente ( bool ) 
    public void setActualizarCliente( Boolean actualizarCliente ) {
        this.actualizarCliente = actualizarCliente;
    }
    public Boolean getActualizarCliente() {
        return this.actualizarCliente;
    }

    //--- DATABASE MAPPING : actualizar_concepto ( bool ) 
    public void setActualizarConcepto( Boolean actualizarConcepto ) {
        this.actualizarConcepto = actualizarConcepto;
    }
    public Boolean getActualizarConcepto() {
        return this.actualizarConcepto;
    }

    //--- DATABASE MAPPING : registrar_regimen_fiscal ( bool ) 
    public void setRegistrarRegimenFiscal( Boolean registrarRegimenFiscal ) {
        this.registrarRegimenFiscal = registrarRegimenFiscal;
    }
    public Boolean getRegistrarRegimenFiscal() {
        return this.registrarRegimenFiscal;
    }

    //--- DATABASE MAPPING : actualizar_regimen_fiscal ( bool ) 
    public void setActualizarRegimenFiscal( Boolean actualizarRegimenFiscal ) {
        this.actualizarRegimenFiscal = actualizarRegimenFiscal;
    }
    public Boolean getActualizarRegimenFiscal() {
        return this.actualizarRegimenFiscal;
    }

    //--- DATABASE MAPPING : reutilizar_folio_cancelado ( bool ) 
    public void setReutilizarFolioCancelado( Boolean reutilizarFolioCancelado ) {
        this.reutilizarFolioCancelado = reutilizarFolioCancelado;
    }
    public Boolean getReutilizarFolioCancelado() {
        return this.reutilizarFolioCancelado;
    }

    //--- DATABASE MAPPING : incrementar_folio_final ( bool ) 
    public void setIncrementarFolioFinal( Boolean incrementarFolioFinal ) {
        this.incrementarFolioFinal = incrementarFolioFinal;
    }
    public Boolean getIncrementarFolioFinal() {
        return this.incrementarFolioFinal;
    }

    //--- DATABASE MAPPING : registrar_actualizar_lugar_expedicion ( bool ) 
    public void setRegistrarActualizarLugarExpedicion( Boolean registrarActualizarLugarExpedicion ) {
        this.registrarActualizarLugarExpedicion = registrarActualizarLugarExpedicion;
    }
    public Boolean getRegistrarActualizarLugarExpedicion() {
        return this.registrarActualizarLugarExpedicion;
    }

    //--- DATABASE MAPPING : prefactura_pdf ( bool ) 
    public void setPrefacturaPdf( Boolean prefacturaPdf ) {
        this.prefacturaPdf = prefacturaPdf;
    }
    public Boolean getPrefacturaPdf() {
        return this.prefacturaPdf;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------
    public void setContribuyente( ContribuyenteEntity contribuyente ) {
        this.contribuyente = contribuyente;
    }
    public ContribuyenteEntity getContribuyente() {
        return this.contribuyente;
    }

    public void setMoneda( MonedaEntity moneda ) {
        this.moneda = moneda;
    }
    public MonedaEntity getMoneda() {
        return this.moneda;
    }


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(emisorXmlPdf);
        sb.append("|");
        sb.append(emisorXml);
        sb.append("|");
        sb.append(emisorPdf);
        sb.append("|");
        sb.append(receptorXmlPdf);
        sb.append("|");
        sb.append(registrarConcepto);
        sb.append("|");
        sb.append(receptorXml);
        sb.append("|");
        sb.append(receptorPdf);
        sb.append("|");
        sb.append(contactoXmlPdf);
        sb.append("|");
        sb.append(contactoXml);
        sb.append("|");
        sb.append(contactoPdf);
        sb.append("|");
        sb.append(registrarCliente);
        sb.append("|");
        sb.append(actualizarCliente);
        sb.append("|");
        sb.append(actualizarConcepto);
        sb.append("|");
        sb.append(registrarRegimenFiscal);
        sb.append("|");
        sb.append(actualizarRegimenFiscal);
        sb.append("|");
        sb.append(reutilizarFolioCancelado);
        sb.append("|");
        sb.append(incrementarFolioFinal);
        sb.append("|");
        sb.append(registrarActualizarLugarExpedicion);
        sb.append("|");
        sb.append(prefacturaPdf);
        return sb.toString(); 
    } 

}
