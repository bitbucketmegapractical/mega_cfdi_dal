/*
 * Created on 6 jul 2017 ( Time 22:00:35 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;


import javax.persistence.*;

/**
 * Persistent class for entity stored in table "comprobante_concepto_impuesto"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="comprobante_concepto_impuesto", schema="facturacion" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="ComprobanteConceptoImpuestoEntity.countAll", query="SELECT COUNT(x) FROM ComprobanteConceptoImpuestoEntity x" )
} )
public class ComprobanteConceptoImpuestoEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_ComprobanteConceptoImpuestoEntity")
	@SequenceGenerator(name = "gen_ComprobanteConceptoImpuestoEntity", sequenceName = "facturacion.comprobante_concepto_impuesto_id_seq", allocationSize = 1)
    @Column(name="id", nullable=false)
    private Long       id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="tasa_cuota")
    private Double     tasaCuota    ;

    @Column(name="base")
    private Double     base         ;

    @Column(name="importe")
    private Double     importe      ;

    @Column(name="eliminado")
    private Boolean    eliminado    ;

	// "idComprobanteConcepto" (column "id_comprobante_concepto") is not defined by itself because used as FK in a link 
	// "idConceptoTipoImpuesto" (column "id_concepto_tipo_impuesto") is not defined by itself because used as FK in a link 
	// "idTipoImpuesto" (column "id_tipo_impuesto") is not defined by itself because used as FK in a link 
	// "idTipoFactor" (column "id_tipo_factor") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="id_concepto_tipo_impuesto", referencedColumnName="id")
    private ConceptoTipoImpuestoEntity conceptoTipoImpuesto;

    @ManyToOne
    @JoinColumn(name="id_tipo_factor", referencedColumnName="id")
    private TipoFactorEntity tipoFactor  ;

    @ManyToOne
    @JoinColumn(name="id_comprobante_concepto", referencedColumnName="id")
    private ComprobanteConceptoEntity comprobanteConcepto;

    @ManyToOne
    @JoinColumn(name="id_tipo_impuesto", referencedColumnName="id")
    private TipoImpuestoEntity tipoImpuesto;


    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public ComprobanteConceptoImpuestoEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Long id ) {
        this.id = id ;
    }
    public Long getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : tasa_cuota ( float8 ) 
    public void setTasaCuota( Double tasaCuota ) {
        this.tasaCuota = tasaCuota;
    }
    public Double getTasaCuota() {
        return this.tasaCuota;
    }

    //--- DATABASE MAPPING : base ( float8 ) 
    public void setBase( Double base ) {
        this.base = base;
    }
    public Double getBase() {
        return this.base;
    }

    //--- DATABASE MAPPING : importe ( float8 ) 
    public void setImporte( Double importe ) {
        this.importe = importe;
    }
    public Double getImporte() {
        return this.importe;
    }

    //--- DATABASE MAPPING : eliminado ( bool ) 
    public void setEliminado( Boolean eliminado ) {
        this.eliminado = eliminado;
    }
    public Boolean getEliminado() {
        return this.eliminado;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------
    public void setConceptoTipoImpuesto( ConceptoTipoImpuestoEntity conceptoTipoImpuesto ) {
        this.conceptoTipoImpuesto = conceptoTipoImpuesto;
    }
    public ConceptoTipoImpuestoEntity getConceptoTipoImpuesto() {
        return this.conceptoTipoImpuesto;
    }

    public void setTipoFactor( TipoFactorEntity tipoFactor ) {
        this.tipoFactor = tipoFactor;
    }
    public TipoFactorEntity getTipoFactor() {
        return this.tipoFactor;
    }

    public void setComprobanteConcepto( ComprobanteConceptoEntity comprobanteConcepto ) {
        this.comprobanteConcepto = comprobanteConcepto;
    }
    public ComprobanteConceptoEntity getComprobanteConcepto() {
        return this.comprobanteConcepto;
    }

    public void setTipoImpuesto( TipoImpuestoEntity tipoImpuesto ) {
        this.tipoImpuesto = tipoImpuesto;
    }
    public TipoImpuestoEntity getTipoImpuesto() {
        return this.tipoImpuesto;
    }


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(tasaCuota);
        sb.append("|");
        sb.append(base);
        sb.append("|");
        sb.append(importe);
        sb.append("|");
        sb.append(eliminado);
        return sb.toString(); 
    } 

}
