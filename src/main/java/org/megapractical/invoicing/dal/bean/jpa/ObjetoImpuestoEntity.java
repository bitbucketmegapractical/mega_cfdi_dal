package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Persistent class for entity stored in table "objeto_impuesto"
 *
 */

@Entity
@Table(name = "objeto_impuesto", schema = "nomencladores")
public class ObjetoImpuestoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	// ----------------------------------------------------------------------
	// ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
	// ----------------------------------------------------------------------
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_ObjetoImpuestoEntity")
	@SequenceGenerator(name = "gen_ObjetoImpuestoEntity", sequenceName = "nomencladores.objeto_impuesto_id_seq", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Long id;

	// ----------------------------------------------------------------------
	// ENTITY DATA FIELDS
	// ----------------------------------------------------------------------
	@Column(name = "codigo", length = 250)
	private String codigo;

	@Column(name = "valor", length = 250)
	private String valor;

	@Column(name = "eliminado")
	private Boolean eliminado;

	public ObjetoImpuestoEntity() {
		super();
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getValor() {
		return this.valor;
	}

	public void setEliminado(Boolean eliminado) {
		this.eliminado = eliminado;
	}

	public Boolean getEliminado() {
		return this.eliminado;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append("]:");
		sb.append(codigo);
		sb.append("|");
		sb.append(valor);
		sb.append("|");
		sb.append(eliminado);
		return sb.toString();
	}

}