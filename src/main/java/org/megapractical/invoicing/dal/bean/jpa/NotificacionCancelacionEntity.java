package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Persistent class for entity stored in table "notificacion_cancelacion"
 *
 * @author Maikel Guerra Ferrer
 *
 */

@Entity
@Table(name = "notificacion_cancelacion", schema = "notificacion")
public class NotificacionCancelacionEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_NotificacionCancelacionEntity")
	@SequenceGenerator(name = "gen_NotificacionCancelacionEntity", sequenceName = "notificacion.notificacion_cancelacion_id_seq", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_archivo_cancelacion", referencedColumnName = "id")
	private ArchivoCancelacionEntity archivoCancelacion;

	@Column(name = "recipient")
	private String recipient;

	@Column(name = "subject")
	private String subject;

	@Column(name = "send_date")
	private LocalDateTime sendDate;

	@Column(name = "reason")
	private String reason;

	/* Getters and Setters */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ArchivoCancelacionEntity getArchivoCancelacion() {
		return archivoCancelacion;
	}

	public void setArchivoCancelacion(ArchivoCancelacionEntity archivoCancelacion) {
		this.archivoCancelacion = archivoCancelacion;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public LocalDateTime getSendDate() {
		return sendDate;
	}

	public void setSendDate(LocalDateTime sendDate) {
		this.sendDate = sendDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	/* toString */
	@Override
	public String toString() {
		return "NotificacionCancelacionEntity [id=" + id + ", archivoCancelacion=" + archivoCancelacion.getId()
				+ ", recipient=" + recipient + ", subject=" + subject + ", sendDate=" + sendDate + ", reason=" + reason
				+ "]";
	}

}