/*
 * Created on 9 ene 2018 ( Time 18:52:03 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "contribuyente_lugar_expedicion"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="contribuyente_lugar_expedicion", schema="contribuyente" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="ContribuyenteLugarExpedicionEntity.countAll", query="SELECT COUNT(x) FROM ContribuyenteLugarExpedicionEntity x" )
} )
public class ContribuyenteLugarExpedicionEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_ContribuyenteLugarExpedicionEntity")
	@SequenceGenerator(name = "gen_ContribuyenteLugarExpedicionEntity", sequenceName = "contribuyente.contribuyente_lugar_expedicion_id_seq", allocationSize = 1)
    @Column(name="id", nullable=false)
    private Long       id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Temporal(TemporalType.DATE)
    @Column(name="fecha_ultima_modificacion")
    private Date       fechaUltimaModificacion ;

    @Column(name="eliminado")
    private Boolean    eliminado    ;

	// "idContribuyente" (column "id_contribuyente") is not defined by itself because used as FK in a link 
	// "idLugarExpedicion" (column "id_lugar_expedicion") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="id_lugar_expedicion", referencedColumnName="id")
    private CodigoPostalEntity codigoPostal;

    @ManyToOne
    @JoinColumn(name="id_contribuyente", referencedColumnName="id")
    private ContribuyenteEntity contribuyente;


    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public ContribuyenteLugarExpedicionEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Long id ) {
        this.id = id ;
    }
    public Long getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : fecha_ultima_modificacion ( date ) 
    public void setFechaUltimaModificacion( Date fechaUltimaModificacion ) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }
    public Date getFechaUltimaModificacion() {
        return this.fechaUltimaModificacion;
    }

    //--- DATABASE MAPPING : eliminado ( bool ) 
    public void setEliminado( Boolean eliminado ) {
        this.eliminado = eliminado;
    }
    public Boolean getEliminado() {
        return this.eliminado;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------
    public void setCodigoPostal( CodigoPostalEntity codigoPostal ) {
        this.codigoPostal = codigoPostal;
    }
    public CodigoPostalEntity getCodigoPostal() {
        return this.codigoPostal;
    }

    public void setContribuyente( ContribuyenteEntity contribuyente ) {
        this.contribuyente = contribuyente;
    }
    public ContribuyenteEntity getContribuyente() {
        return this.contribuyente;
    }


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(fechaUltimaModificacion);
        sb.append("|");
        sb.append(eliminado);
        return sb.toString(); 
    } 

}
