package org.megapractical.invoicing.dal.bean.datatype;

public enum StorageType {
	SERVER, S3
}