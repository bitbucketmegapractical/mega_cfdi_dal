package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.val;

/**
 * Persistent class for entity stored in table "moneda"
 *
 * @author Maikel Guerra Ferrer
 *
 */

@Entity
@Table(name = "exportacion", schema = "nomencladores")
public class ExportacionEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_ExportacionEntity")
	@SequenceGenerator(name = "gen_ExportacionEntity", sequenceName = "nomencladores.exportacion_id_seq", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "codigo", length = 20)
	private String codigo;

	@Column(name = "valor", length = 100)
	private String valor;

	@Column(name = "eliminado")
	private Boolean eliminado;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "exportacion", targetEntity = PrefacturaEntity.class)
	private List<PrefacturaEntity> listOfPrefactura;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "moneda", targetEntity = ComprobanteEntity.class)
	private List<ComprobanteEntity> listOfComprobante;

	public ExportacionEntity() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Boolean getEliminado() {
		return eliminado;
	}

	public void setEliminado(Boolean eliminado) {
		this.eliminado = eliminado;
	}

	public List<PrefacturaEntity> getListOfPrefactura() {
		return listOfPrefactura;
	}

	public void setListOfPrefactura(List<PrefacturaEntity> listOfPrefactura) {
		this.listOfPrefactura = listOfPrefactura;
	}

	public List<ComprobanteEntity> getListOfComprobante() {
		return listOfComprobante;
	}

	public void setListOfComprobante(List<ComprobanteEntity> listOfComprobante) {
		this.listOfComprobante = listOfComprobante;
	}

	public String toString() {
		val sb = new StringBuilder();
		sb.append("[");
		sb.append(id);
		sb.append("]:");
		sb.append(codigo);
		sb.append("|");
		sb.append(valor);
		sb.append("|");
		sb.append(eliminado);
		return sb.toString();
	}

}