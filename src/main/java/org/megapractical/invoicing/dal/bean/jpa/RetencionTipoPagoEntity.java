/*
 * Created on 2 jul 2018 ( Time 22:54:05 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Persistent class for entity stored in table "archivo_factura"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name = "retencion_tipo_pago", schema = "nomencladores")
public class RetencionTipoPagoEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_RetencionTipoPagoEntity")
	@SequenceGenerator(name = "gen_RetencionTipoPagoEntity", sequenceName = "nomencladores.retencion_tipo_pago_id_seq", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "tipo_pago")
	private String tipoPago;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@Column(name = "tipo_impuesto")
	private String tipoImpuesto;
	
	@Column(name = "eliminado")
	private Boolean eliminado;

	/*Getters and Setters*/
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipoImpuesto() {
		return tipoImpuesto;
	}

	public void setTipoImpuesto(String tipoImpuesto) {
		this.tipoImpuesto = tipoImpuesto;
	}

	public Boolean getEliminado() {
		return eliminado;
	}

	public void setEliminado(Boolean eliminado) {
		this.eliminado = eliminado;
	}
	
	@Transient
    public String getData() {
    	return String.format("%s %s", this.tipoPago, this.descripcion);
    }

	@Override
	public String toString() {
		return "RetencionTipoPagoEntity [id=" + id + ", tipoPago=" + tipoPago + ", descripcion=" + descripcion
				+ ", tipoImpuesto=" + tipoImpuesto + ", eliminado=" + eliminado + "]";
	}

}