package org.megapractical.invoicing.dal.bean.common;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class EntryBase {
	@Column(name = "nombre", length = 20)
	private String nombre;

	@Column(name = "campos")
	private Integer campos;

	@Column(name = "longitud")
	private Integer longitud;

	@Column(name = "descripcion", length = 250)
	private String descripcion;

	@Column(name = "principal")
	private boolean principal;

	@Column(name = "requerido")
	private boolean requerido;

	@Column(name = "eliminado")
	private boolean eliminado;

	/*Getters and Setters*/
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCampos() {
		return campos;
	}

	public void setCampos(Integer campos) {
		this.campos = campos;
	}

	public Integer getLongitud() {
		return longitud;
	}

	public void setLongitud(Integer longitud) {
		this.longitud = longitud;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isPrincipal() {
		return principal;
	}

	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}

	public boolean isRequerido() {
		return requerido;
	}

	public void setRequerido(boolean requerido) {
		this.requerido = requerido;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}
	
}