package org.megapractical.invoicing.dal.bean.datatype;

public enum FileStatus {
	C, P, G, NP
}