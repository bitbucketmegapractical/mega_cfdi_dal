/*
 * Created on 13 dic 2017 ( Time 13:40:06 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;
import java.util.Arrays;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "usuario"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name = "usuario", schema = "seguridad")
public class UsuarioEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	// ----------------------------------------------------------------------
	// ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
	// ----------------------------------------------------------------------
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_UsuarioEntity")
	@SequenceGenerator(name = "gen_UsuarioEntity", sequenceName = "seguridad.usuario_id_seq", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Long id;

	// ----------------------------------------------------------------------
	// ENTITY DATA FIELDS
	// ----------------------------------------------------------------------
	@Column(name = "correo_electronico", nullable = false, length = 100)
	private String correoElectronico;

	@Column(name = "clave", length = 128)
	private String clave;

	@Column(name = "hash", length = 128)
	private String hash;

	@ManyToOne
	@JoinColumn(name = "id_rol", referencedColumnName = "id")
	private RolEntity rol;

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_ultimo_acceso")
	private Date fechaUltimoAcceso;

	@Temporal(TemporalType.TIME)
	@Column(name = "hora_ultimo_acceso")
	private Date horaUltimoAcceso;

	@Column(name = "generado_por_sistema")
	private boolean generadoPorSistema;
	
	@Column(name = "api_key", length = 128)
	private String apiKey; 

	@Column(name = "servicio_descarga")
	private boolean servicioDescarga;

	@Column(name = "activo")
	private boolean activo;

	// "userId" (column "user_id") is not defined by itself because used as FK in a
	// link

	// ----------------------------------------------------------------------
	// ENTITY LINKS ( RELATIONSHIP )
	// ----------------------------------------------------------------------
	@OneToMany(mappedBy = "usuario", targetEntity = SesionEntity.class)
	private List<SesionEntity> listOfSesion;

	@OneToMany(mappedBy = "usuario", targetEntity = CodigoActivacionEntity.class)
	private List<CodigoActivacionEntity> listOfCodigoActivacion;

	@OneToMany(mappedBy = "usuario", targetEntity = CredencialEntity.class)
	private List<CredencialEntity> listOfCredencial;

	@OneToMany(mappedBy = "usuario", targetEntity = BitacoraEntity.class)
	private List<BitacoraEntity> listOfBitacora;

	@OneToMany(mappedBy = "usuario", targetEntity = BitacoraSucesoEntity.class)
	private List<BitacoraSucesoEntity> listOfBitacoraSuceso;

	// ----------------------------------------------------------------------
	// CONSTRUCTOR(S)
	// ----------------------------------------------------------------------
	public UsuarioEntity() {
		super();
	}

	// ----------------------------------------------------------------------
	// GETTER & SETTER FOR THE KEY FIELD
	// ----------------------------------------------------------------------
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	// ----------------------------------------------------------------------
	// GETTERS & SETTERS FOR FIELDS
	// ----------------------------------------------------------------------
	// --- DATABASE MAPPING : correo_electronico ( varchar )
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getCorreoElectronico() {
		return this.correoElectronico;
	}

	// --- DATABASE MAPPING : hash ( varchar )
	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getHash() {
		return this.hash;
	}

	// --- DATABASE MAPPING : fecha_ultimo_acceso ( date )
	public void setFechaUltimoAcceso(Date fechaUltimoAcceso) {
		this.fechaUltimoAcceso = fechaUltimoAcceso;
	}

	public Date getFechaUltimoAcceso() {
		return this.fechaUltimoAcceso;
	}

	// --- DATABASE MAPPING : hora_ultimo_acceso ( time )
	public void setHoraUltimoAcceso(Date horaUltimoAcceso) {
		this.horaUltimoAcceso = horaUltimoAcceso;
	}

	public Date getHoraUltimoAcceso() {
		return this.horaUltimoAcceso;
	}

	// --- DATABASE MAPPING : generado_por_sistema ( bool )
	public void setGeneradoPorSistema(Boolean generadoPorSistema) {
		this.generadoPorSistema = generadoPorSistema;
	}

	public Boolean getGeneradoPorSistema() {
		return this.generadoPorSistema;
	}

	// ----------------------------------------------------------------------
	// GETTERS & SETTERS FOR LINKS
	// ----------------------------------------------------------------------
	public void setListOfSesion(List<SesionEntity> listOfSesion) {
		this.listOfSesion = listOfSesion;
	}

	public List<SesionEntity> getListOfSesion() {
		return this.listOfSesion;
	}

	public void setListOfCodigoActivacion(List<CodigoActivacionEntity> listOfCodigoActivacion) {
		this.listOfCodigoActivacion = listOfCodigoActivacion;
	}

	public List<CodigoActivacionEntity> getListOfCodigoActivacion() {
		return this.listOfCodigoActivacion;
	}

	public void setListOfCredencial(List<CredencialEntity> listOfCredencial) {
		this.listOfCredencial = listOfCredencial;
	}

	public List<CredencialEntity> getListOfCredencial() {
		return this.listOfCredencial;
	}

	public void setListOfBitacora(List<BitacoraEntity> listOfBitacora) {
		this.listOfBitacora = listOfBitacora;
	}

	public List<BitacoraEntity> getListOfBitacora() {
		return this.listOfBitacora;
	}

	public void setListOfBitacoraSuceso(List<BitacoraSucesoEntity> listOfBitacoraSuceso) {
		this.listOfBitacoraSuceso = listOfBitacoraSuceso;
	}

	public List<BitacoraSucesoEntity> getListOfBitacoraSuceso() {
		return this.listOfBitacoraSuceso;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public RolEntity getRol() {
		return rol;
	}

	public void setRol(RolEntity rol) {
		this.rol = rol;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public boolean isServicioDescarga() {
		return servicioDescarga;
	}

	public void setServicioDescarga(boolean servicioDescarga) {
		this.servicioDescarga = servicioDescarga;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public void setGeneradoPorSistema(boolean generadoPorSistema) {
		this.generadoPorSistema = generadoPorSistema;
	}

	@Transient
	public String getAuthority() {
		return rol.getCodigo();
	}
	
	@Transient
	public List<String> getAuthorities() {
		return Arrays.asList(rol.getCodigo());
	}

	// ----------------------------------------------------------------------
	// toString METHOD
	// ----------------------------------------------------------------------
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		sb.append(id);
		sb.append("]:");
		sb.append(correoElectronico);
		sb.append("|");
		sb.append(getAuthority());
		sb.append("|");
		sb.append(fechaUltimoAcceso);
		sb.append("|");
		sb.append(horaUltimoAcceso);
		sb.append("|");
		sb.append(generadoPorSistema);
		sb.append("|");
		sb.append(apiKey);
		sb.append("|");
		sb.append(servicioDescarga);
		sb.append("|");
		sb.append(activo);
		return sb.toString();
	}

}
