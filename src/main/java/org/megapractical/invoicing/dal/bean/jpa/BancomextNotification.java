package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Persistent class for entity stored in table "api_token"
 *
 * @author Maikel Guerra Ferrer
 *
 */
@Entity
@Table(name = "bancomext_notification", schema = "clientes")
public class BancomextNotification implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_BancomextNotificationError")
	@SequenceGenerator(name = "gen_BancomextNotificationError", sequenceName = "clientes.bancomext_notification_error_id_seq", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "recipient")
	private String recipient;

	@Column(name = "receiver_rfc")
	private String receiverRfc;

	@Column(name = "serie")
	private String serie;

	@Column(name = "folio")
	private String folio;

	@Column(name = "uuid")
	private String uuid;

	@Column(name = "xml_file_name")
	private String xmlFileName;

	@Column(name = "xml_path")
	private String xmlPath;

	@Column(name = "pdf_file_name")
	private String pdfFileName;

	@Column(name = "pdf_path")
	private String pdfPath;
	
	@Column(name = "file_name")
	private String fileName;
	
	@Column(name = "send_date")
	private LocalDateTime sendDate;

	@Column(name = "reason")
	private String reason;
	
	@Column(name = "subject")
	private String subject;

	/* Getters and Setters */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getReceiverRfc() {
		return receiverRfc;
	}

	public void setReceiverRfc(String receiverRfc) {
		this.receiverRfc = receiverRfc;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getXmlFileName() {
		return xmlFileName;
	}

	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public String getXmlPath() {
		return xmlPath;
	}

	public void setXmlPath(String xmlPath) {
		this.xmlPath = xmlPath;
	}

	public String getPdfFileName() {
		return pdfFileName;
	}

	public void setPdfFileName(String pdfFileName) {
		this.pdfFileName = pdfFileName;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public LocalDateTime getSendDate() {
		return sendDate;
	}

	public void setSendDate(LocalDateTime sendDate) {
		this.sendDate = sendDate;
	}

	public String getReason() {
		return reason;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	/* totring */
	@Override
	public String toString() {
		return "BancomextNotification [id=" + id + ", recipient=" + recipient + ", receiverRfc=" + receiverRfc
				+ ", serie=" + serie + ", folio=" + folio + ", uuid=" + uuid + ", xmlFileName=" + xmlFileName
				+ ", xmlPath=" + xmlPath + ", pdfFileName=" + pdfFileName + ", pdfPath=" + pdfPath + ", fileName="
				+ fileName + ", sendDate=" + sendDate + ", reason=" + reason + ", subject=" + subject + "]";
	}

}