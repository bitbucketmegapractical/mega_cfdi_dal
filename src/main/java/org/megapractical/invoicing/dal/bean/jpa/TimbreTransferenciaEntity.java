/*
 * Created on 29 sep 2017 ( Time 14:47:12 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "timbre_transferencia"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="timbre_transferencia", schema="contabilidad" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="TimbreTransferenciaEntity.countAll", query="SELECT COUNT(x) FROM TimbreTransferenciaEntity x" )
} )
public class TimbreTransferenciaEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_TimbreTransferenciaEntity")
	@SequenceGenerator(name = "gen_TimbreTransferenciaEntity", sequenceName = "contabilidad.timbre_transferencia_id_seq", allocationSize = 1)
    @Column(name="id", nullable=false)
    private Long       id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="cantidad")
    private Integer    cantidad     ;

    @Temporal(TemporalType.DATE)
    @Column(name="fecha")
    private Date       fecha        ;

    @Column(name="transferencia_cuenta_master")
    private Boolean    transferenciaCuentaMaster ;

    @Temporal(TemporalType.TIME)
    @Column(name="hora")
    private Date       hora         ;

	// "idContribuyente" (column "id_contribuyente") is not defined by itself because used as FK in a link 
	// "idCuenta" (column "id_cuenta") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="id_contribuyente", referencedColumnName="id")
    private ContribuyenteEntity contribuyente;

    @ManyToOne
    @JoinColumn(name="id_cuenta", referencedColumnName="id")
    private CuentaEntity cuenta      ;


    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public TimbreTransferenciaEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Long id ) {
        this.id = id ;
    }
    public Long getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : cantidad ( int4 ) 
    public void setCantidad( Integer cantidad ) {
        this.cantidad = cantidad;
    }
    public Integer getCantidad() {
        return this.cantidad;
    }

    //--- DATABASE MAPPING : fecha ( date ) 
    public void setFecha( Date fecha ) {
        this.fecha = fecha;
    }
    public Date getFecha() {
        return this.fecha;
    }

    //--- DATABASE MAPPING : transferencia_cuenta_master ( bool ) 
    public void setTransferenciaCuentaMaster( Boolean transferenciaCuentaMaster ) {
        this.transferenciaCuentaMaster = transferenciaCuentaMaster;
    }
    public Boolean getTransferenciaCuentaMaster() {
        return this.transferenciaCuentaMaster;
    }

    //--- DATABASE MAPPING : hora ( time ) 
    public void setHora( Date hora ) {
        this.hora = hora;
    }
    public Date getHora() {
        return this.hora;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------
    public void setContribuyente( ContribuyenteEntity contribuyente ) {
        this.contribuyente = contribuyente;
    }
    public ContribuyenteEntity getContribuyente() {
        return this.contribuyente;
    }

    public void setCuenta( CuentaEntity cuenta ) {
        this.cuenta = cuenta;
    }
    public CuentaEntity getCuenta() {
        return this.cuenta;
    }


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(cantidad);
        sb.append("|");
        sb.append(fecha);
        sb.append("|");
        sb.append(transferenciaCuentaMaster);
        sb.append("|");
        sb.append(hora);
        return sb.toString(); 
    } 

}
