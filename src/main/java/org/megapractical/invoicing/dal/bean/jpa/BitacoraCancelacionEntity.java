package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.megapractical.invoicing.dal.bean.datatype.CancellationType;

/**
 * Persistent class for entity stored in table "bitacora_cancelacion"
 *
 * @author Maikel Guerra Ferrer
 *
 */

@Entity
@Table(name = "bitacora_cancelacion", schema = "facturacion")
public class BitacoraCancelacionEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_BitacoraCancelacionEntity")
	@SequenceGenerator(name = "gen_BitacoraCancelacionEntity", sequenceName = "facturacion.bitacora_cancelacion_id_seq", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_archivo_cancelacion", referencedColumnName = "id")
	private ArchivoCancelacionEntity archivoCancelacion;

	@Column(name = "tipo")
	@Enumerated(EnumType.STRING)
	private CancellationType tipo;

	@Column(name = "emisor_rfc", length = 250)
	private String emisorRfc;

	@Column(name = "receptor_rfc", nullable = false, length = 250)
	private String receptorRfc;

	@Column(name = "uuid")
	private String uuid;

	@Column(name = "total")
	private String total;

	@Column(name = "motivo")
	private String motivo;

	@Column(name = "uuid_sustitucion")
	private String uuidSustitucion;

	@Column(name = "cancelado")
	private boolean cancelado;

	@Column(name = "cancelacion_en_proceso")
	private boolean cancelacionEnProceso;

	@Column(name = "solicitud_cancelacion")
	private boolean solicitudCancelacion;

	@Column(name = "acuse_cancelacion")
	private String acuseCancelacion;

	@Column(name = "fecha_cancelacion")
	private LocalDateTime fechaCancelacion;

	/* Getters and Setters */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ArchivoCancelacionEntity getArchivoCancelacion() {
		return archivoCancelacion;
	}

	public void setArchivoCancelacion(ArchivoCancelacionEntity archivoCancelacion) {
		this.archivoCancelacion = archivoCancelacion;
	}

	public CancellationType getTipo() {
		return tipo;
	}

	public void setTipo(CancellationType tipo) {
		this.tipo = tipo;
	}

	public String getEmisorRfc() {
		return emisorRfc;
	}

	public void setEmisorRfc(String emisorRfc) {
		this.emisorRfc = emisorRfc;
	}

	public String getReceptorRfc() {
		return receptorRfc;
	}

	public void setReceptorRfc(String receptorRfc) {
		this.receptorRfc = receptorRfc;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getUuidSustitucion() {
		return uuidSustitucion;
	}

	public void setUuidSustitucion(String uuidSustitucion) {
		this.uuidSustitucion = uuidSustitucion;
	}

	public boolean isCancelado() {
		return cancelado;
	}

	public void setCancelado(boolean cancelado) {
		this.cancelado = cancelado;
	}

	public boolean isCancelacionEnProceso() {
		return cancelacionEnProceso;
	}

	public void setCancelacionEnProceso(boolean cancelacionEnProceso) {
		this.cancelacionEnProceso = cancelacionEnProceso;
	}

	public boolean isSolicitudCancelacion() {
		return solicitudCancelacion;
	}

	public void setSolicitudCancelacion(boolean solicitudCancelacion) {
		this.solicitudCancelacion = solicitudCancelacion;
	}

	public String getAcuseCancelacion() {
		return acuseCancelacion;
	}

	public void setAcuseCancelacion(String acuseCancelacion) {
		this.acuseCancelacion = acuseCancelacion;
	}

	public LocalDateTime getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(LocalDateTime fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	/* toString */
	@Override
	public String toString() {
		return "BitacoraCancelacionEntity [id=" + id + ", archivoCancelacion=" + archivoCancelacion + ", tipo=" + tipo
				+ ", emisorRfc=" + emisorRfc + ", receptorRfc=" + receptorRfc + ", uuid=" + uuid + ", total=" + total
				+ ", motivo=" + motivo + ", uuidSustitucion=" + uuidSustitucion + ", cancelado=" + cancelado
				+ ", cancelacionEnProceso=" + cancelacionEnProceso + ", solicitudCancelacion=" + solicitudCancelacion
				+ ", acuseCancelacion=" + acuseCancelacion + ", fechaCancelacion=" + fechaCancelacion + "]";
	}

}