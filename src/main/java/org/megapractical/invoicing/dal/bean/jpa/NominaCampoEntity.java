/*
 * Created on 25 oct 2017 ( Time 09:30:26 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;

//import org.hibernate.validator.constraints.* ;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.megapractical.invoicing.dal.bean.common.FieldBase;

/**
 * Persistent class for entity stored in table "nomina_campo"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name = "nomina_campo", schema = "archivo")
// Define named queries here
@NamedQueries({ @NamedQuery(name = "NominaCampoEntity.countAll", query = "SELECT COUNT(x) FROM NominaCampoEntity x") })
public class NominaCampoEntity extends FieldBase implements Serializable {

	private static final long serialVersionUID = 1L;

	// ----------------------------------------------------------------------
	// ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
	// ----------------------------------------------------------------------
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_NominaCampoEntity")
	@SequenceGenerator(name = "gen_NominaCampoEntity", sequenceName = "archivo.nomina_campo_id_seq", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Long id;

	// "idNominaTrama" (column "id_nomina_trama") is not defined by itself because
	// used as FK in a link

	// ----------------------------------------------------------------------
	// ENTITY LINKS ( RELATIONSHIP )
	// ----------------------------------------------------------------------
	@ManyToOne
	@JoinColumn(name = "id_nomina_trama", referencedColumnName = "id")
	private NominaTramaEntity nominaTrama;

	// ----------------------------------------------------------------------
	// CONSTRUCTOR(S)
	// ----------------------------------------------------------------------
	public NominaCampoEntity() {
		super();
	}

	// ----------------------------------------------------------------------
	// GETTER & SETTER FOR THE KEY FIELD
	// ----------------------------------------------------------------------
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	// ----------------------------------------------------------------------
	// GETTERS & SETTERS FOR LINKS
	// ----------------------------------------------------------------------
	public void setNominaTrama(NominaTramaEntity nominaTrama) {
		this.nominaTrama = nominaTrama;
	}

	public NominaTramaEntity getNominaTrama() {
		return this.nominaTrama;
	}

	// ----------------------------------------------------------------------
	// toString METHOD
	// ----------------------------------------------------------------------
	@Override
	public String toString() {
		return "NominaCampoEntity [id=" + id + ", nombre=" + getNombre()
				+ ", posicion=" + getPosicion() + ", longitud=" + getLongitud() + ", longitudMinima="
				+ getLongitudMinima() + ", longitudMaxima=" + getLongitudMaxima() + ", tipoDato="
				+ getTipoDato() + ", descripcion=" + getDescripcion() + ", formato=" + getFormato()
				+ ", validacion=" + getValidacion() + ", requerido=" + isRequerido() + ", eliminado="
				+ isEliminado() + "]";
	}

}