package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Persistent class for entity stored in table "archivo_cancelacion"
 *
 * @author Maikel Guerra Ferrer
 *
 */

@Entity
@Table(name = "archivo_cancelacion", schema = "archivo")
public class ArchivoCancelacionEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_ArchivoCancelacionEntity")
	@SequenceGenerator(name = "gen_ArchivoCancelacionEntity", sequenceName = "archivo.archivo_cancelacion_id_seq", allocationSize = 1)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_contribuyente", referencedColumnName = "id")
	private ContribuyenteEntity contribuyente;

	@ManyToOne
	@JoinColumn(name = "id_tipo_archivo", referencedColumnName = "id")
	private TipoArchivoEntity tipoArchivo;

	@ManyToOne
	@JoinColumn(name = "id_estado_archivo", referencedColumnName = "id")
	private EstadoArchivoEntity estadoArchivo;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "ruta")
	private String ruta;

	@Column(name = "fecha_carga")
	private LocalDateTime fechaCarga;

	@Column(name = "ruta_acuses")
	private String rutaAcuses;

	@Column(name = "ruta_error")
	private String rutaError;

	@Column(name = "eliminado")
	private boolean eliminado;

	@OneToMany(mappedBy = "archivoCancelacion", targetEntity = BitacoraCancelacionEntity.class)
	private List<BitacoraCancelacionEntity> bitacoraCancelacions;
	
	@OneToMany(mappedBy = "archivoCancelacion", targetEntity = NotificacionCancelacionEntity.class)
	private List<NotificacionCancelacionEntity> notificacionCancelacions;

	/* Getters and Setters */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ContribuyenteEntity getContribuyente() {
		return contribuyente;
	}

	public void setContribuyente(ContribuyenteEntity contribuyente) {
		this.contribuyente = contribuyente;
	}

	public TipoArchivoEntity getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(TipoArchivoEntity tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public EstadoArchivoEntity getEstadoArchivo() {
		return estadoArchivo;
	}

	public void setEstadoArchivo(EstadoArchivoEntity estadoArchivo) {
		this.estadoArchivo = estadoArchivo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public LocalDateTime getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(LocalDateTime fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	public String getRutaAcuses() {
		return rutaAcuses;
	}

	public void setRutaAcuses(String rutaAcuses) {
		this.rutaAcuses = rutaAcuses;
	}

	public String getRutaError() {
		return rutaError;
	}

	public void setRutaError(String rutaError) {
		this.rutaError = rutaError;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	public List<BitacoraCancelacionEntity> getBitacoraCancelacions() {
		if (bitacoraCancelacions == null) {
			bitacoraCancelacions = new ArrayList<>();
		}
		return bitacoraCancelacions;
	}

	public void setBitacoraCancelacions(List<BitacoraCancelacionEntity> bitacoraCancelacions) {
		this.bitacoraCancelacions = bitacoraCancelacions;
	}

	public List<NotificacionCancelacionEntity> getNotificacionCancelacions() {
		if (notificacionCancelacions == null) {
			notificacionCancelacions = new ArrayList<>();
		}
		return notificacionCancelacions;
	}

	public void setNotificacionCancelacions(List<NotificacionCancelacionEntity> notificacionCancelacions) {
		this.notificacionCancelacions = notificacionCancelacions;
	}

	/* toString */
	@Override
	public String toString() {
		return "ArchivoCancelacionEntity [id=" + id + ", contribuyente=" + contribuyente.getId() + ", tipoArchivo="
				+ tipoArchivo.getId() + ", estadoArchivo=" + estadoArchivo.getId() + ", nombre=" + nombre + ", ruta="
				+ ruta + ", fechaCarga=" + fechaCarga + ", rutaAcuses=" + rutaAcuses + ", rutaError="
				+ rutaError + ", eliminado=" + eliminado + "]";
	}

}