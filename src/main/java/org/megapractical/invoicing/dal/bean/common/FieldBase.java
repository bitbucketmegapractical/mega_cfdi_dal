package org.megapractical.invoicing.dal.bean.common;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class FieldBase {
	@Column(name = "nombre", length = 2147483647)
	private String nombre;

	@Column(name = "posicion")
	private Integer posicion;

	@Column(name = "longitud")
	private Integer longitud;

	@Column(name = "longitud_minima")
	private Integer longitudMinima;

	@Column(name = "longitud_maxima")
	private Integer longitudMaxima;

	@Column(name = "tipo_dato", length = 2147483647)
	private String tipoDato;

	@Column(name = "descripcion", length = 1000)
	private String descripcion;

	@Column(name = "formato", length = 2147483647)
	private String formato;

	@Column(name = "validacion", length = 2147483647)
	private String validacion;

	@Column(name = "requerido")
	private boolean requerido;

	@Column(name = "eliminado")
	private boolean eliminado;

	/* Getters and Setters */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPosicion() {
		return posicion;
	}

	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}

	public Integer getLongitud() {
		return longitud;
	}

	public void setLongitud(Integer longitud) {
		this.longitud = longitud;
	}

	public Integer getLongitudMinima() {
		return longitudMinima;
	}

	public void setLongitudMinima(Integer longitudMinima) {
		this.longitudMinima = longitudMinima;
	}

	public Integer getLongitudMaxima() {
		return longitudMaxima;
	}

	public void setLongitudMaxima(Integer longitudMaxima) {
		this.longitudMaxima = longitudMaxima;
	}

	public String getTipoDato() {
		return tipoDato;
	}

	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public String getValidacion() {
		return validacion;
	}

	public void setValidacion(String validacion) {
		this.validacion = validacion;
	}

	public boolean isRequerido() {
		return requerido;
	}

	public void setRequerido(boolean requerido) {
		this.requerido = requerido;
	}

	public boolean isEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

}