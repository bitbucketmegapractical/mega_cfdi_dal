/*
 * Created on 10 feb 2018 ( Time 08:45:45 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "consignatario_contrato"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="consignatario_contrato", schema="contribuyente" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="ConsignatarioContratoEntity.countAll", query="SELECT COUNT(x) FROM ConsignatarioContratoEntity x" )
} )
public class ConsignatarioContratoEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_ConsignatarioContratoEntity")
	@SequenceGenerator(name = "gen_ConsignatarioContratoEntity", sequenceName = "contribuyente.consignatario_contrato_id_seq", allocationSize = 1)
    @Column(name="id", nullable=false)
    private Long       id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Temporal(TemporalType.DATE)
    @Column(name="fecha_inicio_contrato", nullable=false)
    private Date       fechaInicioContrato ;

    @Column(name="ruta_contrato", nullable=false, length=500)
    private String     rutaContrato ;

    @Column(name="activo")
    private Boolean    activo       ;

    @Temporal(TemporalType.DATE)
    @Column(name="fecha_fin_contrato", nullable=false)
    private Date       fechaFinContrato ;

    @Column(name="nombre_contrato", nullable=false, length=500)
    private String     nombreContrato ;

    @Column(name="extension_contrato", nullable=false, length=500)
    private String     extensionContrato ;

	// "idConsignatario" (column "id_consignatario") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="id_consignatario", referencedColumnName="id")
    private ConsignatarioEntity consignatario;


    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public ConsignatarioContratoEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Long id ) {
        this.id = id ;
    }
    public Long getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : fecha_inicio_contrato ( date ) 
    public void setFechaInicioContrato( Date fechaInicioContrato ) {
        this.fechaInicioContrato = fechaInicioContrato;
    }
    public Date getFechaInicioContrato() {
        return this.fechaInicioContrato;
    }

    //--- DATABASE MAPPING : ruta_contrato ( varchar ) 
    public void setRutaContrato( String rutaContrato ) {
        this.rutaContrato = rutaContrato;
    }
    public String getRutaContrato() {
        return this.rutaContrato;
    }

    //--- DATABASE MAPPING : activo ( bool ) 
    public void setActivo( Boolean activo ) {
        this.activo = activo;
    }
    public Boolean getActivo() {
        return this.activo;
    }

    //--- DATABASE MAPPING : fecha_fin_contrato ( date ) 
    public void setFechaFinContrato( Date fechaFinContrato ) {
        this.fechaFinContrato = fechaFinContrato;
    }
    public Date getFechaFinContrato() {
        return this.fechaFinContrato;
    }

    //--- DATABASE MAPPING : nombre_contrato ( varchar ) 
    public void setNombreContrato( String nombreContrato ) {
        this.nombreContrato = nombreContrato;
    }
    public String getNombreContrato() {
        return this.nombreContrato;
    }

    //--- DATABASE MAPPING : extension_contrato ( varchar ) 
    public void setExtensionContrato( String extensionContrato ) {
        this.extensionContrato = extensionContrato;
    }
    public String getExtensionContrato() {
        return this.extensionContrato;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------
    public void setConsignatario( ConsignatarioEntity consignatario ) {
        this.consignatario = consignatario;
    }
    public ConsignatarioEntity getConsignatario() {
        return this.consignatario;
    }


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(fechaInicioContrato);
        sb.append("|");
        sb.append(rutaContrato);
        sb.append("|");
        sb.append(activo);
        sb.append("|");
        sb.append(fechaFinContrato);
        sb.append("|");
        sb.append(nombreContrato);
        sb.append("|");
        sb.append(extensionContrato);
        return sb.toString(); 
    } 

}
