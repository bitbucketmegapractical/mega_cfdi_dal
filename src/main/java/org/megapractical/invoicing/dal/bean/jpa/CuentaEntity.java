/*
 * Created on 29 sep 2017 ( Time 08:33:33 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.List;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "cuenta"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="cuenta", schema="contribuyente" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="CuentaEntity.countAll", query="SELECT COUNT(x) FROM CuentaEntity x" )
} )
public class CuentaEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_CuentaEntity")
	@SequenceGenerator(name = "gen_CuentaEntity", sequenceName = "contribuyente.cuenta_id_seq", allocationSize = 1)
    @Column(name="id", nullable=false)
    private Long       id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="correo_electronico", length=100)
    private String     correoElectronico ;

    @Column(name="eliminado")
    private Boolean    eliminado    ;

	// "idContribuyente" (column "id_contribuyente") is not defined by itself because used as FK in a link 
	// "idContribuyenteAsociado" (column "id_contribuyente_asociado") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="id_contribuyente", referencedColumnName="id")
    private ContribuyenteEntity contribuyente;

    @ManyToOne
    @JoinColumn(name="id_contribuyente_asociado", referencedColumnName="id")
    private ContribuyenteEntity contribuyenteAsociado;
	
	@OneToMany(mappedBy="cuenta", targetEntity=TimbreTransferenciaEntity.class)
    private List<TimbreTransferenciaEntity> listOfTimbreTransferencia;


    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public CuentaEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Long id ) {
        this.id = id ;
    }
    public Long getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : correo_electronico ( varchar ) 
    public void setCorreoElectronico( String correoElectronico ) {
        this.correoElectronico = correoElectronico;
    }
    public String getCorreoElectronico() {
        return this.correoElectronico;
    }

    //--- DATABASE MAPPING : eliminado ( bool ) 
    public void setEliminado( Boolean eliminado ) {
        this.eliminado = eliminado;
    }
    public Boolean getEliminado() {
        return this.eliminado;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------
    public void setContribuyente( ContribuyenteEntity contribuyente ) {
        this.contribuyente = contribuyente;
    }
    public ContribuyenteEntity getContribuyente() {
        return this.contribuyente;
    }

	public void setContribuyenteAsociado(ContribuyenteEntity contribuyenteAsociado) {
		this.contribuyenteAsociado = contribuyenteAsociado;
	}
    public ContribuyenteEntity getContribuyenteAsociado() {
		return contribuyenteAsociado;
	}

	public void setListOfTimbreTransferencia( List<TimbreTransferenciaEntity> listOfTimbreTransferencia ) {
        this.listOfTimbreTransferencia = listOfTimbreTransferencia;
    }
    public List<TimbreTransferenciaEntity> getListOfTimbreTransferencia() {
        return this.listOfTimbreTransferencia;
    }

	//----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(correoElectronico);
        sb.append("|");
        sb.append(eliminado);
        return sb.toString(); 
    } 

}