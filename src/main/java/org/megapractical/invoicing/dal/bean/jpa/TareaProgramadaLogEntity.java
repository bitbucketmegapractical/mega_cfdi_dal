/*
 * Created on 27 Oct 2018 ( Time 09:13:55 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "tarea_programada_log"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="tarea_programada_log", schema="tareas_programadas" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="TareaProgramadaLogEntity.countAll", query="SELECT COUNT(x) FROM TareaProgramadaLogEntity x" )
} )
public class TareaProgramadaLogEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_TareaProgramadaLogEntity")
	@SequenceGenerator(name = "gen_TareaProgramadaLogEntity", sequenceName = "tareas_programadas.tarea_programada_log_id_seq", allocationSize = 1)
    @Column(name="id", nullable=false)
    private Long       id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Temporal(TemporalType.DATE)
    @Column(name="fecha_ejecutada", nullable=false)
    private Date       fechaEjecutada ;

    @Temporal(TemporalType.TIME)
    @Column(name="hora_ejecutada", nullable=false)
    private Date       horaEjecutada ;

	// "idTareaProgramada" (column "id_tarea_programada") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="id_tarea_programada", referencedColumnName="id")
    private TareaProgramadaEntity tareaProgramada;


    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public TareaProgramadaLogEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Long id ) {
        this.id = id ;
    }
    public Long getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : fecha_ejecutada ( date ) 
    public void setFechaEjecutada( Date fechaEjecutada ) {
        this.fechaEjecutada = fechaEjecutada;
    }
    public Date getFechaEjecutada() {
        return this.fechaEjecutada;
    }

    //--- DATABASE MAPPING : hora_ejecutada ( time ) 
    public void setHoraEjecutada( Date horaEjecutada ) {
        this.horaEjecutada = horaEjecutada;
    }
    public Date getHoraEjecutada() {
        return this.horaEjecutada;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------
    public void setTareaProgramada( TareaProgramadaEntity tareaProgramada ) {
        this.tareaProgramada = tareaProgramada;
    }
    public TareaProgramadaEntity getTareaProgramada() {
        return this.tareaProgramada;
    }


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(fechaEjecutada);
        sb.append("|");
        sb.append(horaEjecutada);
        return sb.toString(); 
    } 

}
