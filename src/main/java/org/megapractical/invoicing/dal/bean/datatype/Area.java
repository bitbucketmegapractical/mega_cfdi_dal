package org.megapractical.invoicing.dal.bean.datatype;

public enum Area {
	TESORERIA, RH, MERCADOS, CREDITO, FIDUCIARIO, FACTURAS_MANUALES
}