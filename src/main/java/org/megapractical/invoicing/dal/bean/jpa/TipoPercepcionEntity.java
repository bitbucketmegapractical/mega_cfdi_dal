/*
 * Created on 25 oct 2017 ( Time 09:30:28 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Persistent class for entity stored in table "tipo_percepcion"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name = "tipo_percepcion", schema = "nomencladores")
// Define named queries here
@NamedQueries({
		@NamedQuery(name = "TipoPercepcionEntity.countAll", query = "SELECT COUNT(x) FROM TipoPercepcionEntity x") })
public class TipoPercepcionEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	// ----------------------------------------------------------------------
	// ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
	// ----------------------------------------------------------------------
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_TipoPercepcionEntity")
	@SequenceGenerator(name = "gen_TipoPercepcionEntity", sequenceName = "nomencladores.tipo_percepcion_id_seq", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Long id;

	// ----------------------------------------------------------------------
	// ENTITY DATA FIELDS
	// ----------------------------------------------------------------------
	@Column(name = "codigo", length = 20)
	private String codigo;

	@Column(name = "valor", length = 150)
	private String valor;

	@Column(name = "eliminado")
	private Boolean eliminado;

	// ----------------------------------------------------------------------
	// CONSTRUCTOR(S)
	// ----------------------------------------------------------------------
	public TipoPercepcionEntity() {
		super();
	}

	// ----------------------------------------------------------------------
	// GETTER & SETTER FOR THE KEY FIELD
	// ----------------------------------------------------------------------
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	// ----------------------------------------------------------------------
	// GETTERS & SETTERS FOR FIELDS
	// ----------------------------------------------------------------------
	// --- DATABASE MAPPING : codigo ( varchar )
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return this.codigo;
	}

	// --- DATABASE MAPPING : valor ( varchar )
	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getValor() {
		return this.valor;
	}

	// --- DATABASE MAPPING : eliminado ( bool )
	public void setEliminado(Boolean eliminado) {
		this.eliminado = eliminado;
	}

	public Boolean getEliminado() {
		return this.eliminado;
	}

	// ----------------------------------------------------------------------
	// toString METHOD
	// ----------------------------------------------------------------------
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		sb.append(id);
		sb.append("]:");
		sb.append(codigo);
		sb.append("|");
		sb.append(valor);
		sb.append("|");
		sb.append(eliminado);
		return sb.toString();
	}

}