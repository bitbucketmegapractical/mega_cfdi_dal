package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Persistent class for entity stored in table "api_token"
 *
 * @author Maikel Guerra Ferrer
 *
 */
@Entity
@Table(name = "api_token", schema = "seguridad")
public class ApiToken implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_ApiToken")
	@SequenceGenerator(name = "gen_ApiToken", sequenceName = "seguridad.api_token_id_seq", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "token")
	private String token;

	@Column(name = "request_date")
	private LocalDateTime requestDate;

	@Column(name = "expire_at")
	private LocalDateTime expireAt;

	public ApiToken() {
	}

	public ApiToken(String token, LocalDateTime expireAt) {
		this.token = token;
		this.expireAt = expireAt;
		this.requestDate = LocalDateTime.now();
	}

	/* Getters and Setters */
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public LocalDateTime getExpireAt() {
		return expireAt;
	}

	public void setExpireAt(LocalDateTime expireAt) {
		this.expireAt = expireAt;
	}

	public LocalDateTime getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(LocalDateTime requestDate) {
		this.requestDate = requestDate;
	}

	/* toString */
	@Override
	public String toString() {
		return "ApiToken [id=" + id + ", token=" + token + ", requestDate=" + requestDate + ", expireAt=" + expireAt
				+ "]";
	}

}