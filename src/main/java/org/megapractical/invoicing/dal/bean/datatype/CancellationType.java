package org.megapractical.invoicing.dal.bean.datatype;

public enum CancellationType {
	CFDI, RET
}