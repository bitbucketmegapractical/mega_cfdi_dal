/*
 * Created on 21 ago 2017 ( Time 08:01:22 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
package org.megapractical.invoicing.dal.bean;

import java.io.Serializable;

import javax.validation.constraints.*;


public class Credencial implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @NotNull
    private Long id;

    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @NotNull
    private Long idContribuyente;

    @NotNull
    private Long idUsuario;


    private Boolean eliminado;

    @NotNull
    private Long idLenguaje;



    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Long id ) {
        this.id = id ;
    }

    public Long getId() {
        return this.id;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    public void setIdContribuyente( Long idContribuyente ) {
        this.idContribuyente = idContribuyente;
    }
    public Long getIdContribuyente() {
        return this.idContribuyente;
    }

    public void setIdUsuario( Long idUsuario ) {
        this.idUsuario = idUsuario;
    }
    public Long getIdUsuario() {
        return this.idUsuario;
    }

    public void setEliminado( Boolean eliminado ) {
        this.eliminado = eliminado;
    }
    public Boolean getEliminado() {
        return this.eliminado;
    }

    public void setIdLenguaje( Long idLenguaje ) {
        this.idLenguaje = idLenguaje;
    }
    public Long getIdLenguaje() {
        return this.idLenguaje;
    }


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
 
        public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append(id);
        sb.append("|");
        sb.append(idContribuyente);
        sb.append("|");
        sb.append(idUsuario);
        sb.append("|");
        sb.append(eliminado);
        sb.append("|");
        sb.append(idLenguaje);
        return sb.toString(); 
    } 


}
