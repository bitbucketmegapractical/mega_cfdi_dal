/*
 * Created on 12 jul 2017 ( Time 13:26:06 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package org.megapractical.invoicing.dal.bean.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;


import javax.persistence.*;

/**
 * Persistent class for entity stored in table "comprobante_cfdi_relacionado_uuid"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="comprobante_cfdi_relacionado_uuid", schema="facturacion" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="ComprobanteCfdiRelacionadoUuidEntity.countAll", query="SELECT COUNT(x) FROM ComprobanteCfdiRelacionadoUuidEntity x" )
} )
public class ComprobanteCfdiRelacionadoUuidEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_ComprobanteCfdiRelacionadoUuidEntity")
	@SequenceGenerator(name = "gen_ComprobanteCfdiRelacionadoUuidEntity", sequenceName = "facturacion.comprobante_cfdi_relacionado_uuid_id_seq", allocationSize = 1)
    @Column(name="id", nullable=false)
    private Long       id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="uudi", length=100)
    private String     uudi         ;

	// "idComprobanteCfdiRelacionado" (column "id_comprobante_cfdi_relacionado") is not defined by itself because used as FK in a link 


    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @ManyToOne
    @JoinColumn(name="id_comprobante_cfdi_relacionado", referencedColumnName="id")
    private ComprobanteCfdiRelacionadoEntity comprobanteCfdiRelacionado;


    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public ComprobanteCfdiRelacionadoUuidEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Long id ) {
        this.id = id ;
    }
    public Long getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : uudi ( varchar ) 
    public void setUudi( String uudi ) {
        this.uudi = uudi;
    }
    public String getUudi() {
        return this.uudi;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------
    public void setComprobanteCfdiRelacionado( ComprobanteCfdiRelacionadoEntity comprobanteCfdiRelacionado ) {
        this.comprobanteCfdiRelacionado = comprobanteCfdiRelacionado;
    }
    public ComprobanteCfdiRelacionadoEntity getComprobanteCfdiRelacionado() {
        return this.comprobanteCfdiRelacionado;
    }


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(uudi);
        return sb.toString(); 
    } 

}
