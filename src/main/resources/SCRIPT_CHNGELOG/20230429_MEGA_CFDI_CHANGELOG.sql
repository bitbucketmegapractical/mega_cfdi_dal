ALTER TABLE facturacion.cfdi ADD emisor_rfc varchar(20) NULL;

UPDATE facturacion.cfdi
SET emisor_rfc = contribuyente.rfc_activo
FROM contribuyente.contribuyente contribuyente
INNER JOIN facturacion.cfdi c ON contribuyente.id = c.id_emisor
WHERE contribuyente.id = c.id_emisor;

INSERT INTO nomencladores.tipo_archivo (codigo,valor) VALUES ('CANTXT','Cancelaciones (TXT)');

CREATE SEQUENCE archivo.archivo_cancelacion_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;
	
CREATE TABLE archivo.archivo_cancelacion (
	id bigint NOT NULL DEFAULT nextval('archivo.archivo_cancelacion_id_seq'::regclass),
	id_contribuyente bigint NOT NULL,
	id_tipo_archivo bigint NOT NULL,
	id_estado_archivo bigint NOT NULL,
	nombre varchar NULL,
	ruta varchar NULL,
	fecha_carga timestamp NULL,	
	ruta_acuses varchar NULL,	
	ruta_error varchar NULL,
	eliminado bool NULL DEFAULT false,
	CONSTRAINT archivo_cancelacion_pk PRIMARY KEY (id),
	CONSTRAINT archivo_cancelacion_uq UNIQUE (nombre),
	CONSTRAINT archivo_cancelacion_fk1 FOREIGN KEY (id_contribuyente) REFERENCES contribuyente.contribuyente(id),
	CONSTRAINT archivo_cancelacion_fk2 FOREIGN KEY (id_tipo_archivo) REFERENCES nomencladores.tipo_archivo(id),
	CONSTRAINT archivo_cancelacion_fk3 FOREIGN KEY (id_estado_archivo) REFERENCES nomencladores.estado_archivo(id)
);
CREATE INDEX archivo_cancelacion_idx1 ON archivo.archivo_cancelacion USING btree (id_contribuyente);
CREATE INDEX archivo_cancelacion_idx2 ON archivo.archivo_cancelacion USING btree (id_tipo_archivo);
CREATE INDEX archivo_cancelacion_idx3 ON archivo.archivo_cancelacion USING btree (id_estado_archivo);

CREATE SEQUENCE facturacion.bitacora_cancelacion_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;
	
CREATE TABLE facturacion.bitacora_cancelacion (
	id bigint NOT NULL DEFAULT nextval('facturacion.bitacora_cancelacion_id_seq'::regclass),
	id_archivo_cancelacion bigint NULL,
	tipo varchar(20) NOT NULL,
	emisor_rfc varchar(20) NULL,
	receptor_rfc varchar(20) NOT NULL,
	uuid varchar(100) NOT NULL,
	total varchar(100) NOT NULL,
	motivo varchar(10) NULL,
	uuid_sustitucion varchar(100) NULL,
	cancelado bool NULL DEFAULT false,
	cancelacion_en_proceso bool NULL DEFAULT false,
	solicitud_cancelacion bool NULL DEFAULT false,
	acuse_cancelacion text NULL,
	fecha_cancelacion timestamp NULL,
	CONSTRAINT bitacora_cancelacion_pk PRIMARY KEY (id),
	CONSTRAINT bitacora_cancelacion_uq UNIQUE (uuid),
	CONSTRAINT bitacora_cancelacion_fk2 FOREIGN KEY (id_archivo_cancelacion) REFERENCES archivo.archivo_cancelacion(id)
);

CREATE SEQUENCE nomencladores.cancelacion_codigo_respuesta_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;
	
CREATE TABLE nomencladores.cancelacion_codigo_respuesta (
	id bigint NOT NULL DEFAULT nextval('nomencladores.cancelacion_codigo_respuesta_id_seq'::regclass),
	codigo varchar(5) NOT NULL,
	valor varchar(500) NULL,
	descripcion varchar(500) NULL,
	eliminado bool NULL DEFAULT false,
	CONSTRAINT cancelacion_codigo_respuesta_pk PRIMARY KEY (id),
	CONSTRAINT cancelacion_codigo_respuesta_uq UNIQUE (codigo)
);

INSERT INTO nomencladores.cancelacion_codigo_respuesta(id, codigo, valor, descripcion, eliminado) VALUES
	(1, '201', 'Solicitud de cancelación exitosa', 'Se considera una solicitud de cancelación exitosa, sin embargo esto no asegura su cancelación', false),
	(2, '202', 'Folio Fiscal Previamente Cancelado', 'Se considera solicitud de cancelación previamente enviada. Estatus Cancelado ante el SAT.', false),
	(3, '203', 'Folio Fiscal No Correspondiente al Emisor', '', false),
	(4, '204', 'Folio Fiscal No Aplicable a Cancelación', '', false),
	(5, '205', 'Folio Fiscal No Aplicable a Cancelación', 'El sat da una prorroga de 48 hrs para que el comprobante aparezca con estatus Vigente posterior al envió por parte del Proveedor de Certificación de CFDI. Puede que algunos comprobantes no aparezcan al momento, es necesario esperar por lo menos 48 hrs.', false),
	(6, '206', 'UUID no corresponde a un CFDI del Sector Primario', '', false),
	(7, '207', 'No se especificó el motivo de cancelación o el motivo no es valido', 'El UUID sustitución no existe, está cancelado o tiene una fecha de emisión anterior a la fecha de emisión del comprobante original.', false),
	(8, '208', 'Folio Sustitución invalido', '', false),
	(9, '209', 'Folio Sustitución no requerido', '', false),
	(10, '210', 'La fecha de solicitud de cancelación es mayor a la fecha de declaración', '', false),
	(11, '211', 'La fecha de solicitud de cancelación límite para factura global', '', false),
	(12, '212', 'Relación no valida o inexistente', '', false),
	(13, '300', 'Usuario No Válido', '', false),
	(14, '301', 'XML Mal Formado', 'Este código de error se regresa cuando el request posee información invalida, ejemplo: un RFC de receptor no válido.', false),
	(15, '302', 'Sello Mal Formado', '', false),
	(16, '304', 'Certificado Revocado o Caduco', 'El certificado puede ser inválido por múltiples razones como son el tipo, la vigencia, etc.', false),
	(17, '305', 'Certificado Inválido', 'El certificado puede ser inválido por múltiples razones como son el tipo, la vigencia, etc.', false),
	(18, '309', 'Certificado Inválido', 'El certificado puede ser inválido por múltiples razones como son el tipo, la vigencia, etc.', false),
	(19, '310', 'CSD Inválido', '', false);

CREATE SEQUENCE notificacion.notificacion_cancelacion_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

CREATE TABLE notificacion.notificacion_cancelacion (
	id int8 NOT NULL DEFAULT nextval('notificacion.notificacion_cancelacion_id_seq'::regclass),
	id_archivo_cancelacion bigint NULL,
	recipient varchar(500) NULL DEFAULT NULL::character varying,
	subject varchar NULL,
	send_date timestamp NULL,
	reason text NULL DEFAULT NULL::character varying,	
	CONSTRAINT notificacion_cancelacion_pk PRIMARY KEY (id),
	CONSTRAINT notificacion_cancelacion_fk FOREIGN KEY (id_archivo_cancelacion) REFERENCES archivo.archivo_cancelacion(id)
);	