CREATE SEQUENCE seguridad.usuario_api_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

CREATE TABLE seguridad.usuario_api (
	id bigint NOT NULL DEFAULT nextval('seguridad.usuario_api_id_seq'::regclass),
	username varchar(100) NOT NULL,
	api_key varchar(250) NULL DEFAULT NULL::character varying,
	rol varchar NULL,
	enabled bool NULL DEFAULT false,
	cfdi_upload bool NULL DEFAULT false,
	cfdi_download bool NULL DEFAULT false,
	activo bool NULL,
	CONSTRAINT usuario_api_pk PRIMARY KEY (id),
	CONSTRAINT usuario_api_uq1 UNIQUE (username),
	CONSTRAINT usuario_api_uq2 UNIQUE (api_key)
);

INSERT INTO seguridad.usuario_api (id, username, api_key, rol, enabled, cfdi_upload, cfdi_download) VALUES
	 (1,'megacfdi','$2a$12$Ocd27.GCxcAICer1II7W..kgD.A2Z.P0qmZjwrrWp/FSlwzTOAfyS','ROLE_API',true,true,true),
	 (2,'bancomext','$2a$12$G1l55fnqJzEF4ZnT2Bys/uXoIMvklSJII2mh8u3/QCGEI2zviaBte','ROLE_API',true,false,true);

ALTER TABLE facturacion.cfdi DROP CONSTRAINT cfdi_fk1;
ALTER TABLE facturacion.cfdi DROP CONSTRAINT cfdi_fk4;
ALTER TABLE facturacion.cfdi DROP CONSTRAINT cfdi_fk3;

DROP INDEX facturacion.cfdi_idx2;
DROP INDEX facturacion.cfdi_idx3;
DROP INDEX facturacion.cfdi_idx4;

ALTER TABLE facturacion.cfdi RENAME CONSTRAINT cfdi_fk2 TO cfdi_fk1;

ALTER TABLE facturacion.cfdi DROP COLUMN id_estado_cfdi;
ALTER TABLE facturacion.cfdi DROP COLUMN cfdi_plus;
ALTER TABLE facturacion.cfdi DROP COLUMN importado;
ALTER TABLE facturacion.cfdi DROP COLUMN valido;
ALTER TABLE facturacion.cfdi DROP COLUMN serie_indicada;
ALTER TABLE facturacion.cfdi DROP COLUMN folio_indicado;

ALTER TABLE facturacion.cfdi ADD almacenamiento varchar(10) DEFAULT NULL;
UPDATE facturacion.cfdi SET almacenamiento = 'SERVER' WHERE uuid IS NOT NULL;

ALTER TABLE facturacion.cfdi ADD total float8 NULL;
ALTER TABLE facturacion.cfdi ADD fecha_hora_expedicion varchar DEFAULT NULL;
ALTER TABLE facturacion.cfdi ADD fecha_hora_timbrado varchar DEFAULT NULL;

UPDATE facturacion.cfdi c
SET total = co.total, fecha_hora_expedicion = co.fecha_hora_expedicion, fecha_hora_timbrado = co.fecha_hora_timbrado
FROM facturacion.comprobante co
WHERE c.id_comprobante = co.id;

ALTER TABLE facturacion.cfdi RENAME COLUMN id_comprobante TO tipo_comprobante;
ALTER TABLE facturacion.cfdi ALTER COLUMN tipo_comprobante TYPE varchar USING tipo_comprobante::varchar;

UPDATE facturacion.cfdi c
SET tipo_comprobante = t.codigo
FROM facturacion.comprobante co
INNER JOIN nomencladores.tipo_comprobante t ON co.id_tipo_comprobante = t.id
WHERE c.tipo_comprobante = CAST(co.id AS varchar);

CREATE SEQUENCE seguridad.api_token_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

CREATE TABLE seguridad.api_token (
	id bigint NOT NULL DEFAULT nextval('seguridad.api_token_id_seq'::regclass),
	"token" varchar NOT NULL,
	request_date timestamp NOT NULL,
	CONSTRAINT api_token_pk PRIMARY KEY (id),
	CONSTRAINT api_token_uq UNIQUE ("token")
);