ALTER TABLE facturacion.cfdi ALTER COLUMN ruta_xml TYPE text USING ruta_xml::text;
ALTER TABLE facturacion.cfdi ALTER COLUMN ruta_pdf TYPE text USING ruta_pdf::text;