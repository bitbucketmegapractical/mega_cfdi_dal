-- Table: archivo.archivo_factura_configuracion
CREATE SEQUENCE archivo.archivo_factura_configuracion_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE archivo.archivo_factura_configuracion_id_seq
    OWNER TO megadb01_userprod;
	
DROP TABLE IF EXISTS archivo.archivo_factura_configuracion;
CREATE TABLE archivo.archivo_factura_configuracion
(
    id bigint NOT NULL DEFAULT nextval('archivo.archivo_factura_configuracion_id_seq'::regclass),
    id_contribuyente bigint NOT NULL,
    envio_xml_pdf boolean,
    envio_xml boolean,
    envio_pdf boolean,
    consolidado_1x boolean,
    consolidado_2x boolean,
    directorio_xml_pdf_unico boolean,
    directorio_xml_pdf_separado boolean,
    compactado boolean,
    notificacion boolean,
    CONSTRAINT archivo_factura_configuracion_pk PRIMARY KEY (id),
    CONSTRAINT archivo_factura_configuracion_fk1 FOREIGN KEY (id_contribuyente)
        REFERENCES contribuyente.contribuyente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.archivo_factura_configuracion
    OWNER to megadb01_userprod;

COMMENT ON COLUMN archivo.archivo_factura_configuracion.consolidado_1x
    IS 'Genera consolidado (Pdf con todos los pdf generados) con 1 pdf por página.';

COMMENT ON COLUMN archivo.archivo_factura_configuracion.consolidado_2x
    IS 'Genera consolidado (Pdf con todos los pdf generados) con 1 pdf duplicado por página.';

COMMENT ON COLUMN archivo.archivo_factura_configuracion.directorio_xml_pdf_unico
    IS 'Genera directorio con todos los xml y pdf generados.';

COMMENT ON COLUMN archivo.archivo_factura_configuracion.directorio_xml_pdf_separado
    IS 'Genera directorio por separado (carpeta de xml y carpeta de pdf) de todos los xml y los pdf generados.';

DROP INDEX IF EXISTS archivo.archivo_factura_configuracion_idx1;
CREATE INDEX archivo_factura_configuracion_idx1
    ON archivo.archivo_factura_configuracion USING btree
    (id_contribuyente)
    TABLESPACE pg_default;
	
-- Table: archivo.archivo_factura
DROP SEQUENCE IF EXISTS archivo.archivo_factura_id_seq CASCADE;
CREATE SEQUENCE archivo.archivo_factura_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;
	
DROP TABLE IF EXISTS archivo.archivo_factura CASCADE;
CREATE TABLE archivo.archivo_factura
(
    id bigint NOT NULL DEFAULT nextval('archivo.archivo_factura_id_seq'::regclass),
    id_contribuyente bigint NOT NULL,
    id_tipo_archivo bigint NOT NULL,
    id_estado_archivo bigint NOT NULL,
    nombre character varying COLLATE pg_catalog."default",
    ruta character varying COLLATE pg_catalog."default",
    fecha_carga date,
    timbres_disponibles boolean,
    ruta_compactado character varying COLLATE pg_catalog."default",
    ruta_consolidado character varying COLLATE pg_catalog."default",
    ruta_error character varying COLLATE pg_catalog."default",
    eliminado boolean DEFAULT false,
    enviar_correo_cliente boolean,
    ejercicio character varying COLLATE pg_catalog."default",
    CONSTRAINT archivo_factura_pk PRIMARY KEY (id),
    CONSTRAINT archivo_factura_fk1 FOREIGN KEY (id_contribuyente)
        REFERENCES contribuyente.contribuyente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT archivo_factura_fk2 FOREIGN KEY (id_tipo_archivo)
        REFERENCES nomencladores.tipo_archivo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT archivo_factura_fk3 FOREIGN KEY (id_estado_archivo)
        REFERENCES nomencladores.estado_archivo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.archivo_factura
    OWNER to megadb01_userprod;

DROP INDEX IF EXISTS archivo.archivo_factura_idx1;
CREATE INDEX archivo_factura_idx1
    ON archivo.archivo_factura USING btree
    (id_contribuyente)
    TABLESPACE pg_default;

DROP INDEX IF EXISTS archivo.archivo_factura_idx2;
CREATE INDEX archivo_factura_idx2
    ON archivo.archivo_factura USING btree
    (id_tipo_archivo)
    TABLESPACE pg_default;

DROP INDEX IF EXISTS archivo.archivo_factura_idx3;
CREATE INDEX archivo_factura_idx3
    ON archivo.archivo_factura USING btree
    (id_estado_archivo)
    TABLESPACE pg_default;

-- Table: archivo.archivo_factura_estadistica
DROP SEQUENCE IF EXISTS archivo.archivo_factura_estadistica_id_seq CASCADE;
CREATE SEQUENCE archivo.archivo_factura_estadistica_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE archivo.archivo_factura_estadistica_id_seq
    OWNER TO megadb01_userprod;

DROP TABLE IF EXISTS archivo.archivo_factura_estadistica;
CREATE TABLE archivo.archivo_factura_estadistica
(
    id bigint NOT NULL DEFAULT nextval('archivo.archivo_factura_estadistica_id_seq'::regclass),
    id_archivo_factura bigint NOT NULL,
    facturas integer,
    facturas_timbradas integer,
    facturas_error integer,
    fecha_inicio_analisis date,
    hora_inicio_analisis time without time zone,
    fecha_fin_analisis date,
    hora_fin_analisis time without time zone,
    fecha_inicio_generacion_comprobante date,
    hora_inicio_generacion_comprobante time without time zone,
    fecha_fin_generacion_comprobante date,
    hora_fin_generacion_comprobante time without time zone,
    fecha_inicio_timbrado date,
    hora_inicio_timbrado time without time zone,
    fecha_fin_timbrado date,
    hora_fin_timbrado time without time zone,
    CONSTRAINT archivo_factura_estadistica_pk PRIMARY KEY (id),
    CONSTRAINT archivo_factura_estadistica_fk FOREIGN KEY (id_archivo_factura)
        REFERENCES archivo.archivo_factura (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.archivo_factura_estadistica
    OWNER to megadb01_userprod;

DROP INDEX IF EXISTS archivo.archivo_factura_estadistica_idx1;
CREATE INDEX archivo_factura_estadistica_idx1
    ON archivo.archivo_factura_estadistica USING btree
    (id_archivo_factura)
    TABLESPACE pg_default;
	
-- Table: archivo.complemento_pago_fichero
DROP SEQUENCE IF EXISTS archivo.complemento_pago_fichero_id_seq CASCADE;
CREATE SEQUENCE archivo.complemento_pago_fichero_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE archivo.complemento_pago_fichero_id_seq
    OWNER TO megadb01_userprod;
	
DROP TABLE IF EXISTS archivo.complemento_pago_fichero CASCADE;
CREATE TABLE archivo.complemento_pago_fichero
(
    id bigint NOT NULL DEFAULT nextval('archivo.complemento_pago_fichero_id_seq'::regclass),    
    codigo character varying COLLATE pg_catalog."default",
    nombre character varying COLLATE pg_catalog."default",
    cantidad_trama integer,
    eliminado boolean DEFAULT false,
    CONSTRAINT complemento_pago_fichero_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.complemento_pago_fichero
    OWNER to megadb01_userprod;

-- Table: archivo.complemento_pago_trama
DROP SEQUENCE IF EXISTS archivo.complemento_pago_trama_id_seq CASCADE;
CREATE SEQUENCE archivo.complemento_pago_trama_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE archivo.complemento_pago_trama_id_seq
    OWNER TO megadb01_userprod;
	
DROP TABLE IF EXISTS archivo.complemento_pago_trama CASCADE;
CREATE TABLE archivo.complemento_pago_trama
(
    id bigint NOT NULL DEFAULT nextval('archivo.complemento_pago_trama_id_seq'::regclass),
    id_complemento_pago_fichero bigint NOT NULL,
    nombre character varying(20) COLLATE pg_catalog."default",
    campos integer,
    longitud integer,
    descripcion character varying(250) COLLATE pg_catalog."default",
    principal boolean,
    requerido boolean,
    eliminado boolean,
    CONSTRAINT complemento_pago_trama_pk PRIMARY KEY (id),
    CONSTRAINT complemento_pago_trama_fk FOREIGN KEY (id_complemento_pago_fichero)
        REFERENCES archivo.complemento_pago_fichero (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.complemento_pago_trama
    OWNER to megadb01_userprod;

DROP INDEX IF EXISTS archivo.complemento_pago_trama_idx1;
CREATE INDEX complemento_pago_trama_idx1
    ON archivo.complemento_pago_trama USING btree
    (id_complemento_pago_fichero)
    TABLESPACE pg_default;
	
-- Table: archivo.complemento_pago_campo
DROP SEQUENCE IF EXISTS archivo.complemento_pago_campo_id_seq CASCADE;
CREATE SEQUENCE archivo.complemento_pago_campo_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE archivo.complemento_pago_campo_id_seq
    OWNER TO megadb01_userprod;

DROP TABLE IF EXISTS archivo.complemento_pago_campo;
CREATE TABLE archivo.complemento_pago_campo
(
    id bigint NOT NULL DEFAULT nextval('archivo.complemento_pago_campo_id_seq'::regclass),
    id_complemento_pago_trama bigint,
    nombre character varying COLLATE pg_catalog."default",
    posicion integer,
    longitud integer,
    longitud_minima integer,
    longitud_maxima integer,
    tipo_dato character varying COLLATE pg_catalog."default",
    descripcion character varying COLLATE pg_catalog."default",
    formato character varying COLLATE pg_catalog."default",
    validacion character varying COLLATE pg_catalog."default",
    requerido boolean,
    eliminado boolean,
    CONSTRAINT complemento_pago_campo_pk PRIMARY KEY (id),
    CONSTRAINT complemento_pago_campo_nombre_unico UNIQUE (nombre),
    CONSTRAINT complemento_pago_campo_fk1 FOREIGN KEY (id_complemento_pago_trama)
        REFERENCES archivo.complemento_pago_trama (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.complemento_pago_campo
    OWNER to megadb01_userprod;

DROP INDEX IF EXISTS archivo.complemento_pago_campo_idx1;
CREATE INDEX complemento_pago_campo_idx1
    ON archivo.complemento_pago_campo USING btree
    (id_complemento_pago_trama)
    TABLESPACE pg_default;

ALTER TABLE complementos.complemento_pago ADD id_archivo_factura bigint NULL;
ALTER TABLE complementos.complemento_pago ADD CONSTRAINT complemento_pago_fk4 FOREIGN KEY (id_archivo_factura) 
		REFERENCES archivo.archivo_factura(id)
		ON UPDATE NO ACTION
        ON DELETE NO ACTION;
CREATE INDEX complemento_pago_idx4 ON complementos.complemento_pago (id_archivo_factura);

ALTER TABLE facturacion.cfdi ADD id_archivo_factura bigint NULL;
ALTER TABLE facturacion.cfdi ADD CONSTRAINT cfdi_fk3 FOREIGN KEY (id_archivo_factura) 
		REFERENCES archivo.archivo_factura(id) 
		ON UPDATE NO ACTION
        ON DELETE NO ACTION;		
CREATE INDEX cfdi_idx4 ON facturacion.cfdi (id_archivo_factura);
	
INSERT INTO archivo.complemento_pago_fichero (id,codigo,nombre,cantidad_trama,eliminado) VALUES
	 (1,'CPV20','Complemento recepción de pagos 2.0',14,false);

INSERT INTO archivo.complemento_pago_trama (id,id_complemento_pago_fichero,nombre,campos,longitud,descripcion,principal,requerido,eliminado) VALUES
	 (1,1,'CP',2,3,'Trama principal del complemento de recepción de pagos.',true,true,false),
	 (2,1,'EMI',3,4,'Trama requerida para expresar la información del contribuyente emisor del comprobante.',false,true,false),
	 (3,1,'REC',8,9,'Trama requerida para precisar la información del contribuyente receptor del comprobante.',false,true,false),
	 (4,1,'COMP',4,5,'Trama requerida para expresar la información del comprobante.',false,true,false),
	 (5,1,'UUID',1,2,'Trama opcional para expresar la información de los uuid de los cfdi relacionados.',false,false,false),
	 (6,1,'PAG',16,17,'Trama requerida para expresar la información del pago.',false,true,false),
	 (7,1,'PAGDR',11,12,'Trama opcional para expresar la información de los documentos relacionados al pago.',false,true,false),
	 (8,1,'PAGIR',3,4,'Trama opcional para registrar los impuestos retenidos aplicables conforme al monto del pago recibido, expresados a la moneda de pago',false,false,false),
	 (9,1,'PAGIT',6,7,'Trama opcional para registrar los impuestos trasladados aplicables conforme al monto del pago recibido, expresados a la moneda de pago',false,false,false),
	 (10,1,'PAGDRIR',6,7,'Trama opcional para registrar los impuestos retenidos aplicables conforme al monto del pago recibido, expresados a la moneda del documento relacionado',false,false,false),
	 (11,1,'PAGDRIT',6,7,'Trama opcional para registrar los impuestos trasladados aplicables conforme al monto del pago recibido, expresados a la moneda del documento relacionado',false,false,false),
	 (12,1,'TOT',11,12,'Trama requerida para especificar el monto total de los pagos y el total de los impuestos, deben ser expresados en MXN',false,true,false),
	 (13,1,'CEND',NULL,1,'Trama final del archivo contenedor de las tramas del complemento de pagos.',false,true,false);

INSERT INTO archivo.complemento_pago_campo (id,id_complemento_pago_trama,nombre,posicion,longitud,longitud_minima,longitud_maxima,tipo_dato,descripcion,formato,validacion,requerido,eliminado) VALUES
	 (1,1,'Email',1,NULL,NULL,NULL,'String','Atributo opcional para indicar el correo electrónico del receptor de la factura',NULL,NULL,false,false),
	 (2,1,'Leyenda',2,NULL,NULL,NULL,'String','Atributo opcional para agregar una leyenda que se visualizará en la impresión de la factura (PDF)',NULL,NULL,false,false),
	 (3,2,'RfcEmisor',1,NULL,12,13,'String','Atributo requerido para registrar la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente emisor del comprobante.',NULL,NULL,true,false),
	 (4,2,'NombreEmisor',2,NULL,1,254,'String','Atributo opcional para registrar el nombre, denominación o razón social del contribuyente emisor del comprobante.',NULL,'[^|,]{1,254}?',false,false),
	 (5,2,'RegimenFiscal',3,NULL,NULL,NULL,'String','Atributo requerido para incorporar la clave del régimen del contribuyente emisor al que aplicará el efecto fiscal de este comprobante.',NULL,NULL,true,false),
	 (6,3,'RfcReceptor',1,NULL,12,13,'String','Atributo requerido para precisar la Clave del Registro Federal de Contribuyentes correspondiente al contribuyente receptor del comprobante.',NULL,NULL,true,false),
	 (7,3,'NombreReceptor',2,NULL,1,254,'String','Atributo opcional para precisar el nombre, denominación o razón social del contribuyente receptor del comprobante.',NULL,'[^|,]{1,254}?',false,false),
	 (8,3,'ResidenciaFiscal',4,NULL,NULL,NULL,'String','Atributo condicional para registrar la clave del país de residencia para efectos fiscales del receptor del comprobante, cuando se trate de un extranjero, y que es conforme con la especificación ISO 3166-1 alpha-3. Es requerido cuando se incluya el complemento de comercio exterior o se registre el atributo NumRegIdTrib.',NULL,NULL,false,false),
	 (9,3,'NumRegIdTrib',5,NULL,1,40,'String','Atributo condicional para expresar el número de registro de identidad fiscal del receptor cuando sea residente en el  extranjero. Es requerido cuando se incluya el complemento de comercio exterior.',NULL,NULL,false,false),
	 (10,4,'Serie',1,NULL,1,25,'String','Atributo opcional para precisar la serie para control interno del contribuyente. Este atributo acepta una cadena de caracteres.',NULL,'[^|,]{1,25}?',false,false),
	 (11,4,'Folio',2,NULL,1,40,'String','Atributo opcional para control interno del contribuyente que expresa el folio del comprobante, acepta una cadena de caracteres.',NULL,'[^|,]{1,40}?',false,false),
	 (12,4,'LugarExpedicion',3,5,NULL,NULL,'String','Atributo requerido para incorporar el código postal del lugar de expedición del comprobante (domicilio de la matriz o de la sucursal).',NULL,NULL,true,false),
	 (13,5,'Uuid',1,36,NULL,NULL,'String','Atributo requerido para registrar el folio fiscal (UUID) de un CFDI relacionado con el presente comprobante, por ejemplo: Si el CFDI relacionado es un comprobante de traslado que sirve para registrar el movimiento de la mercancía. Si este comprobante se usa como nota de crédito o nota de débito del comprobante relacionado. Si este comprobante es una devolución sobre el comprobante relacionado. Si éste sustituye a una factura cancelada.',NULL,'[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12}?',true,false),
	 (14,6,'FechaPago',2,NULL,NULL,NULL,'String','Atributo requerido para expresar la fecha y hora en la que el beneficiario recibe el pago. Se expresa en la forma aaaa-mm-ddThh:mm:ss, de acuerdo con la especificación ISO 8601.En caso de no contar ','aaaa-mm-ddThh:mm:ss',NULL,true,false),
	 (15,6,'FormaDePagoP',3,NULL,NULL,NULL,'String','Atributo requerido para expresar la clave de la forma en que se realiza el pago.',NULL,NULL,true,false),
	 (16,6,'Monto',4,NULL,NULL,NULL,'BigDecimal','Atributo requerido para expresar el importe del pago',NULL,NULL,true,false),
	 (17,6,'MonedaP',5,NULL,NULL,NULL,'String','Atributo requerido para identificar la clave de la moneda utilizada para realizar el pago, cuando se usa moneda nacional se registra MXN. El atributo Pagos:Pago:Monto y los atributos TotalImpuestosRetenidos, TotalImpuestosTrasladados, Traslados:Traslado:Importe y Retenciones:Retencion:Importe del nodo Pago:Impuestos deben ser expresados en esta moneda. Conforme con la especificación ISO 4217.',NULL,NULL,true,false),
	 (18,6,'TipoCambioP',6,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el tipo de cambio de la moneda a la fecha en que se realizó el pago. El valor debe reflejar el número de pesos mexicanos que equivalen a una unidad de la divisa señalada en el atributo MonedaP. Es requerido cuando el atributo MonedaP es diferente a MXN.',NULL,NULL,false,false),
	 (19,6,'NumOperacion',7,NULL,1,100,'String','Atributo condicional para expresar el número de cheque, número de autorización, número de referencia, clave de rastreo en caso de ser SPEI, línea de captura o algún número de referencia análogo que identifique la operación que ampara el pago efectuado',NULL,'[^|]{1,100}?',false,false),
	 (20,6,'RfcEmisorCtaOrd',8,NULL,12,13,'String','Atributo condicional para expresar la clave RFC de la entidad emisora de la cuenta origen, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc., en caso de ser extranjero colocar XEXX010101000, considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.',NULL,'XEXX010101000|[A-Z&amp;Ñ]{3}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]?',false,false),
	 (21,6,'CtaOrdenante',9,NULL,10,50,'String','Atributo condicional para incorporar el número de la cuenta con la que se realizó el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago',NULL,'[A-Z0-9_]{10,50}?',false,false),
	 (22,6,'NomBancoOrdExt',10,NULL,1,300,'String','Atributo condicional para expresar el nombre del banco ordenante, es requerido en caso de ser extranjero. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.',NULL,'[^|]{1,300}?',false,false),
	 (23,6,'RfcEmisorCtaBen',11,NULL,12,13,'String','Atributo condicional para expresar la clave RFC de la entidad operadora de la cuenta destino, es decir, la operadora, el banco, la institución financiera, emisor de monedero electrónico, etc. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.',NULL,NULL,false,false),
	 (24,6,'CtaBeneficiario',12,NULL,10,50,'String','Atributo condicional para incorporar el número de cuenta en donde se recibió el pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.',NULL,'[A-Z0-9_]{10,50}?',false,false),
	 (25,6,'TipoCadPago',13,NULL,NULL,NULL,'String','Atributo condicional para identificar la clave del tipo de cadena de pago que genera la entidad receptora del pago. Considerar las reglas de obligatoriedad publicadas en la página del SAT para éste atributo de acuerdo con el catálogo catCFDI:c_FormaPago.',NULL,NULL,false,false),
	 (26,6,'CadPago',14,NULL,1,8192,'String','Atributo condicional para expresar la cadena original del comprobante de pago generado por la entidad emisora de la cuenta beneficiaria. Es requerido en caso de que el atributo TipoCadPago contenga información.',NULL,NULL,false,false),
	 (27,6,'CertPago',15,NULL,NULL,NULL,'String','Atributo condicional que sirve para incorporar el certificado que ampara al pago, como una cadena de texto en formato base 64. Es requerido en caso de que el atributo TipoCadPago contenga información.',NULL,NULL,false,false),
	 (28,6,'SelloPago',16,NULL,NULL,NULL,'String','Atributo condicional para integrar el sello digital que se asocie al pago. La entidad que emite el comprobante de pago, ingresa una cadena original y el sello digital en una sección de dicho comprobante, este sello digital es el que se debe registrar en este campo. Debe ser expresado como una cadena de texto en formato base 64. Es requerido en caso de que el atributo TipoCadPago contenga información.',NULL,NULL,false,false),
	 (29,7,'IdDocumento',2,NULL,16,36,'String','Atributo requerido para expresar el identificador del documento relacionado con el pago. Este dato puede ser un Folio Fiscal de la Factura Electrónica o bien el número de operación de un documento digital.',NULL,'[a-f0-9A-F]{8}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{4}-[a-f0-9A-F]{12})|([0-9]{3}-[0-9]{2}-[0-9]{9}?',true,false),
	 (30,7,'ObjetoImpDR',11,NULL,NULL,NULL,'String','Atributo requerido para expresar si el pago del documento relacionado es objeto o no de impuesto',NULL,NULL,true,false),
	 (31,7,'MonedaDR',5,NULL,NULL,NULL,'String','Atributo requerido para identificar la clave de la moneda utilizada en los importes del documento relacionado, cuando se usa moneda nacional o el documento relacionado no especifica la moneda se registra MXN. Los importes registrados en los atributos “ImpSaldoAnt”, “ImpPagado” e “ImpSaldoInsoluto” de éste nodo, deben corresponder a esta moneda. Conforme con la especificación ISO 4217.',NULL,NULL,true,false),
	 (32,7,'EquivalenciaDR',6,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el tipo de cambio conforme con la moneda registrada en el documento relacionado. Es requerido cuando la moneda del documento relacionado es distinta de la moneda de pago. Se debe registrar el número de unidades de la moneda señalada en el documento relacionado que equivalen a una unidad de la moneda del pago. Por ejemplo: El documento relacionado se registra en USD El pago se realiza por 100 EUR. Este atributo se registra como 1.114700 USD/EUR. El importe pagado equivale a 100 EUR * 1.114700 USD/EUR = 111.47 USD.',NULL,NULL,false,false),
	 (33,7,'SerieDR',3,NULL,1,25,'String','Atributo opcional para precisar la serie del comprobante para control interno del contribuyente, acepta una cadena de caracteres.',NULL,'[^|]{1,25}?',false,false),
	 (34,7,'FolioDR',4,NULL,1,40,'String','Atributo opcional para precisar el folio del comprobante para control interno del contribuyente, acepta una cadena de caracteres.',NULL,'[^|]{1,40}?',false,false),
	 (35,7,'NumParcialidad',7,NULL,NULL,NULL,'Integer','Atributo condicional para expresar el número de parcialidad que corresponde al pago. Es requerido cuando MetodoDePagoDR contiene: “PPD” Pago en parcialidades o diferido.',NULL,'[1-9][0-9]{0,2}?',false,false),
	 (36,7,'ImpSaldoAnt',8,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el monto del saldo insoluto de la parcialidad anterior. Es requerido cuando MetodoDePagoDR contiene: “PPD” Pago en parcialidades o diferido.En el caso de que sea la primer parcialidad este campo debe contener el importe total del documento relacionado.',NULL,NULL,false,false),
	 (37,7,'ImpPagado',9,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el importe pagado para el documento relacionado. Es obligatorio cuando exista más de un documento relacionado o cuando existe un documento relacionado y el TipoCambioDR tiene un valor.',NULL,NULL,false,false),
	 (38,7,'ImpSaldoInsoluto',10,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar la diferencia entre el importe del saldo anterior y el monto del pago. Es requerido cuando MetodoDePagoDR contiene: “PPD” Pago en parcialidades o diferido.',NULL,NULL,false,false),
	 (39,6,'IdPago',1,NULL,1,3,'String','Atributo requerido para expresar el identificador del pago',NULL,NULL,true,false),
	 (40,7,'IdPagoDR',1,NULL,1,3,'String','Atributo requerido para expresar el identificador del pago en el documento relacionado',NULL,NULL,true,false),
	 (41,4,'Exportacion',4,NULL,NULL,NULL,'String','Atributo requerido para expresar si el comprobante ampara una operación de exportación',NULL,NULL,true,false),
	 (42,3,'DomicilioFiscalReceptor',6,5,NULL,NULL,'String','Atributo requerido para registrar el código postal del domicilio fiscal del receptor del comprobante',NULL,NULL,true,false),
	 (43,3,'RegimenFiscalReceptor',7,NULL,NULL,NULL,'String','Atributo requerido para incorporar la clave del régimen fiscal del contribuyente receptor al que aplicará el efecto fiscal de este comprobante',NULL,NULL,true,false),
	 (44,3,'UsoCFDI',8,NULL,NULL,NULL,'String','Atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI',NULL,NULL,true,false),
	 (45,8,'IdPagoR',1,NULL,NULL,NULL,'String','Atributo requerido para expresar el identificador del pago',NULL,NULL,true,false),
	 (46,8,'RImpuestoP',2,NULL,NULL,NULL,'String','Atributo requerido para señalar la clave del tipo de impuesto retenido conforme al monto del pago',NULL,NULL,true,false),
	 (47,8,'RImporteP',3,NULL,NULL,NULL,'BigDecimal','Atributo requerido para señalar el importe del impuesto retenido conforme al monto del pago. No se permiten valores negativos',NULL,NULL,true,false),
	 (48,9,'IdPagoT',1,NULL,NULL,NULL,'String','Atributo requerido para expresar el identificador del pago',NULL,NULL,true,false),
	 (49,9,'TBaseP',2,NULL,NULL,NULL,'BigDecimal','Atributo requerido para señalar la suma de los atributos BaseDR de los documentos relacionados del impuesto trasladado. No se permiten valores negativos',NULL,NULL,true,false),
	 (50,9,'TImpuestoP',3,NULL,NULL,NULL,'String','Atributo requerido para señalar la clave del tipo de impuesto trasladado conforme al monto del pago',NULL,NULL,true,false),
	 (51,9,'TTipoFactorP',4,NULL,NULL,NULL,'String','Atributo requerido para señalar la clave del tipo de factor que se aplica a la base del impuesto',NULL,NULL,true,false),
	 (52,9,'TTasaOCuotaP',5,NULL,NULL,NULL,'BigDecimal','Atributo condicional para señalar el valor de la tasa o cuota del impuesto que se traslada en los documentos relacionados',NULL,NULL,false,false),
	 (53,9,'TImporteP',6,NULL,NULL,NULL,'BigDecimal','Atributo condicional para señalar la suma del impuesto trasladado, agrupado por ImpuestoP, TipoFactorP y TasaOCuotaP. No se permiten valores negativos',NULL,NULL,true,false),
	 (54,10,'RIdDocumento',1,NULL,NULL,NULL,'String','Atributo requerido para expresar el identificador del documento relacionado',NULL,NULL,true,false),
	 (55,10,'RBaseDR',2,NULL,NULL,NULL,'BigDecimal','Atributo requerido para señalar la base para el cálculo de la retención conforme al monto del pago, aplicable al documento relacionado, la determinación de la base se realiza de acuerdo con las disposiciones fiscales vigentes. No se permiten valores negativos.',NULL,NULL,true,false),
	 (56,10,'RImpuestoDR',3,NULL,NULL,NULL,'String','Atributo requerido para señalar la clave del tipo de impuesto retenido conforme al monto del pago, aplicable al documento relacionado',NULL,NULL,true,false),
	 (57,10,'RTipoFactorDR',4,NULL,NULL,NULL,'String','Atributo requerido para señalar la clave del tipo de factor que se aplica a la base del impuesto',NULL,NULL,true,false),
	 (58,10,'RTasaOCuotaDR',5,NULL,NULL,NULL,'BigDecimal','Atributo requerido para señalar el valor de la tasa o cuota del impuesto que se retiene',NULL,NULL,true,false),
	 (59,10,'RImporteDR',6,NULL,NULL,NULL,'BigDecimal','Atributo requerido para señalar el importe del impuesto retenido conforme al monto del pago, aplicable al documento relacionado. No se permiten valores negativos',NULL,NULL,true,false),
	 (60,11,'TIdDocumento',1,NULL,NULL,NULL,'String','Atributo requerido para expresar el identificador del documento relacionado',NULL,NULL,true,false),
	 (61,11,'TBaseDR',2,NULL,NULL,NULL,'BigDecimal','Atributo requerido para señalar la base para el cálculo del impuesto trasladado conforme al monto del pago, aplicable al documento relacionado, la determinación de la base se realiza de acuerdo con las disposiciones fiscales vigentes. No se permiten valores negativos',NULL,NULL,true,false),
	 (62,11,'TImpuestoDR',3,NULL,NULL,NULL,'String','Atributo requerido para señalar la clave del tipo de impuesto trasladado conforme al monto del pago, aplicable al documento relacionado',NULL,NULL,true,false),
	 (63,11,'TTipoFactorDR',4,NULL,NULL,NULL,'String','Atributo requerido para señalar la clave del tipo de factor que se aplica a la base del impuesto',NULL,NULL,true,false),
	 (64,11,'TTasaOCuotaDR',5,NULL,NULL,NULL,'BigDecimal','Atributo condicional para señalar el valor de la tasa o cuota del impuesto que se traslada. Es requerido cuando el atributo TipoFactorDR contenga una clave que corresponda a Tasa o Cuota',NULL,NULL,false,false),
	 (65,11,'TImporteDR',6,NULL,NULL,NULL,'BigDecimal','Atributo condicional para señalar el importe del impuesto trasladado conforme al monto del pago, aplicable al documento relacionado. No se permiten valores negativos. Es requerido cuando el tipo factor sea Tasa o Cuota',NULL,NULL,false,false),
	 (66,12,'TotalRetencionesIVA',1,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el total de los impuestos retenidos de IVA que se desprenden de los pagos. No se permiten valores negativos',NULL,NULL,false,false),
	 (67,12,'TotalRetencionesISR',2,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el total de los impuestos retenidos de ISR que se desprenden de los pagos. No se permiten valores negativos',NULL,NULL,false,false),
	 (68,12,'TotalRetencionesIEPS',3,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el total de los impuestos retenidos de IEPS que se desprenden de los pagos. No se permiten valores negativos',NULL,NULL,false,false),
	 (69,12,'TotalTrasladosBaseIVA16',4,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el total de la base de IVA trasladado a la tasa del 16% que se desprende de los pagos. No se permiten valores negativos',NULL,NULL,false,false),
	 (70,12,'TotalTrasladosImpuestoIVA16',5,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el total de los impuestos de IVA trasladado a la tasa del 16% que se desprenden de los pagos. No se permiten valores negativos',NULL,NULL,false,false),
	 (71,12,'TotalTrasladosBaseIVA8',6,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el total de la base de IVA trasladado a la tasa del 8% que se desprende de los pagos. No se permiten valores negativos',NULL,NULL,false,false),
	 (72,12,'TotalTrasladosImpuestoIVA8',7,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el total de los impuestos de IVA trasladado a la tasa del 8% que se desprenden de los pagos. No se permiten valores negativos',NULL,NULL,false,false),
	 (73,12,'TotalTrasladosBaseIVA0',8,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el total de la base de IVA trasladado a la tasa del 0% que se desprende de los pagos. No se permiten valores negativos',NULL,NULL,false,false),
	 (74,12,'TotalTrasladosImpuestoIVA0',9,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el total de los impuestos de IVA trasladado a la tasa del 0% que se desprenden de los pagos. No se permiten valores negativos',NULL,NULL,false,false),
	 (75,12,'TotalTrasladosBaseIVAExento',10,NULL,NULL,NULL,'BigDecimal','Atributo condicional para expresar el total de la base de IVA trasladado exento que se desprende de los pagos. No se permiten valores negativos',NULL,NULL,false,false),
	 (76,12,'MontoTotalPagos',11,NULL,NULL,NULL,'BigDecimal','Atributo requerido para expresar el total de los pagos que se desprenden de los nodos Pago. No se permiten valores negativos',NULL,NULL,true,false),
	 (77,3,'Residente',3,NULL,NULL,NULL,'String','Atributo requerido para expresar la residencia (Nacional o Extranjero) del contribuyente receptor',NULL,NULL,true,false);

INSERT INTO nomencladores.tipo_archivo (id,codigo,valor,eliminado)
	VALUES (1001000000000000004,'CP20TXT','Pagos v2.0 (TXT)',false);
ALTER TABLE contribuyente.contribuyente ADD pagos_subidos int4 NULL;