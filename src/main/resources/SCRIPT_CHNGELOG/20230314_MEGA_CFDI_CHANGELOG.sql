INSERT INTO nomencladores.tipo_regimen (id, codigo, valor, eliminado) VALUES 
	(1001000000000000013, '13', 'Indemnización o Separación', false);

ALTER TABLE facturacion.nomina ADD curp varchar(50) NULL;
ALTER TABLE facturacion.nomina ADD numero_empleado varchar(100) NULL;
ALTER TABLE facturacion.nomina ADD uuid varchar(100) NULL;
ALTER TABLE facturacion.nomina ADD fecha_hora_expedicion varchar NULL;
ALTER TABLE facturacion.nomina ADD fecha_hora_timbrado varchar NULL;
ALTER TABLE facturacion.nomina ADD almacenamiento varchar(10) NULL DEFAULT NULL::character varying;
ALTER TABLE facturacion.nomina ADD cancelacion_en_proceso bool NULL DEFAULT false;
ALTER TABLE facturacion.nomina ADD solicitud_cancelacion bool NULL DEFAULT false;
ALTER TABLE facturacion.nomina ADD acuse_cancelacion text NULL;
ALTER TABLE facturacion.nomina ADD fecha_cancelado date NULL;
ALTER TABLE facturacion.nomina ADD hora_cancelado time NULL;
ALTER TABLE facturacion.nomina ADD CONSTRAINT nomina_uq UNIQUE (uuid);

ALTER TABLE facturacion.nomina ADD id_emisor bigint NULL;
ALTER TABLE facturacion.nomina ADD CONSTRAINT nomina_fk2 FOREIGN KEY (id_emisor) REFERENCES contribuyente.contribuyente(id);
CREATE INDEX nomina_idx2 ON facturacion.nomina (id_emisor);

UPDATE facturacion.nomina n
SET uuid = c.folio_fiscal, fecha_hora_expedicion = c.fecha_hora_expedicion, fecha_hora_timbrado = c.fecha_hora_timbrado
FROM facturacion.comprobante_nomina cn
INNER JOIN facturacion.comprobante c ON cn.id_comprobante = c.id
WHERE cn.id_nomina = n.id;

UPDATE facturacion.nomina n
SET id_emisor = an.id_contribuyente
FROM facturacion.comprobante_nomina cn
INNER JOIN archivo.archivo_nomina an ON cn.id_archivo_nomina = an.id
WHERE cn.id_nomina = n.id;

UPDATE facturacion.nomina n
SET curp = nr.curp, numero_empleado = nr.numero_empleado
FROM facturacion.nomina_receptor nr
WHERE nr.id_nomina = n.id;

UPDATE facturacion.nomina n SET almacenamiento = 'SERVER' 
WHERE ruta_xml IS NOT NULL
AND ruta_pdf IS NOT NULL;

DROP TABLE facturacion.nomina_emisor;
DROP TABLE facturacion.nomina_subcontratacion;
DROP TABLE facturacion.nomina_receptor;
DROP TABLE facturacion.nomina_separacion_indemnizacion;
DROP TABLE facturacion.nomina_otro_pago;
DROP TABLE facturacion.nomina_jubilacion_pension_retiro;
DROP TABLE facturacion.nomina_incapacidad;
DROP TABLE facturacion.nomina_hora_extra;
DROP TABLE facturacion.nomina_deduccion;
DROP TABLE facturacion.nomina_deducciones;
DROP TABLE facturacion.nomina_percepcion;
DROP TABLE facturacion.nomina_percepciones;
DROP TABLE facturacion.comprobante_nomina;
DROP SEQUENCE facturacion.comprobante_nomina_id_seq;
DROP SEQUENCE facturacion.nomina_deduccion_id_seq;
DROP SEQUENCE facturacion.nomina_deducciones_id_seq;
DROP SEQUENCE facturacion.nomina_emisor_id_seq;
DROP SEQUENCE facturacion.nomina_hora_extra_id_seq;
DROP SEQUENCE facturacion.nomina_incapacidad_id_seq;
DROP SEQUENCE facturacion.nomina_jubilacion_pension_retiro_id_seq;
DROP SEQUENCE facturacion.nomina_otro_pago_id_seq;
DROP SEQUENCE facturacion.nomina_percepcion_id_seq;
DROP SEQUENCE facturacion.nomina_percepciones_id_seq;
DROP SEQUENCE facturacion.nomina_receptor_id_seq;
DROP SEQUENCE facturacion.nomina_separacion_indemnizacion_id_seq;
DROP SEQUENCE facturacion.nomina_subcontratacion_id_seq;

CREATE SEQUENCE clientes.bancomext_notification_error_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

CREATE TABLE clientes.bancomext_notification_error (
	id bigint NOT NULL DEFAULT nextval('clientes.bancomext_notification_error_id_seq'::regclass),
	recipients varchar(500) NULL DEFAULT NULL::character varying,		
	receiver_rfc varchar(100) NULL DEFAULT NULL::character varying,
	serie varchar(100) NULL DEFAULT NULL::character varying,
	folio varchar(100) NULL DEFAULT NULL::character varying,
	uuid varchar(100) NULL DEFAULT NULL::character varying,
	xml_file_name varchar(250) NULL DEFAULT NULL::character varying,
	xml_path varchar(250) NULL DEFAULT NULL::character varying,
	pdf_file_name varchar(250) NULL DEFAULT NULL::character varying,
	pdf_path varchar(250) NULL DEFAULT NULL::character varying,
	file_name varchar(250) NULL DEFAULT NULL::character varying,
	send_date timestamp NULL,
	reason text NULL DEFAULT NULL::character varying,	
	CONSTRAINT bancomext_notification_error_pk PRIMARY KEY (id)
);
