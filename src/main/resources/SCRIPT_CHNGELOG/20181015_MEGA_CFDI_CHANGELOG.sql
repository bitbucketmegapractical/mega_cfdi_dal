CREATE SEQUENCE contribuyente.contribuyente_plantilla_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1001000000000000001
  CACHE 1;
ALTER TABLE contribuyente.contribuyente_plantilla_id_seq
  OWNER TO postgres;
  
CREATE TABLE contribuyente.contribuyente_plantilla
(
    id bigint NOT NULL DEFAULT nextval('contribuyente.contribuyente_plantilla_id_seq'::regclass),
    id_contribuyente bigint NOT NULL,
    codigo_tipo_comprobante character varying(20) COLLATE pg_catalog."default",
	nombre_plantilla character varying(20) COLLATE pg_catalog."default",
    habilitado boolean,
    observaciones boolean default false,
    CONSTRAINT contribuyente_plantilla_pk PRIMARY KEY (id),
    CONSTRAINT contribuyente_plantilla_fk1 FOREIGN KEY (id_contribuyente)
        REFERENCES contribuyente.contribuyente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE contribuyente.contribuyente_plantilla
    OWNER to postgres;

CREATE INDEX contribuyente_plantilla_idx
    ON contribuyente.contribuyente_plantilla USING btree
    (id_contribuyente)
    TABLESPACE pg_default;
	
CREATE SEQUENCE contribuyente.contribuyente_plantilla_subreporte_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1001000000000000001
  CACHE 1;
ALTER TABLE contribuyente.contribuyente_plantilla_subreporte_id_seq
  OWNER TO postgres;
  
CREATE TABLE contribuyente.contribuyente_plantilla_subreporte
(
    id bigint NOT NULL DEFAULT nextval('contribuyente.contribuyente_plantilla_subreporte_id_seq'::regclass),
    id_contribuyente_plantilla bigint NOT NULL,
    codigo character varying(20) COLLATE pg_catalog."default",
	nombre_plantilla character varying(20) COLLATE pg_catalog."default",
    habilitado boolean,
    CONSTRAINT contribuyente_plantilla_subreporte_pk PRIMARY KEY (id),
    CONSTRAINT contribuyente_plantilla_subreporte_fk1 FOREIGN KEY (id_contribuyente_plantilla)
        REFERENCES contribuyente.contribuyente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE contribuyente.contribuyente_plantilla_subreporte
    OWNER to postgres;

CREATE INDEX contribuyente_plantilla_subreporte_idx
    ON contribuyente.contribuyente_plantilla_subreporte USING btree
    (id_contribuyente_plantilla)
    TABLESPACE pg_default;	