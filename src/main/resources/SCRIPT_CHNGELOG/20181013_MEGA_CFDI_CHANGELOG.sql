﻿

ALTER TABLE contribuyente.contribuyente ADD COLUMN fecha_inicio_plan DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE contribuyente.contribuyente ADD COLUMN fecha_fin_plan DATE NOT NULL DEFAULT now() + interval '10 days';


ALTER TABLE contribuyente.contribuyente ADD COLUMN nominas_subidas INT NOT NULL DEFAULT 0;
update contribuyente.contribuyente set nominas_subidas = 0;

