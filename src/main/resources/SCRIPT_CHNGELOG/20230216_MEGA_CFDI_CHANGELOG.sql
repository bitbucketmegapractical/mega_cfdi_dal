-- Table: nomencladores.retencion_tipo_pago
DROP sequence if EXISTS nomencladores.retencion_tipo_pago_id_seq;
CREATE SEQUENCE nomencladores.retencion_tipo_pago_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE nomencladores.retencion_tipo_pago_id_seq
    OWNER TO megadb01_userprod;

DROP TABLE IF EXISTS nomencladores.retencion_tipo_pago;    
CREATE TABLE nomencladores.retencion_tipo_pago (
	id bigint NOT NULL DEFAULT nextval('nomencladores.retencion_tipo_pago_id_seq'::regclass),
	tipo_pago varchar(20) NULL,
	descripcion varchar(100) NULL,
	tipo_impuesto varchar(20) NULL,
	eliminado bool NULL,
	CONSTRAINT retencion_tipo_pago_pk PRIMARY KEY (id)
);

INSERT INTO nomencladores.retencion_tipo_pago (id, tipo_pago, descripcion, tipo_impuesto, eliminado) VALUES
	(1, '01', 'Pago definitivo IVA', '002', false),
	(2, '02', 'Pago definitivo IEPS', '003', false),
	(3, '03', 'Pago definitivo ISR ', '001', false),
	(4, '04', 'Pago provisional ISR', '001', false);

-- Table: archivo.bancomex_archivo
DROP sequence if EXISTS archivo.bancomex_archivo_id_seq;
CREATE SEQUENCE archivo.bancomex_archivo_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE archivo.bancomex_archivo_id_seq
    OWNER TO megadb01_userprod;

DROP TABLE IF EXISTS archivo.bancomex_archivo;
CREATE TABLE archivo.bancomex_archivo (
	id bigint NOT NULL DEFAULT nextval('archivo.bancomex_archivo_id_seq'::regclass),
	id_contribuyente int8 NOT NULL,
	area varchar NOT NULL,
	tipo_archivo varchar NOT NULL,
	origen varchar NOT NULL,
	status varchar NOT NULL,
	nombre varchar NULL,
	fecha_carga date NULL,
	ruta varchar NULL,	
	ruta_compactado varchar NULL,
	ruta_error varchar NULL,
	eliminado bool NULL DEFAULT false,	
	ejercicio varchar NULL,
	CONSTRAINT bancomex_archivo_pk PRIMARY KEY (id),
	CONSTRAINT bancomex_archivo_uq UNIQUE (area, nombre),
	CONSTRAINT bancomex_archivo_fk1 FOREIGN KEY (id_contribuyente)
        REFERENCES contribuyente.contribuyente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.bancomex_archivo
    OWNER TO megadb01_userprod;

DROP INDEX IF EXISTS archivo.bancomex_archivo_idx;
CREATE INDEX bancomex_archivo_idx
    ON archivo.bancomex_archivo USING btree
    (id_contribuyente)
    TABLESPACE pg_default;