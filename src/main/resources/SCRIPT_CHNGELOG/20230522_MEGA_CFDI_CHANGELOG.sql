ALTER TABLE facturacion.nomina ADD emisor_rfc varchar(20) NULL;

UPDATE facturacion.nomina
SET emisor_rfc = contribuyente.rfc_activo
FROM contribuyente.contribuyente contribuyente
INNER JOIN facturacion.nomina c ON contribuyente.id = c.id_emisor
WHERE contribuyente.id = c.id_emisor;