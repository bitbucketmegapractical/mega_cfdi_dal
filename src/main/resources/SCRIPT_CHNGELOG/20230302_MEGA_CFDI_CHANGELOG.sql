ALTER TABLE nomencladores.retencion_tipo_pago ALTER COLUMN id SET DEFAULT nextval('nomencladores.retencion_tipo_pago_id_seq'::regclass);
DROP INDEX IF EXISTS archivo.bancomex_archivo_idx;
DROP TABLE IF EXISTS archivo.bancomex_archivo;
DROP sequence IF EXISTS archivo.bancomex_archivo_id_seq;

CREATE SCHEMA clientes;

-- Table: clientes.bancomex_archivo
DROP sequence if EXISTS clientes.bancomex_archivo_id_seq CASCADE;
CREATE SEQUENCE clientes.bancomex_archivo_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE clientes.bancomex_archivo_id_seq
    OWNER TO megadb01_userprod;

DROP TABLE IF EXISTS clientes.bancomex_archivo;
CREATE TABLE clientes.bancomex_archivo (
	id bigint NOT NULL DEFAULT nextval('clientes.bancomex_archivo_id_seq'::regclass),
	id_contribuyente bigint NOT NULL,
	area varchar NULL,
	origen varchar NOT NULL,
	status varchar NOT NULL,
	nombre varchar NULL,
	fecha_carga date NULL,
	ruta varchar NULL,	
	ruta_compactado varchar NULL,
	ruta_error varchar NULL,
	eliminado bool NULL DEFAULT false,	
	ejercicio varchar NULL,
	CONSTRAINT bancomex_archivo_pk PRIMARY KEY (id),
	CONSTRAINT bancomex_archivo_uq UNIQUE (area, nombre),
	CONSTRAINT bancomex_archivo_fk1 FOREIGN KEY (id_contribuyente)
        REFERENCES contribuyente.contribuyente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
) WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE clientes.bancomex_archivo
    OWNER to megadb01_userprod;

DROP INDEX IF EXISTS clientes.bancomex_archivo_idx;
CREATE INDEX bancomex_archivo_idx
    ON clientes.bancomex_archivo USING btree
    (id_contribuyente)
    TABLESPACE pg_default;
	
-- Table: clientes.bancomex_area
DROP sequence if EXISTS clientes.bancomex_area_id_seq CASCADE;
CREATE SEQUENCE clientes.bancomex_area_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE clientes.bancomex_area_id_seq
    OWNER TO megadb01_userprod;

DROP TABLE IF EXISTS clientes.bancomex_area;
CREATE TABLE clientes.bancomex_area (
	id bigint NOT NULL DEFAULT nextval('clientes.bancomex_area_id_seq'::regclass),
	area varchar NOT NULL,
	serie varchar NOT null,
	eliminado bool NULL DEFAULT false,	
	CONSTRAINT bancomex_area_pk PRIMARY KEY (id),
	CONSTRAINT bancomex_area_uq UNIQUE (area, serie)
) WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE clientes.bancomex_area
    OWNER to megadb01_userprod;

DROP INDEX IF EXISTS clientes.bancomex_area_idx;
CREATE INDEX bancomex_area_idx
    ON clientes.bancomex_area USING btree
    (serie)
    TABLESPACE pg_default;

INSERT INTO clientes.bancomex_area (id, area, serie, eliminado) VALUES
	(1, 'CREDITO', 'BK', false),
	(2, 'CREDITO', 'COM', false),
	(3, 'CREDITO', 'FAC', false),
	(4, 'CREDITO', 'IMX', false),
	(5, 'CREDITO', 'REPF', false),
	(6, 'FACTURAS_MANUALES', 'NC', false),
	(7, 'FACTURAS_MANUALES', 'PM', false),
	(8, 'FACTURAS_MANUALES', 'RP', false),
	(9, 'FIDUCIARIO', 'DF', false),
	(10, 'MERCADOS', 'EDC', false),
	(11, 'TESORERIA', 'BNC', false),
	(12, 'TESORERIA', 'SDP', false),
	(13, 'TESORERIA', 'SIT', false),
	(14, 'TESORERIA', 'STN', false),	
	(15, 'RH', 'HIP', false),
	(16, 'RH', 'NOMINA', false),
	(17, 'RH', 'PEA', false);