ALTER TABLE clientes.bancomext_notification_error RENAME TO bancomext_notification;
ALTER TABLE clientes.bancomext_notification RENAME COLUMN recipients TO recipient;
ALTER TABLE clientes.bancomext_notification ADD subject varchar NULL;
ALTER TABLE clientes.bancomext_notification ADD file_name varchar NULL;