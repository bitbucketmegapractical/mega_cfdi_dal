INSERT INTO nomencladores.tipo_operacion(id, codigo, valor, imagen, eliminado)
	VALUES (1001000000000000082, 'ENVIO_CORREO_ELECTRONICO', 'El usuario ha enviado por correo electrónico el XML y PDF del CFDI con UUID {1}', '<i class="fa fa-envelope" aria-hidden="true"></i>', false);
SELECT setval('nomencladores.tipo_operacion_id_seq', 1001000000000000082, true);	