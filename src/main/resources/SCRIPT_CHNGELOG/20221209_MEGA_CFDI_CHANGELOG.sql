UPDATE configuracion.complemento SET fecha_ultima_actualizacion_sat='2021-12-31',"version"='2.0' WHERE id=1001000000000000001;
UPDATE nomencladores.uso_cfdi SET codigo='S01',valor='Sin efectos fiscales' WHERE id=1001000000000000022;
INSERT INTO nomencladores.uso_cfdi (codigo,valor,aplica_persona_fisica,aplica_persona_moral,eliminado) VALUES
	('CP01','Pagos',true,true,false),
	('CN01','Nómina',true,false,false);

CREATE SEQUENCE nomencladores.objeto_impuesto_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1001000000000000001
	CACHE 1
	NO CYCLE;
	
CREATE TABLE nomencladores.objeto_impuesto
(
    id bigint NOT NULL DEFAULT nextval('nomencladores.objeto_impuesto_id_seq'::regclass),    
	codigo character varying(20) COLLATE pg_catalog."default",
	valor character varying(100) COLLATE pg_catalog."default",
	eliminado bool NULL DEFAULT false,
    CONSTRAINT objeto_impuesto_pk PRIMARY KEY (id),
    CONSTRAINT objeto_impuesto_uq UNIQUE (codigo)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nomencladores.objeto_impuesto
    OWNER to megadb01_userprod;
	
INSERT INTO nomencladores.objeto_impuesto (codigo, valor, eliminado) VALUES
	('01', 'No objeto de impuesto', false),
	('02', 'Sí objeto de impuesto', false),
	('03', 'Sí objeto del impuesto y no obligado al desglose', false);
	
ALTER TABLE complementos.complemento_pago_documento_relacionado ALTER COLUMN id_metodo_pago DROP NOT NULL;
ALTER TABLE complementos.complemento_pago_documento_relacionado ADD objeto_impuesto varchar(10) NULL;

CREATE SEQUENCE nomencladores.exportacion_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1001000000000000001
	CACHE 1
	NO CYCLE;
	
CREATE TABLE nomencladores.exportacion
(
    id bigint NOT NULL DEFAULT nextval('nomencladores.exportacion_id_seq'::regclass),    
	codigo character varying(20) COLLATE pg_catalog."default",
	valor character varying(100) COLLATE pg_catalog."default",
	eliminado bool NULL DEFAULT false,
    CONSTRAINT exportacion_pk PRIMARY KEY (id),
    CONSTRAINT exportacion_uq UNIQUE (codigo)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nomencladores.exportacion
    OWNER to megadb01_userprod;
	
INSERT INTO nomencladores.exportacion (codigo, valor, eliminado) VALUES
	('01', 'No aplica', false),
	('02', 'Definitiva con clave A1', false),
	('03', 'Temporal', false),
	('04', 'Definitiva con clave distinta a A1 o cuando no existe enajenación en términos del CFF', false);
	
ALTER TABLE facturacion.comprobante ADD id_exportacion bigint NULL;
CREATE INDEX comprobante_exportacion_idx ON facturacion.comprobante (id_exportacion);
ALTER TABLE facturacion.comprobante ADD CONSTRAINT comprobante_exportacion FOREIGN KEY (id_exportacion) REFERENCES nomencladores.exportacion(id);

ALTER TABLE facturacion.prefactura ADD id_exportacion bigint NULL;
CREATE INDEX prefactura_exportacion_idx ON facturacion.prefactura (id_exportacion);
ALTER TABLE facturacion.prefactura ADD CONSTRAINT prefactura_exportacion FOREIGN KEY (id_exportacion) REFERENCES nomencladores.exportacion(id);

ALTER TABLE contribuyente.cliente ADD id_domicilio_fiscal bigint NULL;
CREATE INDEX domicilio_fiscal_idx ON contribuyente.cliente (id_domicilio_fiscal);
ALTER TABLE contribuyente.cliente ADD CONSTRAINT domicilio_fiscal FOREIGN KEY (id_domicilio_fiscal) REFERENCES nomencladores.codigo_postal(id);

ALTER TABLE contribuyente.cliente ADD id_regimen_fiscal bigint NULL;
CREATE INDEX regimen_fiscal_idx ON contribuyente.cliente (id_regimen_fiscal);
ALTER TABLE contribuyente.cliente ADD CONSTRAINT regimen_fiscal FOREIGN KEY (id_regimen_fiscal) REFERENCES nomencladores.regimen_fiscal(id);

ALTER TABLE facturacion.comprobante ALTER COLUMN qr TYPE varchar(5000) USING qr::varchar;