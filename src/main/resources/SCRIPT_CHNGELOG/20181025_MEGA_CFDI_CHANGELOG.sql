
ALTER TABLE facturacion.cfdi
    ADD COLUMN fecha_cancelado date;
ALTER TABLE facturacion.cfdi
    ADD COLUMN hora_cancelado time without time zone;