ALTER TABLE nomencladores.tipo_deduccion ALTER COLUMN valor TYPE varchar(250) USING valor::varchar;
ALTER TABLE nomencladores.tipo_percepcion ALTER COLUMN valor TYPE varchar(250) USING valor::varchar;

INSERT INTO nomencladores.tipo_deduccion (id,codigo,valor,eliminado) VALUES 
	(1001000000000000101,'101','ISR Retenido de ejercicio anterior',false),
	(1001000000000000102,'102','Ajuste a pagos por gratificaciones, primas, compensaciones, recompensas u otros a extrabajadores derivados de jubilación en parcialidades, gravados',false),
	(1001000000000000103,'103','Ajuste a pagos que se realicen a extrabajadores que obtengan una jubilación en parcialidades derivados de la ejecución de una resolución judicial o de un laudo gravados',false),
	(1001000000000000104,'104','Ajuste a pagos que se realicen a extrabajadores que obtengan una jubilación en parcialidades derivados de la ejecución de una resolución judicial o de un laudo exentos',false),
	(1001000000000000105,'105','Ajuste a pagos que se realicen a extrabajadores que obtengan una jubilación en una sola exhibición derivados de la ejecución de una resolución judicial o de un laudo gravados',false),
	(1001000000000000106,'106','Ajuste a pagos que se realicen a extrabajadores que obtengan una jubilación en una sola exhibición derivados de la ejecución de una resolución judicial o de un laudo exentos',false),
	(1001000000000000107,'107','Ajuste al Subsidio Causado',false);
	
INSERT INTO nomencladores.tipo_percepcion (id,codigo,valor,eliminado) VALUES
	 (1001000000000000043,'049','Premios por asistencia',false),
	 (1001000000000000044,'050','Viáticos',false),
	 (1001000000000000045,'051','Pagos por gratificaciones, primas, compensaciones, recompensas u otros a extrabajadores derivados de jubilación en parcialidades',false),
	 (1001000000000000046,'052','Pagos que se realicen a extrabajadores que obtengan una jubilación en parcialidades derivados de la ejecución de resoluciones judicial o de un laudo',false),
	 (1001000000000000047,'053','Pagos que se realicen a extrabajadores que obtengan una jubilación en una sola exhibición derivados de la ejecución de resoluciones judicial o de un laudo',false);
	