CREATE SEQUENCE nomencladores.nivel_educativo_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE nomencladores.nivel_educativo_id_seq
    OWNER TO postgres;
	
CREATE TABLE nomencladores.nivel_educativo
(
    id bigint NOT NULL DEFAULT nextval('nomencladores.nivel_educativo_id_seq'::regclass),
    codigo character varying(250) COLLATE pg_catalog."default",
    valor character varying(250) COLLATE pg_catalog."default",
    eliminado boolean DEFAULT false,
    CONSTRAINT nivel_educativo_pk PRIMARY KEY (id),
	CONSTRAINT nivel_educativo_uq1 UNIQUE (codigo),
	CONSTRAINT nivel_educativo_uq2 UNIQUE (valor)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nomencladores.nivel_educativo
    OWNER to postgres;
	
INSERT INTO nomencladores.nivel_educativo(codigo, valor, eliminado) VALUES 
	('PRE', 'Preescolar', false),
	('PRI', 'Primaria', false),
	('SEC', 'Secundaria', false),
	('PRO', 'Profesional técnico', false),
	('BAC', 'Bachillerato o su equivalente', false);