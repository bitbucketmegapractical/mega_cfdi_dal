INSERT INTO nomencladores.tipo_operacion(id, codigo, valor, imagen, eliminado)
	VALUES (1001000000000000081, 'DESCARGA_MASIVA_CFDI_V33', 'El usuario ha realizado una descarga masiva de {1} CFDIs', '<i class="fa fa-download" aria-hidden="true"></i>', false);
SELECT setval('nomencladores.tipo_operacion_id_seq', 1001000000000000081, true);	