CREATE SEQUENCE seguridad.rol_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;
	
CREATE TABLE seguridad.rol (
	id bigint NOT NULL DEFAULT nextval('seguridad.rol_id_seq'::regclass),
	codigo varchar(20) NULL,
	valor varchar(20) NULL,
	activo bool NULL DEFAULT true,
	CONSTRAINT rol_pk PRIMARY KEY (id)
);

INSERT INTO seguridad.rol (id,codigo,valor,activo) VALUES
	 (-1,'ROLE_ROOT','ROOT',true),
	 (1001000000000000001,'ROLE_USR','USUARIO',true),
	 (1001000000000000002,'ROLE_MST','MASTER',true),
	 (1001000000000000003,'ROLE_ADM','ADMINISTRADOR',true);

ALTER TABLE seguridad.perfil DROP CONSTRAINT rol_perfil;
ALTER TABLE seguridad.perfil ADD CONSTRAINT rol_perfil FOREIGN KEY (id_rol) REFERENCES seguridad.rol(id);

DROP TABLE nomencladores.rol;
DROP SEQUENCE nomencladores.rol_id_seq;

ALTER TABLE seguridad.usuario ADD id_rol bigint NULL;
ALTER TABLE seguridad.usuario ADD CONSTRAINT usuario_fk FOREIGN KEY (id_rol) REFERENCES seguridad.rol(id);
CREATE INDEX usuario_idx ON seguridad.usuario (id_rol);
ALTER TABLE seguridad.usuario ADD activo bool NULL DEFAULT true;
ALTER TABLE seguridad.usuario ADD api_key varchar(250) NULL DEFAULT NULL;
ALTER TABLE seguridad.usuario ADD servicio_descarga bool NULL DEFAULT false;

UPDATE seguridad.usuario
SET activo = true, servicio_descarga = false
WHERE hash IS NOT NULL;

UPDATE seguridad.usuario
SET clave = users.password
FROM public.users users
INNER JOIN seguridad.usuario su ON users.username = su.correo_electronico
WHERE seguridad.usuario.correo_electronico = users.username;

UPDATE seguridad.usuario
SET id_rol = rol.id
FROM seguridad.rol rol
INNER JOIN public.authorities auth ON rol.codigo = auth.authority
WHERE seguridad.usuario.correo_electronico = auth.username;

ALTER TABLE seguridad.usuario DROP CONSTRAINT users_usuario;
ALTER TABLE seguridad.usuario DROP COLUMN user_id;
DROP TABLE public.authorities;
DROP TABLE public.users