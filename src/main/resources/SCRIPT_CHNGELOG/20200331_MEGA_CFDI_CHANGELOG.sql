CREATE SEQUENCE nomencladores.retencion_complemento_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE nomencladores.retencion_complemento_id_seq
    OWNER TO postgres;
	
CREATE TABLE nomencladores.retencion_complemento
(
    id bigint NOT NULL DEFAULT nextval('nomencladores.retencion_complemento_id_seq'::regclass),
    codigo character varying(250) COLLATE pg_catalog."default",
    valor character varying(250) COLLATE pg_catalog."default",
    eliminado boolean DEFAULT false,
    CONSTRAINT retencion_complemento_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nomencladores.retencion_complemento
    OWNER to postgres;

ALTER TABLE nomencladores.retencion_complemento
    ADD CONSTRAINT retencion_complemento_uq UNIQUE (codigo);
	
INSERT INTO nomencladores.retencion_complemento(codigo, valor, eliminado) VALUES 
	('01', 'Servicios profesionales', false),
	('02', 'Regalías por derechos de autor', false),
	('03', 'Autotransporte terrestre de carga', false),
	('04', 'Servicios prestados por comisionistas', false),
	('05', 'Arrendamiento', false),
	('06', 'Enajenación de acciones', false),
	('07', 'Enajenación de bienes objeto de la LIEPS, a través de mediadores, agentes, representantes, corredores, consignatarios o distribuidores', false),
	('08', 'Enajenación de bienes inmuebles consignada en escritura pública', false),
	('09', 'Enajenación de otros bienes, no consignada en escritura pública', false),
	('10', 'Adquisición de desperdicios industriales', false),
	('11', 'Adquisición de bienes consignada en escritura pública', false),
	('12', 'Adquisición de otros bienes, no consignada en escritura pública', false),
	('13', 'Otros retiros de AFORE', false),
	('14', 'Dividendos o utilidades distribuidas', false),
	('15', 'Remanente distribuible', false),
	('16', 'Intereses', false),
	('17', 'Arrendamiento en fideicomiso', false),
	('18', 'Pagos realizados a favor de residentes en el extranjero', false),
	('19', 'Enajenación de acciones u operaciones en bolsa de valores', false),
	('20', 'Obtención de premios', false),
	('21', 'Fideicomisos que no realizan actividades empresariales', false),
	('22', 'Planes personales de retiro', false),
	('23', 'Intereses reales deducibles por créditos hipotecarios', false),
	('24', 'Operaciones Financieras Derivadas de Capital', false),
	('25', 'Otro tipo de retenciones', false);
	
CREATE SEQUENCE nomencladores.retencion_tipo_dividendo_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE nomencladores.retencion_tipo_dividendo_id_seq
    OWNER TO postgres;
	
CREATE TABLE nomencladores.retencion_tipo_dividendo
(
    id bigint NOT NULL DEFAULT nextval('nomencladores.retencion_tipo_dividendo_id_seq'::regclass),
    codigo character varying(250) COLLATE pg_catalog."default",
    valor character varying(250) COLLATE pg_catalog."default",
    eliminado boolean DEFAULT false,
    CONSTRAINT retencion_tipo_dividendo_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nomencladores.retencion_tipo_dividendo
    OWNER to postgres;
	
INSERT INTO nomencladores.retencion_tipo_dividendo(codigo, valor, eliminado) VALUES 
	('01', 'Proviene de CUFIN', false),
	('02', 'No proviene de CUFIN', false),
	('03', 'Reembolso o reducción de capital', false),
	('04', 'Liquidación de la persona moral', false),
	('05', 'CUFINRE', false),
	('06', 'Proviene de CUFIN al 31 de diciembre 2013', false);
	
ALTER TABLE nomencladores.retencion_tipo_dividendo
    ADD CONSTRAINT retencion_tipo_dividendo_uq UNIQUE (codigo);
	
CREATE SEQUENCE nomencladores.retencion_pais_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE nomencladores.retencion_pais_id_seq
    OWNER TO postgres;
	
CREATE TABLE nomencladores.retencion_pais
(
    id bigint NOT NULL DEFAULT nextval('nomencladores.retencion_pais_id_seq'::regclass),
    codigo character varying(250) COLLATE pg_catalog."default",
    valor character varying(250) COLLATE pg_catalog."default",
    eliminado boolean DEFAULT false,
    CONSTRAINT retencion_pais_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nomencladores.retencion_pais
    OWNER to postgres;

ALTER TABLE nomencladores.retencion_pais
    ADD CONSTRAINT retencion_pais_uq UNIQUE (codigo);

INSERT INTO nomencladores.retencion_pais(id, codigo, valor, eliminado) VALUES
	(1001000000000000001, 'AF', 'AFGANISTAN (EMIRATO ISLAMICO DE)', false),
	(1001000000000000002, 'AL', 'ALBANIA (REPUBLICA DE)', false),
	(1001000000000000003, 'DE', 'ALEMANIA (REPUBLICA FEDERAL DE)', false),
	(1001000000000000004, 'AD', 'ANDORRA (PRINCIPADO DE)', false),
	(1001000000000000005, 'AO', 'ANGOLA (REPUBLICA DE)', false),
	(1001000000000000006, 'AI', 'ANGUILA', false),
	(1001000000000000007, 'AQ', 'ANTARTIDA', false),
	(1001000000000000008, 'AG', 'ANTIGUA Y BARBUDA (COMUNIDAD BRITANICA DE NACIONES)', false),
	(1001000000000000009, 'AN', 'ANTILLAS NEERLANDESAS (TERRITORIO HOLANDES DE ULTRAMAR)', false),
	(1001000000000000010, 'SA', 'ARABIA SAUDITA (REINO DE)', false),
	(1001000000000000011, 'DZ', 'ARGELIA (REPUBLICA DEMOCRATICA Y POPULAR DE)', false),
	(1001000000000000012, 'AR', 'ARGENTINA (REPUBLICA)', false),
	(1001000000000000013, 'AM', 'ARMENIA (REPUBLICA DE)', false),
	(1001000000000000014, 'AW', 'ARUBA (TERRITORIO HOLANDES DE ULTRAMAR)', false),
	(1001000000000000015, 'AU', 'AUSTRALIA (COMUNIDAD DE)', false),
	(1001000000000000016, 'AT', 'AUSTRIA (REPUBLICA DE)', false),
	(1001000000000000017, 'AZ', 'AZERBAIJAN (REPUBLICA AZERBAIJANI)', false),
	(1001000000000000018, 'BS', 'BAHAMAS (COMUNIDAD DE LAS)', false),
	(1001000000000000019, 'BH', 'BAHREIN (ESTADO DE)', false),
	(1001000000000000020, 'BD', 'BANGLADESH (REPUBLICA POPULAR DE)', false),
	(1001000000000000021, 'BB', 'BARBADOS (COMUNIDAD BRITANICA DE NACIONES)', false),
	(1001000000000000022, 'BE', 'BELGICA (REINO DE)', false),
	(1001000000000000023, 'BZ', 'BELICE', false),
	(1001000000000000024, 'BJ', 'BENIN (REPUBLICA DE)', false),
	(1001000000000000025, 'BM', 'BERMUDAS', false),
	(1001000000000000026, 'BY', 'BIELORRUSIA (REPUBLICA DE)', false),
	(1001000000000000027, 'BO', 'BOLIVIA (REPUBLICA DE)', false),
	(1001000000000000028, 'BA', 'BOSNIA Y HERZEGOVINA', false),
	(1001000000000000029, 'BW', 'BOTSWANA (REPUBLICA DE)', false),
	(1001000000000000030, 'BR', 'BRASIL (REPUBLICA FEDERATIVA DE)', false),
	(1001000000000000031, 'BN', 'BRUNEI (ESTADO DE) (RESIDENCIA DE PAZ)', false),
	(1001000000000000032, 'BG', 'BULGARIA (REPUBLICA DE)', false),
	(1001000000000000033, 'BF', 'BURKINA FASO', false),
	(1001000000000000034, 'BI', 'BURUNDI (REPUBLICA DE)', false),
	(1001000000000000035, 'BT', 'BUTAN (REINO DE)', false),
	(1001000000000000036, 'CV', 'CABO VERDE (REPUBLICA DE)', false),
	(1001000000000000037, 'TD', 'CHAD (REPUBLICA DEL)', false),
	(1001000000000000038, 'KY', 'CAIMAN (ISLAS)', false),
	(1001000000000000039, 'KH', 'CAMBOYA (REINO DE)', false),
	(1001000000000000040, 'CM', 'CAMERUN (REPUBLICA DEL)', false),
	(1001000000000000041, 'CA', 'CANADA', false),
	(1001000000000000042, 'CL', 'CHILE (REPUBLICA DE)', false),
	(1001000000000000043, 'CN', 'CHINA (REPUBLICA POPULAR)', false),
	(1001000000000000044, 'CY', 'CHIPRE (REPUBLICA DE)', false),
	(1001000000000000045, 'VA', 'CIUDAD DEL VATICANO (ESTADO DE LA)', false),
	(1001000000000000046, 'CC', 'COCOS (KEELING,  ISLAS AUSTRALIANAS)', false),
	(1001000000000000047, 'CO', 'COLOMBIA (REPUBLICA DE)', false),
	(1001000000000000048, 'KM', 'COMORAS (ISLAS)', false),
	(1001000000000000049, 'CG', 'CONGO (REPUBLICA DEL)', false),
	(1001000000000000050, 'CK', 'COOK (ISLAS)', false),
	(1001000000000000051, 'KP', 'COREA (REPUBLICA POPULAR DEMOCRATICA DE) (COREA DEL NORTE)', false),
	(1001000000000000052, 'KR', 'COREA (REPUBLICA DE) (COREA DEL SUR)', false),
	(1001000000000000053, 'CI', 'COSTA DE MARFIL (REPUBLICA DE LA)', false),
	(1001000000000000054, 'CR', 'COSTA RICA (REPUBLICA DE)', false),
	(1001000000000000055, 'HR', 'CROACIA (REPUBLICA DE)', false),
	(1001000000000000056, 'CU', 'CUBA (REPUBLICA DE)', false),
	(1001000000000000057, 'DK', 'DINAMARCA (REINO DE)', false),
	(1001000000000000058, 'DJ', 'DJIBOUTI (REPUBLICA DE)', false),
	(1001000000000000059, 'DM', 'DOMINICA (COMUNIDAD DE)', false),
	(1001000000000000060, 'EC', 'ECUADOR (REPUBLICA DEL)', false),
	(1001000000000000061, 'EG', 'EGIPTO (REPUBLICA ARABE DE)', false),
	(1001000000000000062, 'SV', 'EL SALVADOR (REPUBLICA DE)', false),
	(1001000000000000063, 'AE', 'EMIRATOS ARABES UNIDOS', false),
	(1001000000000000064, 'ER', 'ERITREA (ESTADO DE)', false),
	(1001000000000000065, 'SI', 'ESLOVENIA (REPUBLICA DE)', false),
	(1001000000000000066, 'ES', 'ESPAÃ‘A (REINO DE)', false),
	(1001000000000000067, 'FM', 'ESTADO FEDERADO DE MICRONESIA', false),
	(1001000000000000068, 'US', 'ESTADOS UNIDOS DE AMERICA', false),
	(1001000000000000069, 'EE', 'ESTONIA (REPUBLICA DE)', false),
	(1001000000000000070, 'ET', 'ETIOPIA (REPUBLICA DEMOCRATICA FEDERAL)', false),
	(1001000000000000071, 'FJ', 'FIDJI (REPUBLICA DE)', false),
	(1001000000000000072, 'PH', 'FILIPINAS (REPUBLICA DE LAS)', false),
	(1001000000000000073, 'FI', 'FINLANDIA (REPUBLICA DE)', false),
	(1001000000000000074, 'FR', 'FRANCIA (REPUBLICA FRANCESA)', false),
	(1001000000000000075, 'GA', 'GABONESA (REPUBLICA)', false),
	(1001000000000000076, 'GM', 'GAMBIA (REPUBLICA DE LA)', false),
	(1001000000000000077, 'GE', 'GEORGIA (REPUBLICA DE)', false),
	(1001000000000000078, 'GH', 'GHANA (REPUBLICA DE)', false),
	(1001000000000000079, 'GI', 'GIBRALTAR (R.U.)', false),
	(1001000000000000080, 'GD', 'GRANADA', false),
	(1001000000000000081, 'GR', 'GRECIA (REPUBLICA HELENICA)', false),
	(1001000000000000082, 'GL', 'GROENLANDIA (DINAMARCA)', false),
	(1001000000000000083, 'GP', 'GUADALUPE (DEPARTAMENTO DE)', false),
	(1001000000000000084, 'GU', 'GUAM (E.U.A.)', false),
	(1001000000000000085, 'GT', 'GUATEMALA (REPUBLICA DE)', false),
	(1001000000000000086, 'GG', 'GUERNSEY', false),
	(1001000000000000087, 'GW', 'GUINEA-BISSAU (REPUBLICA DE)', false),
	(1001000000000000088, 'GQ', 'GUINEA ECUATORIAL (REPUBLICA DE)', false),
	(1001000000000000089, 'GN', 'GUINEA (REPUBLICA DE)', false),
	(1001000000000000090, 'GF', 'GUYANA FRANCESA', false),
	(1001000000000000091, 'GY', 'GUYANA (REPUBLICA COOPERATIVA DE)', false),
	(1001000000000000092, 'HT', 'HAITI (REPUBLICA DE)', false),
	(1001000000000000093, 'HN', 'HONDURAS (REPUBLICA DE)', false),
	(1001000000000000094, 'HK', 'HONG KONG (REGION ADMINISTRATIVA ESPECIAL DE LA REPUBLICA)', false),
	(1001000000000000095, 'HU', 'HUNGRIA (REPUBLICA DE)', false),
	(1001000000000000096, 'IN', 'INDIA (REPUBLICA DE)', false),
	(1001000000000000097, 'ID', 'INDONESIA (REPUBLICA DE)', false),
	(1001000000000000098, 'IQ', 'IRAK (REPUBLICA DE)', false),
	(1001000000000000099, 'IR', 'IRAN (REPUBLICA ISLAMICA DEL)', false),
	(1001000000000000100, 'IE', 'IRLANDA (REPUBLICA DE)', false),
	(1001000000000000101, 'IS', 'ISLANDIA (REPUBLICA DE)', false),
	(1001000000000000102, 'BV', 'ISLA BOUVET', false),
	(1001000000000000103, 'IM', 'ISLA DE MAN', false),
	(1001000000000000104, 'AX', 'ISLAS ALAND', false),
	(1001000000000000105, 'FO', 'ISLAS FEROE', false),
	(1001000000000000106, 'GS', 'ISLAS GEORGIA Y SANDWICH DEL SUR', false),
	(1001000000000000107, 'HM', 'ISLAS HEARD Y MCDONALD', false),
	(1001000000000000108, 'FK', 'ISLAS MALVINAS (R.U.)', false),
	(1001000000000000109, 'MP', 'ISLAS MARIANAS SEPTENTRIONALES', false),
	(1001000000000000110, 'MH', 'ISLAS MARSHALL', false),
	(1001000000000000111, 'UM', 'ISLAS MENORES DE ULTRAMAR DE ESTADOS UNIDOS DE AMERICA', false),
	(1001000000000000112, 'SB', 'ISLAS SALOMON (COMUNIDAD BRITANICA DE NACIONES)', false),
	(1001000000000000113, 'SJ', 'ISLAS SVALBARD Y JAN MAYEN (NORUEGA)', false),
	(1001000000000000114, 'TK', 'ISLAS TOKELAU', false),
	(1001000000000000115, 'WF', 'ISLAS WALLIS Y FUTUNA', false),
	(1001000000000000116, 'IL', 'ISRAEL (ESTADO DE)', false),
	(1001000000000000117, 'IT', 'ITALIA (REPUBLICA ITALIANA)', false),
	(1001000000000000118, 'JM', 'JAMAICA', false),
	(1001000000000000119, 'JP', 'JAPON', false),
	(1001000000000000120, 'JE', 'JERSEY', false),
	(1001000000000000121, 'JO', 'JORDANIA (REINO HACHEMITA DE)', false),
	(1001000000000000122, 'KZ', 'KAZAKHSTAN (REPUBLICA DE)', false),
	(1001000000000000123, 'KE', 'KENYA (REPUBLICA DE)', false),
	(1001000000000000124, 'KI', 'KIRIBATI (REPUBLICA DE)', false),
	(1001000000000000125, 'KW', 'KUWAIT (ESTADO DE)', false),
	(1001000000000000126, 'KG', 'KYRGYZSTAN (REPUBLICA KIRGYZIA)', false),
	(1001000000000000127, 'LS', 'LESOTHO (REINO DE)', false),
	(1001000000000000128, 'LV', 'LETONIA (REPUBLICA DE)', false),
	(1001000000000000129, 'LB', 'LIBANO (REPUBLICA DE)', false),
	(1001000000000000130, 'LR', 'LIBERIA (REPUBLICA DE)', false),
	(1001000000000000131, 'LY', 'LIBIA (JAMAHIRIYA LIBIA ARABE POPULAR SOCIALISTA)', false),
	(1001000000000000132, 'LI', 'LIECHTENSTEIN (PRINCIPADO DE)', false),
	(1001000000000000133, 'LT', 'LITUANIA (REPUBLICA DE)', false),
	(1001000000000000134, 'LU', 'LUXEMBURGO (GRAN DUCADO DE)', false),
	(1001000000000000135, 'MO', 'MACAO', false),
	(1001000000000000136, 'MK', 'MACEDONIA (ANTIGUA REPUBLICA YUGOSLAVA DE)', false),
	(1001000000000000137, 'MG', 'MADAGASCAR (REPUBLICA DE)', false),
	(1001000000000000138, 'MY', 'MALASIA', false),
	(1001000000000000139, 'MW', 'MALAWI (REPUBLICA DE)', false),
	(1001000000000000140, 'MV', 'MALDIVAS (REPUBLICA DE)', false),
	(1001000000000000141, 'ML', 'MALI (REPUBLICA DE)', false),
	(1001000000000000142, 'MT', 'MALTA (REPUBLICA DE)', false),
	(1001000000000000143, 'MA', 'MARRUECOS (REINO DE)', false),
	(1001000000000000144, 'MQ', 'MARTINICA (DEPARTAMENTO DE) (FRANCIA)', false),
	(1001000000000000145, 'MU', 'MAURICIO (REPUBLICA DE)', false),
	(1001000000000000146, 'MR', 'MAURITANIA (REPUBLICA ISLAMICA DE)', false),
	(1001000000000000147, 'YT', 'MAYOTTE', false),
	(1001000000000000148, 'MX', 'MEXICO (ESTADOS UNIDOS MEXICANOS)', false),
	(1001000000000000149, 'MD', 'MOLDAVIA (REPUBLICA DE)', false),
	(1001000000000000150, 'MC', 'MONACO (PRINCIPADO DE)', false),
	(1001000000000000151, 'MN', 'MONGOLIA', false),
	(1001000000000000152, 'MS', 'MONSERRAT (ISLA)', false),
	(1001000000000000153, 'ME', 'MONTENEGRO', false),
	(1001000000000000154, 'MZ', 'MOZAMBIQUE (REPUBLICA DE)', false),
	(1001000000000000155, 'MM', 'MYANMAR (UNION DE)', false),
	(1001000000000000156, 'NA', 'NAMIBIA (REPUBLICA DE)', false),
	(1001000000000000157, 'NR', 'NAURU', false),
	(1001000000000000158, 'CX', 'NAVIDAD (CHRISTMAS) (ISLAS)', false),
	(1001000000000000159, 'NP', 'NEPAL (REINO DE)', false),
	(1001000000000000160, 'NI', 'NICARAGUA (REPUBLICA DE)', false),
	(1001000000000000161, 'NE', 'NIGER (REPUBLICA DE)', false),
	(1001000000000000162, 'NG', 'NIGERIA (REPUBLICA FEDERAL DE)', false),
	(1001000000000000163, 'NU', 'NIVE (ISLA)', false),
	(1001000000000000164, 'NF', 'NORFOLK (ISLA)', false),
	(1001000000000000165, 'NO', 'NORUEGA (REINO DE)', false),
	(1001000000000000166, 'NC', 'NUEVA CALEDONIA (TERRITORIO FRANCES DE ULTRAMAR)', false),
	(1001000000000000167, 'NZ', 'NUEVA ZELANDIA', false),
	(1001000000000000168, 'OM', 'OMAN (SULTANATO DE)', false),
	(1001000000000000169, 'PIK', 'PACIFICO,  ISLAS DEL (ADMON. E.U.A.)', false),
	(1001000000000000170, 'NL', 'PAISES BAJOS (REINO DE LOS) (HOLANDA)', false),
	(1001000000000000171, 'PK', 'PAKISTAN (REPUBLICA ISLAMICA DE)', false),
	(1001000000000000172, 'PW', 'PALAU (REPUBLICA DE)', false),
	(1001000000000000173, 'PS', 'PALESTINA', false),
	(1001000000000000174, 'PA', 'PANAMA (REPUBLICA DE)', false),
	(1001000000000000175, 'PG', 'PAPUA NUEVA GUINEA (ESTADO INDEPENDIENTE DE)', false),
	(1001000000000000176, 'PY', 'PARAGUAY (REPUBLICA DEL)', false),
	(1001000000000000177, 'PE', 'PERU (REPUBLICA DEL)', false),
	(1001000000000000178, 'PN', 'PITCAIRNS (ISLAS DEPENDENCIA BRITANICA)', false),
	(1001000000000000179, 'PF', 'POLINESIA FRANCESA', false),
	(1001000000000000180, 'PL', 'POLONIA (REPUBLICA DE)', false),
	(1001000000000000181, 'PT', 'PORTUGAL (REPUBLICA PORTUGUESA)', false),
	(1001000000000000182, 'PR', 'PUERTO RICO (ESTADO LIBRE ASOCIADO DE LA COMUNIDAD DE)', false),
	(1001000000000000183, 'QA', 'QATAR (ESTADO DE)', false),
	(1001000000000000184, 'GB', 'REINO UNIDO DE LA GRAN BRETAÑA E IRLANDA DEL NORTE', false),
	(1001000000000000185, 'CZ', 'REPUBLICA CHECA', false),
	(1001000000000000186, 'CF', 'REPUBLICA CENTROAFRICANA', false),
	(1001000000000000187, 'LA', 'REPUBLICA DEMOCRATICA POPULAR LAOS', false),
	(1001000000000000188, 'RS', 'REPUBLICA DE SERBIA', false),
	(1001000000000000189, 'DO', 'REPUBLICA DOMINICANA', false),
	(1001000000000000190, 'SK', 'REPUBLICA ESLOVACA', false),
	(1001000000000000191, 'CD', 'REPUBLICA POPULAR DEL CONGO', false),
	(1001000000000000192, 'RW', 'REPUBLICA RUANDESA', false),
	(1001000000000000193, 'RE', 'REUNION (DEPARTAMENTO DE LA) (FRANCIA)', false),
	(1001000000000000194, 'RO', 'RUMANIA', false),
	(1001000000000000195, 'RU', 'RUSIA (FEDERACION RUSA)', false),
	(1001000000000000196, 'EH', 'SAHARA OCCIDENTAL (REPUBLICA ARABE SAHARAVI DEMOCRATICA)', false),
	(1001000000000000197, 'WS', 'SAMOA (ESTADO INDEPENDIENTE DE)', false),
	(1001000000000000198, 'AS', 'SAMOA AMERICANA', false),
	(1001000000000000199, 'BL', 'SAN BARTOLOME', false),
	(1001000000000000200, 'KN', 'SAN CRISTOBAL Y NIEVES (FEDERACION DE) (SAN KITTS-NEVIS)', false),
	(1001000000000000201, 'SM', 'SAN MARINO (SERENISIMA REPUBLICA DE)', false),
	(1001000000000000202, 'MF', 'SAN MARTIN', false),
	(1001000000000000203, 'PM', 'SAN PEDRO Y MIQUELON', false),
	(1001000000000000204, 'VC', 'SAN VICENTE Y LAS GRANADINAS', false),
	(1001000000000000205, 'SH', 'SANTA ELENA', false),
	(1001000000000000206, 'LC', 'SANTA LUCIA', false),
	(1001000000000000207, 'ST', 'SANTO TOME Y PRINCIPE (REPUBLICA DEMOCRATICA DE)', false),
	(1001000000000000208, 'SN', 'SENEGAL (REPUBLICA DEL)', false),
	(1001000000000000209, 'SC', 'SEYCHELLES (REPUBLICA DE LAS)', false),
	(1001000000000000210, 'SL', 'SIERRA LEONA (REPUBLICA DE)', false),
	(1001000000000000211, 'SG', 'SINGAPUR (REPUBLICA DE)', false),
	(1001000000000000212, 'SY', 'SIRIA (REPUBLICA ARABE)', false),
	(1001000000000000213, 'SO', 'SOMALIA', false),
	(1001000000000000214, 'LK', 'SRI LANKA (REPUBLICA DEMOCRATICA SOCIALISTA DE)', false),
	(1001000000000000215, 'ZA', 'SUDAFRICA (REPUBLICA DE)', false),
	(1001000000000000216, 'SD', 'SUDAN (REPUBLICA DEL)', false),
	(1001000000000000217, 'SE', 'SUECIA (REINO DE)', false),
	(1001000000000000218, 'CH', 'SUIZA (CONFEDERACION)', false),
	(1001000000000000219, 'SR', 'SURINAME (REPUBLICA DE)', false),
	(1001000000000000220, 'SZ', 'SWAZILANDIA (REINO DE)', false),
	(1001000000000000221, 'TJ', 'TADJIKISTAN (REPUBLICA DE)', false),
	(1001000000000000222, 'TH', 'TAILANDIA (REINO DE)', false),
	(1001000000000000223, 'TW', 'TAIWAN (REPUBLICA DE CHINA)', false),
	(1001000000000000224, 'TZ', 'TANZANIA (REPUBLICA UNIDA DE)', false),
	(1001000000000000225, 'IO', 'TERRITORIOS BRITANICOS DEL OCEANO INDICO', false),
	(1001000000000000226, 'TF', 'TERRITORIOS FRANCESES, AUSTRALES Y ANTARTICOS', false),
	(1001000000000000227, 'TL', 'TIMOR ORIENTAL', false),
	(1001000000000000228, 'TG', 'TOGO (REPUBLICA TOGOLESA)', false),
	(1001000000000000229, 'TO', 'TONGA (REINO DE)', false),
	(1001000000000000230, 'TT', 'TRINIDAD Y TOBAGO (REPUBLICA DE)', false),
	(1001000000000000231, 'TN', 'TUNEZ (REPUBLICA DE)', false),
	(1001000000000000232, 'TC', 'TURCAS Y CAICOS (ISLAS)', false),
	(1001000000000000233, 'TM', 'TURKMENISTAN (REPUBLICA DE)', false),
	(1001000000000000234, 'TR', 'TURQUIA (REPUBLICA DE)', false),
	(1001000000000000235, 'TV', 'TUVALU (COMUNIDAD BRITANICA DE NACIONES)', false),
	(1001000000000000236, 'UA', 'UCRANIA', false),
	(1001000000000000237, 'UG', 'UGANDA (REPUBLICA DE)', false),
	(1001000000000000238, 'UY', 'URUGUAY (REPUBLICA ORIENTAL DEL)', false),
	(1001000000000000239, 'UZ', 'UZBEJISTAN (REPUBLICA DE)', false),
	(1001000000000000240, 'VU', 'VANUATU', false),
	(1001000000000000241, 'VE', 'VENEZUELA (REPUBLICA DE)', false),
	(1001000000000000242, 'VN', 'VIETNAM (REPUBLICA SOCIALISTA DE)', false),
	(1001000000000000243, 'VG', 'VIRGENES. ISLAS (BRITANICAS)', false),
	(1001000000000000244, 'VI', 'VIRGENES. ISLAS (NORTEAMERICANAS)', false),
	(1001000000000000245, 'YE', 'YEMEN (REPUBLICA DE)', false),
	(1001000000000000246, 'ZM', 'ZAMBIA (REPUBLICA DE)', false),
	(1001000000000000247, 'ZW', 'ZIMBABWE (REPUBLICA DE)', false);

CREATE SEQUENCE facturacion.retencion_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE facturacion.retencion_id_seq
    OWNER TO postgres;
	
CREATE TABLE facturacion.retencion
(
    id bigint NOT NULL DEFAULT nextval('facturacion.retencion_id_seq'::regclass),
    id_emisor bigint NOT NULL,
    receptor_razon_social character varying(250) COLLATE pg_catalog."default", 
	receptor_rfc character varying(250) COLLATE pg_catalog."default",       
    receptor_curp character varying(250) COLLATE pg_catalog."default",
	receptor_numero_identificacion character varying(250) COLLATE pg_catalog."default",
	receptor_correo_electronico character varying(250) COLLATE pg_catalog."default",
    uuid character varying(100) COLLATE pg_catalog."default" NOT NULL,
	fecha_expedicion date,	
	fecha_hora_expedicion character varying(50) COLLATE pg_catalog."default",
    fecha_hora_timbrado character varying(50) COLLATE pg_catalog."default",
	clave_retencion character varying(20) COLLATE pg_catalog."default",
	complemento character varying(150) COLLATE pg_catalog."default",
	periodo_mes_inicial int,
	periodo_mes_final int,
	periodo_ejercicio int,		    
    folio character varying(20) COLLATE pg_catalog."default",
    ruta_xml character varying(250) COLLATE pg_catalog."default",
    ruta_pdf character varying(250) COLLATE pg_catalog."default",
    cancelado boolean DEFAULT false,
    acuse_cancelacion text COLLATE pg_catalog."default",
    fecha_cancelado date,
    hora_cancelado time without time zone,
    CONSTRAINT retencion_pk PRIMARY KEY (id),
    CONSTRAINT retencion_fk1 FOREIGN KEY (id_emisor)
        REFERENCES contribuyente.contribuyente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE facturacion.retencion
    OWNER to postgres;

CREATE INDEX retencion_idx1
    ON facturacion.retencion USING btree
    (id_emisor ASC NULLS LAST)
    TABLESPACE pg_default;	

INSERT INTO nomencladores.tipo_operacion(id, codigo, valor, imagen, eliminado) VALUES	
	(1001000000000000088, 'GENERACION_RETENCION_CLAVE_01', 'El usuario ha generado una Retención con la clave de retención 01', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000089, 'GENERACION_RETENCION_CLAVE_02', 'El usuario ha generado una Retención con la clave de retención 02', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),	
	(1001000000000000090, 'GENERACION_RETENCION_CLAVE_03', 'El usuario ha generado una Retención con la clave de retención 03', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000091, 'GENERACION_RETENCION_CLAVE_04', 'El usuario ha generado una Retención con la clave de retención 04', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000092, 'GENERACION_RETENCION_CLAVE_05', 'El usuario ha generado una Retención con la clave de retención 05', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000093, 'GENERACION_RETENCION_CLAVE_06', 'El usuario ha generado una Retención con la clave de retención 06', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000094, 'GENERACION_RETENCION_CLAVE_07', 'El usuario ha generado una Retención con la clave de retención 07', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000095, 'GENERACION_RETENCION_CLAVE_08', 'El usuario ha generado una Retención con la clave de retención 08', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000096, 'GENERACION_RETENCION_CLAVE_09', 'El usuario ha generado una Retención con la clave de retención 09', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000097, 'GENERACION_RETENCION_CLAVE_10', 'El usuario ha generado una Retención con la clave de retención 10', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000098, 'GENERACION_RETENCION_CLAVE_11', 'El usuario ha generado una Retención con la clave de retención 11', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000099, 'GENERACION_RETENCION_CLAVE_12', 'El usuario ha generado una Retención con la clave de retención 12', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000100, 'GENERACION_RETENCION_CLAVE_13', 'El usuario ha generado una Retención con la clave de retención 13', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000101, 'GENERACION_RETENCION_CLAVE_14', 'El usuario ha generado una Retención con la clave de retención 14', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000102, 'GENERACION_RETENCION_CLAVE_15', 'El usuario ha generado una Retención con la clave de retención 15', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000103, 'GENERACION_RETENCION_CLAVE_16', 'El usuario ha generado una Retención con la clave de retención 16', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000104, 'GENERACION_RETENCION_CLAVE_17', 'El usuario ha generado una Retención con la clave de retención 17', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000105, 'GENERACION_RETENCION_CLAVE_18', 'El usuario ha generado una Retención con la clave de retención 18', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000106, 'GENERACION_RETENCION_CLAVE_19', 'El usuario ha generado una Retención con la clave de retención 19', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000107, 'GENERACION_RETENCION_CLAVE_20', 'El usuario ha generado una Retención con la clave de retención 20', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000108, 'GENERACION_RETENCION_CLAVE_21', 'El usuario ha generado una Retención con la clave de retención 21', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000109, 'GENERACION_RETENCION_CLAVE_22', 'El usuario ha generado una Retención con la clave de retención 22', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000110, 'GENERACION_RETENCION_CLAVE_23', 'El usuario ha generado una Retención con la clave de retención 23', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000111, 'GENERACION_RETENCION_CLAVE_24', 'El usuario ha generado una Retención con la clave de retención 24', '<i class="fa fa-terminal" aria-hidden="true"></i>', false),
	(1001000000000000112, 'GENERACION_RETENCION_CLAVE_25', 'El usuario ha generado una Retención con la clave de retención 25', '<i class="fa fa-terminal" aria-hidden="true"></i>', false);
	SELECT setval('nomencladores.tipo_operacion_id_seq', 1001000000000000112, true);
	
CREATE SEQUENCE nomencladores.retencion_tipo_impuesto_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE nomencladores.retencion_tipo_impuesto_id_seq
    OWNER TO postgres;
	
CREATE TABLE nomencladores.retencion_tipo_impuesto
(
    id bigint NOT NULL DEFAULT nextval('nomencladores.retencion_tipo_impuesto_id_seq'::regclass),
    codigo character varying(20) COLLATE pg_catalog."default",
    valor character varying(20) COLLATE pg_catalog."default",
    eliminado boolean,
    CONSTRAINT retencion_tipo_impuesto_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nomencladores.retencion_tipo_impuesto
    OWNER to postgres;
	
INSERT INTO nomencladores.retencion_tipo_impuesto(codigo, valor, eliminado) VALUES 
	('01', 'ISR', false),
	('02', 'IVA', false),
	('03', 'IEPS', false);	
	
CREATE SEQUENCE nomencladores.retencion_tipo_contribuyente_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE nomencladores.retencion_tipo_contribuyente_id_seq
    OWNER TO postgres;
	
CREATE TABLE nomencladores.retencion_tipo_contribuyente
(
    id bigint NOT NULL DEFAULT nextval('nomencladores.retencion_tipo_contribuyente_id_seq'::regclass),
    codigo character varying(250) COLLATE pg_catalog."default",
    valor character varying(250) COLLATE pg_catalog."default",
    eliminado boolean DEFAULT false,
    CONSTRAINT retencion_tipo_contribuyente_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nomencladores.retencion_tipo_contribuyente
    OWNER to postgres;

INSERT INTO nomencladores.retencion_tipo_contribuyente (id, codigo, valor, eliminado) VALUES 
	(1001000000000000001, '1', 'Artistas, deportistas y espectáculos públicos', false),
	(1001000000000000002, '2', 'Otras personas físicas', false),
	(1001000000000000003, '3', 'Persona moral', false),
	(1001000000000000004, '4', 'Fideicomiso', false),
	(1001000000000000005, '5', 'Asociación en participación', false),
	(1001000000000000006, '6', 'Organizaciones Internacionales o de gobierno', false),
	(1001000000000000007, '7', 'Organizaciones exentas', false),
	(1001000000000000008, '8', 'Agentes pagadores', false),
	(1001000000000000009, '9', 'Otros', false);

ALTER TABLE nomencladores.retencion_tipo_contribuyente
    ADD CONSTRAINT retencion_tipo_contribuyente_uq UNIQUE (codigo);		

ALTER TABLE nomencladores.tipo_archivo
    ADD CONSTRAINT tipo_archivo_uq UNIQUE (codigo);

INSERT INTO nomencladores.tipo_archivo(id, codigo, valor, eliminado) VALUES 
	(1001000000000000003, 'R10TXT', 'Retención v1.0 (TXT)', false);

CREATE SEQUENCE archivo.archivo_retencion_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE archivo.archivo_retencion_id_seq
    OWNER TO postgres;
	
CREATE TABLE archivo.archivo_retencion
(
    id bigint NOT NULL DEFAULT nextval('archivo.archivo_retencion_id_seq'::regclass),
    id_contribuyente bigint NOT NULL,
    id_tipo_archivo bigint NOT NULL,
    id_estado_archivo bigint NOT NULL,
    nombre character varying COLLATE pg_catalog."default",
    fecha_carga date,
    hora_carga time without time zone NOT NULL,
    periodo_mes_inicial character varying COLLATE pg_catalog."default",
    periodo_mes_final character varying COLLATE pg_catalog."default",
    periodo_ejercicio character varying COLLATE pg_catalog."default",
    ruta character varying COLLATE pg_catalog."default",
    ruta_compactado character varying COLLATE pg_catalog."default",
    ruta_error character varying COLLATE pg_catalog."default",
    timbres_disponibles boolean,
    eliminado boolean,
    CONSTRAINT archivo_retencion_pk PRIMARY KEY (id),
    CONSTRAINT archivo_retencion_fk1 FOREIGN KEY (id_contribuyente)
        REFERENCES contribuyente.contribuyente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT archivo_retencion_fk2 FOREIGN KEY (id_tipo_archivo)
        REFERENCES nomencladores.tipo_archivo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT archivo_retencion_fk3 FOREIGN KEY (id_estado_archivo)
        REFERENCES nomencladores.estado_archivo (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.archivo_retencion
    OWNER to postgres;

CREATE INDEX archivo_retencion_idx1
    ON archivo.archivo_retencion USING btree
    (id_contribuyente ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX archivo_retencion_idx2
    ON archivo.archivo_retencion USING btree
    (id_tipo_archivo ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE INDEX archivo_retencion_idx3
    ON archivo.archivo_retencion USING btree
    (id_estado_archivo ASC NULLS LAST)
    TABLESPACE pg_default;	

CREATE SEQUENCE archivo.archivo_retencion_configuracion_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE archivo.archivo_retencion_configuracion_id_seq
    OWNER TO postgres;
	
CREATE TABLE archivo.archivo_retencion_configuracion
(
    id bigint NOT NULL DEFAULT nextval('archivo.archivo_retencion_configuracion_id_seq'::regclass),
    id_contribuyente bigint NOT NULL,
    envio_xml_pdf boolean,
    envio_xml boolean,
    envio_pdf boolean,
    compactado boolean,
    CONSTRAINT archivo_retencion_configuracion_pk PRIMARY KEY (id),
    CONSTRAINT archivo_retencion_configuracion_fk1 FOREIGN KEY (id_contribuyente)
        REFERENCES contribuyente.contribuyente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.archivo_retencion_configuracion
    OWNER to postgres;

CREATE INDEX archivo_retencion_configuracion_idx1
    ON archivo.archivo_retencion_configuracion USING btree
    (id_contribuyente ASC NULLS LAST)
    TABLESPACE pg_default;	

CREATE SEQUENCE archivo.retencion_fichero_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE archivo.retencion_fichero_id_seq
    OWNER TO postgres;

	
CREATE TABLE archivo.retencion_fichero
(
    id bigint NOT NULL DEFAULT nextval('archivo.retencion_fichero_id_seq'::regclass),
    codigo character varying COLLATE pg_catalog."default",
    nombre character varying COLLATE pg_catalog."default",
    cantidad_trama integer,
    eliminado boolean DEFAULT false,
    CONSTRAINT retencion_fichero_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.retencion_fichero
    OWNER to postgres;	

ALTER TABLE archivo.retencion_fichero
    ADD CONSTRAINT retencion_fichero_uq UNIQUE (codigo);

INSERT INTO archivo.retencion_fichero(codigo, nombre, cantidad_trama, eliminado) VALUES 
	('RETV10', 'Retención 1.0', 11, false);
	
CREATE SEQUENCE archivo.retencion_trama_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE archivo.retencion_trama_id_seq
    OWNER TO postgres;

CREATE TABLE archivo.retencion_trama
(
    id bigint NOT NULL DEFAULT nextval('archivo.retencion_trama_id_seq'::regclass),
    id_retencion_fichero bigint NOT NULL,
    nombre character varying(20) COLLATE pg_catalog."default",
    campos integer,
    longitud integer,
    descripcion character varying(250) COLLATE pg_catalog."default",
    principal boolean,
    requerido boolean,
    eliminado boolean,
    CONSTRAINT retencion_trama_pk PRIMARY KEY (id),
    CONSTRAINT retencion_trama_fk1 FOREIGN KEY (id_retencion_fichero)
        REFERENCES archivo.retencion_fichero (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.retencion_trama
    OWNER to postgres;

CREATE INDEX retencion_trama_idx1
    ON archivo.retencion_trama USING btree
    (id_retencion_fichero ASC NULLS LAST)
    TABLESPACE pg_default;

ALTER TABLE archivo.retencion_trama
    ADD CONSTRAINT retencion_trama_uq UNIQUE (nombre);

INSERT INTO archivo.retencion_trama VALUES 
	(1001000000000000001, 1001000000000000001, 'H0', 4, 5, 'Trama principal de las Retenciones', true, true, false),
	(1001000000000000002, 1001000000000000001, 'T1', 3, 4, 'Información del Emisor', false, true, false),
	(1001000000000000005, 1001000000000000001, 'T4', 4, 5, 'Información de Totales', false, true, false),
	(1001000000000000006, 1001000000000000001, 'T5', 9, 10, 'Información del complemento Dividendos', false, false, false),
	(1001000000000000007, 1001000000000000001, 'T6', 3, 4, 'Información del complemento Enajenación de acciones', false, false, false),
	(1001000000000000008, 1001000000000000001, 'T7', 6, 7, 'Información del complemento Intereses', false, false, false),
	(1001000000000000009, 1001000000000000001, 'T8', 2, 3, 'Información del complemento Operaciones con derivados', false, false, false),
	(1001000000000000010, 1001000000000000001, 'T9', 7, 8, 'Información del complemento Pago a extranjeros', false, false, false),
	(1001000000000000011, 1001000000000000001, 'T10', 3, 4, 'Información del complemento Sector financiero', false, false, false),
	(1001000000000000003, 1001000000000000001, 'T2', 6, 7, 'Información del Receptor', false, true, false),
	(1001000000000000004, 1001000000000000001, 'T3', 4, 5, 'Información de Impuestos retenidos', false, false, false);

CREATE SEQUENCE archivo.retencion_campo_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE archivo.retencion_campo_id_seq
    OWNER TO postgres;

CREATE TABLE archivo.retencion_campo
(
    id bigint NOT NULL DEFAULT nextval('archivo.retencion_campo_id_seq'::regclass),
    id_retencion_trama bigint NOT NULL,
    nombre character varying COLLATE pg_catalog."default",
    posicion integer,
    longitud integer,
    longitud_minima integer,
    longitud_maxima integer,
    tipo_dato character varying COLLATE pg_catalog."default",
    descripcion character varying COLLATE pg_catalog."default",
    formato character varying COLLATE pg_catalog."default",
    validacion character varying COLLATE pg_catalog."default",
    requerido boolean,
    condicion character varying COLLATE pg_catalog."default",
    eliminado boolean DEFAULT false,
    CONSTRAINT retencion_campo_pk PRIMARY KEY (id),
    CONSTRAINT retencion_campo_uq1 UNIQUE (nombre),
    CONSTRAINT retencion_campo_fk1 FOREIGN KEY (id_retencion_trama)
        REFERENCES archivo.retencion_trama (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE archivo.retencion_campo
    OWNER to postgres;

CREATE INDEX retencion_campo_idx1
    ON archivo.retencion_campo USING btree
    (id_retencion_trama ASC NULLS LAST)
    TABLESPACE pg_default;

INSERT INTO archivo.retencion_campo VALUES 
	(1001000000000000001, 1001000000000000001, 'PeriodoMesInicial', 1, NULL, NULL, NULL, 'Integer', 'Atributo requerido para la expresión del mes inicial del periodo de la retención e información de pagos', NULL, NULL, true, NULL, false),
	(1001000000000000002, 1001000000000000001, 'PeriodoMesFinal', 2, NULL, NULL, NULL, 'Integer', 'Atributo requerido para la expresión del mes final del periodo de la retención e información de pagos', NULL, NULL, true, NULL, false),
	(1001000000000000003, 1001000000000000001, 'PeriodoEjercicioFiscal', 3, NULL, NULL, NULL, 'Integer', 'Atributo requerido para la expresión del ejercicio fiscal (año)', NULL, NULL, true, NULL, false),
	(1001000000000000004, 1001000000000000001, 'FolioInt', 4, NULL, 1, 20, 'String', 'Atributo opcional para control interno del contribuyente que expresa el folio del documento que ampara la retención e información de pagos. Permite números y/o letras.', NULL, NULL, false, NULL, false),
	(1001000000000000005, 1001000000000000002, 'RfcEmisor', 1, NULL, 12, 13, 'String', 'Atributo requerido para incorporar la clave en el Registro Federal de Contribuyentes correspondiente al contribuyente emisor del documento de retención e información de pagos, sin guiones o espacios.', NULL, NULL, true, NULL, false),
	(1001000000000000006, 1001000000000000002, 'NombreEmisor', 2, NULL, 1, 300, 'String', 'Atributo opcional para el nombre, denominación o razón social del contribuyente emisor del documento de retención e información de pagos.', NULL, NULL, false, NULL, false),
	(1001000000000000007, 1001000000000000002, 'CurpEmisor', 3, 18, NULL, NULL, 'String', 'Atributo opcional para la Clave Única del Registro Poblacional del contribuyente emisor del documento de retención e información de pagos.', NULL, NULL, false, NULL, false),
	(1001000000000000008, 1001000000000000003, 'Nacionalidad', 1, NULL, NULL, NULL, 'String', 'Atributo requerido para expresar la nacionalidad del receptor del documento.', NULL, NULL, true, NULL, false),
	(1001000000000000014, 1001000000000000004, 'BaseImpuesto', 1, NULL, NULL, NULL, 'BigDecimal', 'Atributo opcional para expresar la  base del impuesto, que puede ser la diferencia entre los ingresos percibidos y las deducciones autorizadas', NULL, NULL, false, NULL, false),
	(1001000000000000015, 1001000000000000004, 'Impuesto', 2, NULL, NULL, NULL, 'BigDecimal', 'Atributo opcional para señalar el tipo de impuesto retenido del periodo o ejercicio conforme al catálogo.', NULL, NULL, false, NULL, false),
	(1001000000000000016, 1001000000000000004, 'MontoRetenido', 3, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar el importe del impuesto retenido en el periodo o ejercicio', NULL, NULL, true, NULL, false),
	(1001000000000000017, 1001000000000000004, 'TipoPagoRetencion', 4, NULL, 1, NULL, 'String', 'Atributo requerido para precisar si el monto de la retención es considerado pago definitivo o pago provisional', NULL, NULL, true, NULL, false),
	(1001000000000000018, 1001000000000000005, 'MontoTotalOperacion', 1, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar  el total del monto de la operación  que se relaciona en el comprobante', NULL, NULL, true, NULL, false),
	(1001000000000000019, 1001000000000000005, 'MontoTotalGravado', 2, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar el total del monto gravado de la operación  que se relaciona en el comprobante.', NULL, NULL, true, NULL, false),
	(1001000000000000020, 1001000000000000005, 'MontoTotalExento', 3, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar el total del monto exento de la operación  que se relaciona en el comprobante.', NULL, NULL, true, NULL, false),
	(1001000000000000021, 1001000000000000005, 'MontoTotalRetenciones', 4, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar el monto total de las retenciones. Sumatoria de los montos de retención del nodo ImpRetenidos.', NULL, NULL, true, NULL, false),
	(1001000000000000022, 1001000000000000006, 'CveTipDivOUtil', 1, NULL, 1, NULL, 'String', 'Atributo requerido para expresar la clave del tipo de dividendo o utilidad distribuida de acuerdo al catálogo.', NULL, NULL, true, NULL, false),
	(1001000000000000023, 1001000000000000006, 'MontISRAcredRetMexico', 2, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar el importe o retención  del dividendo o  utilidad en territorio nacional', NULL, NULL, true, NULL, false),
	(1001000000000000024, 1001000000000000006, 'MontISRAcredRetExtranjero', 3, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar el importe o retención  del dividendo o  utilidad en territorio extranjero', NULL, NULL, true, NULL, false),
	(1001000000000000025, 1001000000000000006, 'MontRetExtDivExt', 4, NULL, NULL, NULL, 'BigDecimal', 'Atributo opcional para expresar el monto de la retención en el extranjero sobre dividendos del extranjero', NULL, NULL, false, NULL, false),
	(1001000000000000026, 1001000000000000006, 'TipoSocDistrDiv', 5, NULL, 1, NULL, 'String', 'Atributo requerido  para expresar si el dividendo es distribuido por sociedades nacionales o extranjeras.', NULL, NULL, true, NULL, false),
	(1001000000000000027, 1001000000000000006, 'MontISRAcredNal', 6, NULL, NULL, NULL, 'BigDecimal', 'Atributo opcional para expresar el monto del ISR acreditable nacional', NULL, NULL, false, NULL, false),
	(1001000000000000028, 1001000000000000006, 'MontDivAcumNal', 7, NULL, NULL, NULL, 'BigDecimal', 'Atributo opcional para expresar el monto del dividendo acumulable nacional', NULL, NULL, false, NULL, false),
	(1001000000000000029, 1001000000000000006, 'MontDivAcumExt', 8, NULL, NULL, NULL, 'BigDecimal', 'Atributo opcional para expresar el monto del dividendo acumulable extranjero', NULL, NULL, false, NULL, false),
	(1001000000000000030, 1001000000000000006, 'ProporcionRem', 9, NULL, NULL, NULL, 'BigDecimal', 'Atributo opcional que expresa el porcentaje de participación de sus integrantes o accionistas', NULL, NULL, false, NULL, false),
	(1001000000000000031, 1001000000000000007, 'ContratoIntermediacion', 1, NULL, 1, 300, 'String', 'Atributo requerido para expresar la descripción del contrato de intermediación ', NULL, NULL, true, NULL, false),
	(1001000000000000032, 1001000000000000007, 'EnaGanancia', 2, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar la ganancia obtenida por la enajenación de acciones u operación de valores', NULL, NULL, true, NULL, false),
	(1001000000000000033, 1001000000000000007, 'EnaPerdida', 3, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar la pérdida en el contrato de intermediación', NULL, NULL, true, NULL, false),
	(1001000000000000034, 1001000000000000008, 'SistFinanciero', 1, NULL, 1, NULL, 'String', 'Atributo requerido para expresar si los interés obtenidos en el periodo o ejercicio provienen del sistema financiero', NULL, NULL, true, NULL, false),
	(1001000000000000035, 1001000000000000008, 'RetiroAORESRetInt', 2, NULL, 1, NULL, 'String', 'Atributo requerido para expresar si los intereses obtenidos fueron retirados en el periodo o ejercicio', NULL, NULL, true, NULL, false),
	(1001000000000000036, 1001000000000000008, 'OperFinancDerivad', 3, NULL, 1, NULL, 'String', 'Atributo requerido para expresar si los intereses obtenidos corresponden a operaciones financieras derivadas.', NULL, NULL, true, NULL, false),
	(1001000000000000037, 1001000000000000008, 'MontIntNominal', 4, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar el importe del interés Nóminal obtenido en un periodo o ejercicio', NULL, NULL, true, NULL, false),
	(1001000000000000038, 1001000000000000008, 'MontIntReal', 5, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar el monto de los intereses reales  (diferencia que se obtiene restando al tipo de interés nominal y la tasa de inflación del periodo o ejercicio )', NULL, NULL, true, NULL, false),
	(1001000000000000039, 1001000000000000008, 'IntPerdida', 6, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar la pérdida por los intereses obtenidos en el periodo o ejercicio', NULL, NULL, true, NULL, false),
	(1001000000000000040, 1001000000000000009, 'MontGanAcum', 1, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar el monto de la ganancia acumulable.', NULL, NULL, true, NULL, false),
	(1001000000000000041, 1001000000000000009, 'MontPerdDed', 2, NULL, NULL, NULL, 'BigDecimal', 'Atributo requerido para expresar el monto de la pérdida deducible.', NULL, NULL, true, NULL, false),
	(1001000000000000042, 1001000000000000010, 'EsBenefEfectDelCobro', 1, NULL, 1, NULL, 'String', 'Atributo requerido para expresar si el beneficiario del pago es la misma persona que retiene', NULL, NULL, true, NULL, false),
	(1001000000000000047, 1001000000000000010, 'ConceptoPago', 6, NULL, 1, NULL, 'String', 'Atributo requerido para expresar el tipo de contribuyente sujeto a la retención, conforme al catálogo.', NULL, NULL, true, NULL, false),
	(1001000000000000048, 1001000000000000010, 'DescripcionConcepto', 7, NULL, 1, 255, 'String', 'Atributo requerido para expresar la descripción de la definición del pago del residente en el extranjero', NULL, NULL, true, NULL, false),
	(1001000000000000049, 1001000000000000011, 'IdFideicom', 1, NULL, 1, 20, 'String', 'Atributo requerido para expresar el Identificador o Número del Fideicomiso', NULL, NULL, true, NULL, false),
	(1001000000000000050, 1001000000000000011, 'NomFideicom', 2, NULL, 1, 100, 'String', 'Atributo opcional para expresar el Nombre del Fideicomiso', NULL, NULL, false, NULL, false),
	(1001000000000000051, 1001000000000000011, 'DescripFideicom', 3, NULL, 1, 300, 'String', 'Atributo requerido para expresar el objeto o fin del Fideicomiso', NULL, NULL, true, NULL, false),
	(1001000000000000009, 1001000000000000003, 'ReceptorNacionalRfc', 2, NULL, 12, 13, 'String', 'Atributo requerido para la clave del Registro Federal de Contribuyentes correspondiente al contribuyente receptor del documento.', NULL, NULL, true, 'Nacional', false),
	(1001000000000000010, 1001000000000000003, 'ReceptorNacionalNombre', 3, NULL, 1, 300, 'String', 'Atributo opcional para el nombre, denominación o razón social del contribuyente receptor del documento.', NULL, NULL, false, 'Nacional', false),
	(1001000000000000011, 1001000000000000003, 'ReceptorNacionalCurp', 4, 18, NULL, NULL, 'String', 'Atributo opcional para la Clave Única del Registro Poblacional del contribuyente receptor del documento.', NULL, NULL, false, 'Nacional', false),
	(1001000000000000012, 1001000000000000003, 'ReceptorExtNombre', 5, NULL, 1, 300, 'String', 'Atributo requerido para expresar el nombre, denominación o razón social del receptor del documento cuando sea residente en el extranjero', NULL, NULL, true, 'Extranjero', false),
	(1001000000000000013, 1001000000000000003, 'ReceptorExtNumRegIdTrib', 6, NULL, 1, 20, 'String', 'Atributo opcional para expresar el número de registro de identificación fiscal del receptor del documento cuando sea residente en el extranjero', NULL, NULL, false, 'Extranjero', false),
	(1001000000000000043, 1001000000000000010, 'PaisDeResidParaEfecFisc', 2, NULL, 1, NULL, 'String', 'Atributo requerido para expresar la clave del país de residencia del extranjero, conforme al catálogo de países publicado en el Anexo 10 de la RMF. ', NULL, NULL, true, 'No', false),
	(1001000000000000044, 1001000000000000010, 'PexRfc', 3, NULL, 12, 13, 'String', 'Atributo requerido para expresar la clave del registro federal de contribuyentes del representante legal en México', NULL, NULL, true, 'Si', false),
	(1001000000000000045, 1001000000000000010, 'PexCurp', 4, 18, NULL, NULL, 'String', 'Atributo requerido para la expresión de la CURP del representante legal', NULL, NULL, true, 'Si', false),
	(1001000000000000046, 1001000000000000010, 'NomDenRazSocB', 5, NULL, 1, 300, 'String', 'Atributo requerido para expresar el nombre, denominación o razón social del contribuyente en México', NULL, NULL, true, 'Si', false);

CREATE SEQUENCE nomencladores.retencion_ejercicio_rango_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE nomencladores.retencion_ejercicio_rango_id_seq
    OWNER TO postgres;
	
CREATE TABLE nomencladores.retencion_ejercicio_rango
(
    id bigint NOT NULL DEFAULT nextval('nomencladores.retencion_ejercicio_rango_id_seq'::regclass),
	version_retencion character varying(250) COLLATE pg_catalog."default" NOT NULL,
    ejercicio_inicial integer NOT NULL,
    ejercicio_final integer NOT NULL,
    eliminado boolean DEFAULT false,
    CONSTRAINT retencion_ejercicio_rango_pk PRIMARY KEY (id),
    CONSTRAINT retencion_ejercicio_rango_uq UNIQUE (version_retencion)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE nomencladores.retencion_ejercicio_rango
    OWNER to postgres;

INSERT INTO nomencladores.retencion_ejercicio_rango(id, version_retencion, ejercicio_inicial, ejercicio_final, eliminado) VALUES 
	(1001000000000000001, '1.0', 2004, 2024, false);	