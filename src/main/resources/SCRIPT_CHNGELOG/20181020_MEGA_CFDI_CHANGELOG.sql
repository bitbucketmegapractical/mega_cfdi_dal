ALTER TABLE facturacion.cfdi
    ADD COLUMN cancelacion_en_proceso boolean DEFAULT false;
ALTER TABLE facturacion.cfdi
    ADD COLUMN solicitud_cancelacion boolean DEFAULT false;
ALTER TABLE facturacion.cfdi
    ADD COLUMN acuse_cancelacion text COLLATE pg_catalog."default";
	
CREATE SEQUENCE facturacion.cfdi_solicitud_cancelacion_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1001000000000000001
  CACHE 1;
ALTER TABLE facturacion.cfdi_solicitud_cancelacion_id_seq
  OWNER TO postgres;
  
CREATE TABLE facturacion.cfdi_solicitud_cancelacion
(
    id bigint NOT NULL DEFAULT nextval('facturacion.cfdi_solicitud_cancelacion_id_seq'::regclass),    
    uuid character varying(100) COLLATE pg_catalog."default",
	codigo_estatus character varying(250) COLLATE pg_catalog."default",
	es_cancelable character varying(250) COLLATE pg_catalog."default",
	estado character varying(250) COLLATE pg_catalog."default",
	estatus_cancelacion character varying(250) COLLATE pg_catalog."default",
	fecha_solicitud date NOT NULL,
	hora_solicitud time without time zone NOT NULL,
	fecha_ultima_actualizacion date,
	hora_ultima_actualizacion time without time zone,
	solicitud_activa boolean DEFAULT true,
    CONSTRAINT cfdi_solicitud_cancelacion_pk PRIMARY KEY (id),
    CONSTRAINT cfdi_solicitud_cancelacion_uq UNIQUE (uuid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE facturacion.cfdi_solicitud_cancelacion
    OWNER to postgres;

CREATE INDEX cfdi_solicitud_cancelacion_idx1
    ON facturacion.cfdi_solicitud_cancelacion USING btree
    (uuid)
    TABLESPACE pg_default;	
	
CREATE INDEX cfdi_solicitud_cancelacion_idx2
    ON facturacion.cfdi_solicitud_cancelacion USING btree
    (uuid, estado)
    TABLESPACE pg_default;
	
CREATE INDEX cfdi_solicitud_cancelacion_idx3
    ON facturacion.cfdi_solicitud_cancelacion USING btree
    (uuid, es_cancelable)
    TABLESPACE pg_default;
	
CREATE INDEX cfdi_solicitud_cancelacion_idx4
    ON facturacion.cfdi_solicitud_cancelacion USING btree
    (uuid, estatus_cancelacion)
    TABLESPACE pg_default;

CREATE SEQUENCE facturacion.cfdi_cancelacion_log_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1001000000000000001
  CACHE 1;
ALTER TABLE facturacion.cfdi_cancelacion_log_id_seq
  OWNER TO postgres;

CREATE TABLE facturacion.cfdi_cancelacion_log
(
    id bigint NOT NULL DEFAULT nextval('facturacion.cfdi_cancelacion_log_id_seq'::regclass),
    uuid character varying(100) COLLATE pg_catalog."default",	
	codigo_estatus character varying(250) COLLATE pg_catalog."default",
	es_cancelable character varying(250) COLLATE pg_catalog."default",
	estado character varying(250) COLLATE pg_catalog."default",
	estatus_cancelacion character varying(250) COLLATE pg_catalog."default",
	fecha date NOT NULL,
	hora time without time zone NOT NULL,
	tarea_programada boolean DEFAULT false,
    CONSTRAINT cfdi_cancelacion_log_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE facturacion.cfdi_cancelacion_log
    OWNER to postgres;

CREATE SEQUENCE facturacion.cfdi_cancelacion_log_error_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1001000000000000001
  CACHE 1;
ALTER TABLE facturacion.cfdi_cancelacion_log_error_id_seq
  OWNER TO postgres;
  
CREATE TABLE facturacion.cfdi_cancelacion_log_error
(
    id bigint NOT NULL DEFAULT nextval('facturacion.cfdi_cancelacion_log_error_id_seq'::regclass),
    uuid character varying(100) COLLATE pg_catalog."default",	
	wsdl_url character varying(250) COLLATE pg_catalog."default",
	wsdl_endpoint character varying(250) COLLATE pg_catalog."default",
	wsdl_operacion character varying(250) COLLATE pg_catalog."default",
	traza_error text COLLATE pg_catalog."default",
	fecha date NOT NULL,
	hora time without time zone NOT NULL,
	tarea_programada boolean DEFAULT false,
    CONSTRAINT cfdi_cancelacion_log_error_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE facturacion.cfdi_cancelacion_log_error
    OWNER to postgres;  	