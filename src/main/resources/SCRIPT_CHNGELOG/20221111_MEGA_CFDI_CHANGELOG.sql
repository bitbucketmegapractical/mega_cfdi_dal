UPDATE archivo.nomina_trama SET longitud=8, campos=7 WHERE id=1001000000000000003;
INSERT INTO archivo.nomina_campo (id_nomina_trama,nombre,posicion,tipo_dato,descripcion,requerido,eliminado) VALUES 
	(1001000000000000003,'Exportacion',7,'String','Atributo requerido para expresar si el comprobante ampara una operación de exportación.',true,false);
	
UPDATE archivo.nomina_trama SET longitud=7, campos=6 WHERE id=1001000000000000005;
INSERT INTO archivo.nomina_campo (id_nomina_trama,nombre,posicion,longitud,tipo_dato,descripcion,requerido,eliminado) VALUES 
	(1001000000000000005,'DomicilioFiscal',3,5,'String','Atributo requerido para registrar el código postal del domicilio fiscal del receptor del comprobante.',true,false);
INSERT INTO archivo.nomina_campo (id_nomina_trama,nombre,posicion,tipo_dato,descripcion,requerido,eliminado) VALUES 
	(1001000000000000005,'RegimenFiscal',4,'String','Atributo requerido para incorporar la clave del régimen fiscal del contribuyente receptor al que aplicará el efecto fiscal de este comprobante.',true,false),
	(1001000000000000005,'UsoCFDI',5,'String','Atributo requerido para expresar la clave del uso que dará a esta factura el receptor del CFDI.',true,false),
	(1001000000000000005,'ObjetoImp',6,'String','Atributo requerido para expresar si la operación comercial es objeto o no de impuesto.',true,false);
	
UPDATE nomencladores.entidad_federativa SET codigo='CMX' WHERE id=1001000000000000009;
INSERT INTO nomencladores.riesgo_puesto (codigo,valor,eliminado) VALUES ('99','No aplica',false);