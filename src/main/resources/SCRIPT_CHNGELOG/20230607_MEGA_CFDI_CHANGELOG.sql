ALTER TABLE clientes.bancomex_archivo SET SCHEMA archivo;
ALTER TABLE archivo.bancomex_archivo RENAME TO archivo_stfp;
ALTER SEQUENCE clientes.bancomex_archivo_id_seq SET SCHEMA archivo;
ALTER SEQUENCE archivo.bancomex_archivo_id_seq RENAME TO archivo_stfp_id_seq;

ALTER TABLE archivo.archivo_stfp ADD hora_carga time NULL;
ALTER TABLE archivo.archivo_stfp ADD ruta_descarga varchar NULL;
ALTER TABLE archivo.archivo_stfp ADD ruta_error_validacion varchar NULL;
ALTER TABLE archivo.archivo_stfp ADD ruta_error_contenido varchar NULL;
ALTER TABLE archivo.archivo_stfp ADD ruta_error_s3 varchar NULL;