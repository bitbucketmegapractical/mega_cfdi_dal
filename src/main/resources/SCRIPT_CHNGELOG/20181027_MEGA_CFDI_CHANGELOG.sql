ALTER SEQUENCE facturacion.cfdi_cancelacion_log_id_seq
    SET SCHEMA log;
ALTER TABLE facturacion.cfdi_cancelacion_log
  SET SCHEMA log;

ALTER SEQUENCE facturacion.cfdi_cancelacion_log_error_id_seq  
    SET SCHEMA log;
ALTER TABLE facturacion.cfdi_cancelacion_log_error
  SET SCHEMA log;  
	
CREATE SEQUENCE log.cfdi_consulta_estatus_log_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1001000000000000001
  CACHE 1;
ALTER TABLE log.cfdi_consulta_estatus_log_id_seq
  OWNER TO postgres;

CREATE TABLE log.cfdi_consulta_estatus_log
(
    id bigint NOT NULL DEFAULT nextval('log.cfdi_consulta_estatus_log_id_seq'::regclass),
    uuid character varying(100) COLLATE pg_catalog."default",	
	codigo_estatus character varying(250) COLLATE pg_catalog."default",
	es_cancelable character varying(250) COLLATE pg_catalog."default",
	estado character varying(250) COLLATE pg_catalog."default",
	estatus_cancelacion character varying(250) COLLATE pg_catalog."default",
	fecha date NOT NULL,
	hora time without time zone NOT NULL,
	tarea_programada boolean DEFAULT false,
    CONSTRAINT cfdi_consulta_estatus_log_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE log.cfdi_consulta_estatus_log
    OWNER to postgres;

CREATE SEQUENCE log.cfdi_consulta_estatus_log_error_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1001000000000000001
  CACHE 1;
ALTER TABLE log.cfdi_consulta_estatus_log_error_id_seq
  OWNER TO postgres;
  
CREATE TABLE log.cfdi_consulta_estatus_log_error
(
    id bigint NOT NULL DEFAULT nextval('log.cfdi_consulta_estatus_log_error_id_seq'::regclass),
    uuid character varying(100) COLLATE pg_catalog."default",	
	wsdl_url character varying(250) COLLATE pg_catalog."default",
	wsdl_endpoint character varying(250) COLLATE pg_catalog."default",
	wsdl_operacion character varying(250) COLLATE pg_catalog."default",
	traza_error text COLLATE pg_catalog."default",
	fecha date NOT NULL,
	hora time without time zone NOT NULL,
	tarea_programada boolean DEFAULT false,
    CONSTRAINT cfdi_consulta_estatus_log_error_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE log.cfdi_consulta_estatus_log_error
    OWNER to postgres;	
	
CREATE SCHEMA tareas_programadas
    AUTHORIZATION postgres;
	
CREATE SEQUENCE tareas_programadas.tarea_programada_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE tareas_programadas.tarea_programada_id_seq
    OWNER TO postgres;

CREATE TABLE tareas_programadas.tarea_programada
(
    id bigint NOT NULL DEFAULT nextval('tareas_programadas.tarea_programada_id_seq'::regclass),
    codigo character varying(100) COLLATE pg_catalog."default",
    descripcion character varying(500) COLLATE pg_catalog."default",
    habilitado boolean DEFAULT false,
    CONSTRAINT tarea_programada_pk PRIMARY KEY (id),
    CONSTRAINT tarea_programada_uq UNIQUE (codigo)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE tareas_programadas.tarea_programada
    OWNER to postgres;

CREATE SEQUENCE tareas_programadas.tarea_programada_log_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE tareas_programadas.tarea_programada_log_id_seq
    OWNER TO postgres;

CREATE TABLE tareas_programadas.tarea_programada_log
(
    id bigint NOT NULL DEFAULT nextval('tareas_programadas.tarea_programada_log_id_seq'::regclass),
    id_tarea_programada bigint NOT NULL,
    fecha_ejecutada date NOT NULL,
    hora_ejecutada time without time zone NOT NULL,
    CONSTRAINT tarea_programada_log_pk PRIMARY KEY (id),
    CONSTRAINT tarea_programada_log_fk1 FOREIGN KEY (id_tarea_programada)
        REFERENCES tareas_programadas.tarea_programada (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE tareas_programadas.tarea_programada_log
    OWNER to postgres;

CREATE INDEX tarea_programada_log_idx1
    ON tareas_programadas.tarea_programada_log USING btree
    (id_tarea_programada)
    TABLESPACE pg_default;
	
CREATE SEQUENCE tareas_programadas.tarea_programada_log_error_id_seq
    INCREMENT 1
    START 1001000000000000001
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE tareas_programadas.tarea_programada_log_error_id_seq
    OWNER TO postgres;

CREATE TABLE tareas_programadas.tarea_programada_log_error
(
    id bigint NOT NULL DEFAULT nextval('tareas_programadas.tarea_programada_log_error_id_seq'::regclass),
    id_tarea_programada bigint,
    fecha_error date NOT NULL,
    hora_error time without time zone NOT NULL,
    traza_error text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tarea_programada_log_error_pk PRIMARY KEY (id),
    CONSTRAINT tarea_programada_log_error_fk1 FOREIGN KEY (id_tarea_programada)
        REFERENCES tareas_programadas.tarea_programada (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE tareas_programadas.tarea_programada_log_error
    OWNER to postgres;

CREATE INDEX tarea_programada_log_error_idx1
    ON tareas_programadas.tarea_programada_log_error USING btree
    (id_tarea_programada)
    TABLESPACE pg_default;	
	
INSERT INTO tareas_programadas.tarea_programada(id, codigo, descripcion, habilitado)
	VALUES (1001000000000000001, 'ST_CCS', 'Tarea ejecutada cada 3 horas todos los días. Actualiza las solicitudes de cancelación pendientes', true);