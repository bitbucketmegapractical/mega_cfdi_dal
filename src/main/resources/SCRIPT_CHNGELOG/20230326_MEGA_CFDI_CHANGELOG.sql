ALTER TABLE facturacion.cfdi ALTER COLUMN serie TYPE varchar(100) USING serie::varchar;
ALTER TABLE facturacion.cfdi ALTER COLUMN folio TYPE varchar(100) USING folio::varchar;
ALTER TABLE facturacion.nomina ADD serie varchar(100) NULL;
ALTER TABLE facturacion.nomina ADD folio varchar(100) NULL;

ALTER TABLE seguridad.usuario_api ADD stamp bool NULL DEFAULT false;
UPDATE seguridad.usuario_api SET stamp=true WHERE id=1;