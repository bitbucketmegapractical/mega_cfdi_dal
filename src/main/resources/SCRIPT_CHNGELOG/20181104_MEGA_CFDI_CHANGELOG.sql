INSERT INTO nomencladores.tipo_operacion(id, codigo, valor, imagen, eliminado)
	VALUES (1001000000000000083, 'CANCELACION_MASIVA_CFDIV33', 'El usuario ha cancelado masivamente mediante archivo un total de {1} UUIDs', '<i class="fa fa-envelope" aria-hidden="true"></i>', false);
SELECT setval('nomencladores.tipo_operacion_id_seq', 1001000000000000083, true);

INSERT INTO nomencladores.tipo_suceso(id, codigo, valor, notificable, imagen, eliminado)
	VALUES (1001000000000000045, 'DESCARGA_MASIVA_CFDI_V33', 'Ha realizado una descarga masiva de {1} CFDIs', true, '<i class="fa fa-file-o" aria-hidden="true"></i>', false);
INSERT INTO nomencladores.tipo_suceso(id, codigo, valor, notificable, imagen, eliminado)
	VALUES (1001000000000000046, 'ENVIO_CORREO_ELECTRONICO', 'Ha enviado por correo electrónico el XML y PDF del CFDI con UUID {1}', true, '<i class="fa fa-file-o" aria-hidden="true"></i>', false);
INSERT INTO nomencladores.tipo_suceso(id, codigo, valor, notificable, imagen, eliminado)
	VALUES (1001000000000000047, 'CANCELACION_MASIVA_CFDIV33', 'Ha cancelado masivamente mediante archivo un total de {1} UUIDs', true, '<i class="fa fa-file-o" aria-hidden="true"></i>', false);
SELECT setval('nomencladores.tipo_suceso_id_seq', 1001000000000000047, true);	

ALTER TABLE seguridad.bitacora
    ADD COLUMN descripcion character varying(500) COLLATE pg_catalog."default";